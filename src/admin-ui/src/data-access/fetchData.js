/**
 * the function "getLinkWithoutParams" receives a url as parameter in this case "serveraddress"
 * and looks for curly braves in this url, if there are curly braves they will cut out.
 * Then the function returns the url.
 *
 * @param link is a url as string
 * @return returns the link as string without the curly braces and everything between there
 */
export function getLinkWithoutParams(link) {
    if (link.includes("{")) {
        return link.split("{")[0];
    }
    return link;
}

/**
 * Authors: Ralf Stuerzner, Anton Diettrich
 * Date: 2020/08/01
 * the function "fetchData" parses data from the server
 *
 * @param serveraddress is a url as string
 * @param data are the fetched data from the url
 * @return returns the fetched data if everything is fetched
 */
export default async function fetchData(serveraddress) {
    try {
        let data = await fetch(getLinkWithoutParams(serveraddress));
        if (!data.ok) {
            throw new Error(data.status);
        }
        return await data.json();
    } catch (error) {
        console.log(error);
    }
}

/**
 * Send data using POST and receive response.
 * 
 * Author: Hartmut Knaack
 * Mostly example implementation of https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
 * @param {url} Server URL
 * @param {data} JSON object to send via POST request
 * @return {json} Server response as JSON
 */
export async function fetchDataPost(url = '', data = {}) {
    // Default options are marked with *
    try {
        let response = await fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json' // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });

        if (!response.ok) {
            throw new Error("Network response was not OK");
        }

        return response.json(); // parses JSON response into native JavaScript objects
    } catch (error) {
        console.log(error.message);
    }
}
