/**
 * Author: Hartmut Knaack
 *
 * @fileoverview Test cases for backend configuration methods.
 */

import BackendUrl, {hostname, portNr, baseDirectory, protocol} from './backendconfig.js';

describe('BackendConfig', () => {
  test('hostname() with defined domain name', () => {
    expect(hostname()).toBe('www.iana.org');
  });

  test('portNr() with defined port number', () => {
    expect(portNr()).toBe('443');
  });

  test('baseDirectory() with defined path', () => {
    expect(baseDirectory()).toBe('/domains/reserved');
  });

  test('protocol() with defined protocol', () => {
    expect(protocol()).toBe('https');
  });

  test('BackendUrl() with all variables defined', () => {
    expect(BackendUrl()).toBe('https://www.iana.org:443/domains/reserved');
  });
});
