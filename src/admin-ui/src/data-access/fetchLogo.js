import backendUrl from "./backendconfig";

export default async function fetchLogo (id) {
    let image = null;
    await fetch(backendUrl() + "/api/organisations/" + id + "/logo")
        .then(response => {

            if (response.status === 200) {
                return response.blob();
            }
        }, )
        .then((myBlob) => image = URL.createObjectURL(myBlob))
        .catch( error => {});
    return image;
}