/**
 * Author: Anton Rhein
 * Date: 2020-06-11
 */

import BackendUrl from "./backendconfig";
import ApiResult, {ApiCodes} from "./ApiResult";
import Auth from "./Auth";

/**
 * transforms the passed data object (according to properties of JS fetch-API)
 * into a domain specific @type {ApiResult}
 * if result is an integrityViolation, data contains a human-readable german field "message" of the reason
 * @param {*} data
 */
const createApiResult = async (data) => {
    if (data.ok) {
        let json = ""
        if (data.status !== 204) {
            json = await data.json();
        }
        if (data.projectsNotDeletedWarning) {
            return new ApiResult(ApiCodes.WithWarnings, {
                data: json,
                warning: "Manche Projekte konnten nicht entfernt werden, da sie ohne Organisation nicht existieren können! Löschen Sie diese oder Teilen Sie ihnen eine andere Organisation zu!"
            })
        }
        if (data.interviewsNotDeletedWarning) {
            return new ApiResult(ApiCodes.WithWarnings, {
                data: json,
                warning: "Manche Interviews konnten nicht entfernt werden, da sie ohne Organisation nicht existieren können! Löschen Sie diese oder Teilen Sie ihnen eine andere Organisation zu!"
            })
        }
        return new ApiResult(ApiCodes.Ok, json);
    } else if (data.status === 400) {
        console.log(await data.json());
        return new ApiResult(ApiCodes.ServerValidationError);
    } else if (data.status === 500) {
        return new ApiResult(ApiCodes.InternalError);
    } else if (data.status === 404) {
        return new ApiResult(ApiCodes.NotFound);
    } else if (data.status === 409) {
        let reason = "Unbekannter Integritätsfehler";
        let json = (await data.json());
        let message = json.message;
        if (message.toLowerCase().includes("organisation_project")) {
            reason = "Zur Organisation existieren noch Projekte! Löschen sie zuvor die Verbindung zu den entsprechenden Projekten!"
        } else if (json.cause != null) {
            let cause = json.cause;
            while (cause.cause) {
                cause = cause.cause;
            }
            const msg = cause.message.toLowerCase();
            if (msg.includes("update") && msg.includes("interview") && msg.includes("organisation")) {
                reason = "Interviews dürfen nicht ohne Organisation existieren! Weisen Sie die Interviews vorher anderen Organisationen zu!";
            }
            if (msg.includes("interview")) {
                reason = "Zur Organisation existieren noch Interviews! Löschen Sie zuvor die entsprechenden Interviews!"
            }

        }
        return new ApiResult(ApiCodes.IntegrityViolation, {reason});
    } else {
        return new ApiResult(ApiCodes.Unknown);
    }
};

/**
 * Util Function to get Headers for requests
 */
function getHeaders() {
    let header = {
        "Accept": "application/json",
        "Content-Type": "application/json",
    };

    if (Auth.isUserAuthenticated()) {
        header["Authorization"] = "Bearer " + Auth.getToken();
    }

    return header;
}

/**
 * Transforms the project passed from ui into a rest project
 * @param {*} uiProject
 */

function transformProject(uiProject) {
    let result = Object.assign({}, uiProject);
    result.organisations = result.organisations ? result.organisations.map((o) =>
        o._links.self.href.replace(`${BackendUrl()}/api/`, "")
    ) : [];
    result.categories = result.categories ? result.categories.map(c =>
        c._links.self.href.replace(`${BackendUrl()}/api/`, "")
    ) : [];
    result.tags = result.tags ? result.tags.map(t =>
        t._links.self.href.replace(`${BackendUrl()}/api/`, ""),
    ) : [];
    result.location = {
        longitude: 0.0,
        latitude: 0.0,
        street: "Test",
        streetNumber: "24a",
        zipCode: "12345",
    };
    result.imageUrls = [uiProject.imageUrls];

    return result;
}

/**
 * Transforms the organisation passed from ui into a rest organisation
 * @param {*} uiOrga
 */
function transformOrganisation(uiOrga) {

    let result = Object.assign({}, uiOrga);


    result.categories = (result.categories) ? result.categories.map(c => c._links.self.href.replace(`${BackendUrl()}/api/`, "")) : [];
    result.interviews = (result.interviews) ? result.interviews.map(i => i._links.self.href.replace(`${BackendUrl()}/api/`, "")) : [];
    result.projects = (result.projects) ? result.projects.map(i => i._links.self.href.replace(`${BackendUrl()}/api/`, "")) : [];
    result.tags = (result.tags) ? result.tags.map(i => i._links.self.href.replace(`${BackendUrl()}/api/`, "")) : [];

    return result;
}

/**
 * Transforms the interview passed from ui into a rest interview
 * @param {*} uiInterview
 */
function transformInterview(uiInterview) {
    let result = Object.assign({}, uiInterview);
    result.organisations = result.organisations ? result.organisations.map((o) =>
        o._links.self.href.replace(`${BackendUrl()}/api/`, "")
    ) : [];
    return result;
}

/**
 * Utility class to simplify data access made to the Quernetzer REST Api
 */
export class Backend {
    /**
     * Performs a post request using @see BackendUrl to the Quernetzer REST API
     * Not supposed to modify relations between entities
     *
     * Attention: method under development. project-data may be modified by this method because of missing clarification about location handling
     * @param {*} entity one of {"project", "organisation","interview","tag","category"}
     * @param {*} data data to pass to the backend
     *
     * When using projects:
     *
     * categories of project are expected to have following format--> categories = ["https:.../api/categories/{id}"]
     * organisations of project are supposed to have format specified in backend documentation (complex object, _links.self.href will be parsed by this method)
     */
    post = async (entity, data) => {
        let result = null;
        if (!Auth.isUserAuthenticated) {
            return new ApiResult(ApiCodes.NotAuthenticated);
        }

        if (entity === "project") {
            const project = transformProject(data);
            result = await fetch(BackendUrl() + "/api/projects", {
                method: "POST",
                headers: getHeaders(),
                body: JSON.stringify(project),
            });
        } else if (entity === "organisation") {
            const newOrganisation = transformOrganisation(data);
            result = await fetch(BackendUrl() + "/api/organisations", {
                method: "POST",
                headers: getHeaders(),
                body: JSON.stringify(newOrganisation),
            });
        } else if (entity === "category") {
            const newCategory = data;
            result = await fetch(BackendUrl() + "/api/categories", {
                method: "POST",
                headers: getHeaders(),
                body: JSON.stringify(newCategory),
            });
        } else if (entity === "tag") {
            const newTag = data;
            result = await fetch(BackendUrl() + "/api/tags", {
                method: "POST",
                headers: getHeaders(),
                body: JSON.stringify(newTag),
            });
        } else if (entity === "interview") {
            const interview = transformInterview(data);
            result = await fetch(BackendUrl() + "/api/interviews", {
                method: "POST",
                headers: getHeaders(),
                body: JSON.stringify(interview),
            });
        } else {
            throw new Error(`Unknown Entity specified: ${entity}`);
        }
        return createApiResult(result);
    };

    /**
     * WIP
     * @param {} entity
     * @param {*} data
     */
    patch = async (entity, data) => {
        let result = null;
        if (!Auth.isUserAuthenticated) {
            return new ApiResult(ApiCodes.NotAuthenticated);
        }

        const jwt = Auth.getToken();
        const deleteInit = {
            method: "DELETE",
            headers: {"Authorization": "Bearer " + jwt}
        };

        if (entity === "project") {
            const updatedProject = transformProject(data);
            /* DELETE PREVIOUS ASSOCIATIONS */
            let res = await fetch(`${updatedProject.uri}?projection=withTagsCategoriesOrganisationsAndInterviews`, {
                method: "GET",
                headers: {
                    "Accept": "application/json",
                }
            });
            if (res.ok) {
                let persistedProject = (await res.json());
                let persistedCategoryProjectAssociations = persistedProject.categories.map(c => `${c._links.self.href}/${updatedProject.uri.replace(`${BackendUrl()}/api/`, "")}`);
                let persistedTagProjectAssociations = persistedProject.tags.map(t => `${t._links.self.href}/${updatedProject.uri.replace(`${BackendUrl()}/api/`, "")}`);

                let results = await Promise.all(persistedCategoryProjectAssociations.map(a => fetch(a, deleteInit)));
                results = results.concat((await Promise.all(persistedTagProjectAssociations.map(a => fetch(a, deleteInit)))));
                if ((results.length > 0) && results.some(r => !(r.ok))) {
                    console.error("Could not delete previously associated tags or categories!");
                    return new ApiResult(ApiCodes.IntegrityViolation, {reason: "Vorher verknüpfte Tags oder Kategorien konnten nicht entfernt werden!"});
                }
            } else {
                console.error("Could not obtain project to be updated!");
                return new ApiResult(ApiCodes.Unknown);
            }
            /* END OF DELETING PREVIOUS ASSOCIATIONS */
            /* Save updated Project, including new associations */
            result = await fetch(updatedProject.uri, {
                method: "PATCH",
                headers: getHeaders(),
                body: JSON.stringify(updatedProject)
            });
        } else if (entity === "organisation") {
            const updatedOrganisation = transformOrganisation(data);
            /* flag that indicates, that some project associations could not be deleted (by intent) */
            let someProjectsDeleteFailed = false;
            /* flag that indicates, that some interview associations could not be deleted (by intent) */
            let someInterviewsDeleteFailed = false;
            /* get associated tags and categories */
            let res = await fetch(`${updatedOrganisation.uri}?projection=withTagsCategoriesProjectsAndInterviews`, {
                method: "GET",
                headers: {
                    "Accept": "application/json",
                }
            });
            if (res.ok) {
                let persistedOrganisation = (await res.json());
                console.log(persistedOrganisation)
                //generate hypermedia uris for deleting these associations
                let persistedCategoryOrganisationAssociations = persistedOrganisation.categories.map(c => `${c._links.self.href}/${updatedOrganisation.uri.replace(`${BackendUrl()}/api/`, "")}`);
                let persistedTagOrganisationAssociations = persistedOrganisation.tags.map(t => `${t._links.self.href}/${updatedOrganisation.uri.replace(`${BackendUrl()}/api/`, "")}`);

                //get organisations associated to organisation
                let res2 = await fetch(persistedOrganisation._links.projects.href.replace("{?projection}", ""), {
                    method: "GET",
                    headers: {
                        "Accept": "application/json"
                    }
                });

                //array that holds the project associations, that will be deleted
                let projectAssociationsToDelete = []
                if (res2.ok) {
                    //get persisted projects
                    const projects = (await res2.json())._embedded.projects;
                    //filter for projects, that won't be associated to organisation afterwards
                    let projectsToDelete = projects.filter(pr => !updatedOrganisation.projects.some(up => pr._links.self.href.includes(up.replace("{?projection}", "")))).map(pr => pr._links.self.href);
                    let projectsSafeToDelete = [];
                    //check if projects that will be removed from organisation still have another organisation associated
                    for (var p of projectsToDelete) {
                        let orgas = (await (await fetch(`${p}/organisations`)).json())._embedded.organisations;
                        if (orgas.length > 1) {
                            //yes they have, we can delete this assoc
                            projectsSafeToDelete.push(p);
                        } else {
                            //no: set flag
                            someProjectsDeleteFailed = true;
                        }
                    }
                    projectAssociationsToDelete = projectsSafeToDelete.map(ps => `${ps}/${updatedOrganisation.uri.replace(`${BackendUrl()}/api/`, "")}`);
                }

                let res3 = await fetch(persistedOrganisation._links.interviews.href.replace("{?projection}", ""), {
                    method: "GET",
                    headers: {
                        "Accept": "application/json"
                    }
                });

                let interviewAssociationsToDelete = []
                if (res3.ok) {
                    //get persisted projects
                    const interviews = (await res3.json())._embedded.interviews;
                    //filter for interviews, that won't be associated to organisation afterwards
                    let interviewsToDelete = interviews.filter(i => !updatedOrganisation.interviews.some(up => i._links.self.href.includes(up.replace("{?projection}", "")))).map(i => i._links.self.href);
                    let interviewsSafeToDelete = [];
                    //check if interviews that will be removed from organisation still have another organisation associated
                    for (var i of interviewsToDelete) {
                        let orgas = (await (await fetch(`${i}/organisations`)).json())._embedded.organisations;
                        if (orgas.length > 1) {
                            //yes they have, we can delete this assoc
                            interviewsSafeToDelete.push(i);
                        } else {
                            //no: set flag
                            someInterviewsDeleteFailed = true;
                        }
                    }
                    interviewAssociationsToDelete = interviewsSafeToDelete.map(int => `${int}/${updatedOrganisation.uri.replace(`${BackendUrl()}/api/`, "")}`);
                }

                //remove all old assocations
                let nPromises = [];
                nPromises = nPromises.concat(persistedCategoryOrganisationAssociations.map(a => fetch(a, deleteInit)));
                nPromises = nPromises.concat(persistedTagOrganisationAssociations.map(a => fetch(a, deleteInit)));
                nPromises = nPromises.concat(projectAssociationsToDelete.map(a => fetch(a, deleteInit)));
                nPromises = nPromises.concat(interviewAssociationsToDelete.map(a => fetch(a, deleteInit)));

                let results = await Promise.all(nPromises);

                if ((results.length !== 0) && (results.some(r => !(r.ok)))) {
                    console.error("Could not delete previously associated tags or categories!");
                    return new ApiResult(ApiCodes.IntegrityViolation, {reason: "Vorher verknüpfte Tags oder Kategorien konnten nicht entfernt werden!"});
                }
            } else {
                console.error("Could not obtain project to be updated!");
                return new ApiResult(ApiCodes.Unknown);
            }

            //add new relations to other entities to organisation
            const patchInit = {
                method: "PATCH",
                headers: {
                    "Content-Type": "text/uri-list",
                    "Authorization": "Bearer " + jwt,
                },
                body: updatedOrganisation.uri
            };
            let promises = [];
            promises = promises.concat(Promise.all(updatedOrganisation.categories.map(c => fetch(`${BackendUrl()}/api/${c}/organisations`.replace("{?projection}", ""), patchInit))));
            promises = promises.concat(Promise.all(updatedOrganisation.tags.map(t => fetch(`${BackendUrl()}/api/${t}/organisations`.replace("{?projection}", ""), patchInit))));
            promises = promises.concat(Promise.all(updatedOrganisation.projects.map(pr => fetch(`${BackendUrl()}/api/${pr}/organisations`.replace("{?projection}", ""), patchInit))));
            promises = promises.concat(Promise.all(updatedOrganisation.interviews.map(int => fetch(`${BackendUrl()}/api/${int}/organisations`.replace("{?projection}", ""), patchInit))));

            await Promise.all(promises);

            //update atomic data (proj, tags, cats won't be affected, interviews will be saved)
            result = await fetch(updatedOrganisation.uri, {
                method: "PATCH",
                headers: getHeaders(),
                body: JSON.stringify(updatedOrganisation)
            });

            result.projectsNotDeletedWarning = someProjectsDeleteFailed;
            result.interviewsNotDeletedWarning = someInterviewsDeleteFailed;
        } else if (entity === "interview") {
            //my favourite patch method
            const interview = transformInterview(data);
            result = await fetch(interview.uri, {
                method: "PATCH",
                headers: getHeaders(),
                body: JSON.stringify(interview)
            });
        } else {
            console.error("not implemented yet");
            return new ApiResult(ApiCodes.Unknown);
        }
        return createApiResult(result);
    };

    /**
     * WIP
     * @param {*} entity
     * @param {*} object
     */
    delete = async (uri) => {
        if (!Auth.isUserAuthenticated) {
            return new ApiResult(ApiCodes.NotAuthenticated);
        }

        const jwt = Auth.getToken();
        const data = await fetch(uri, {
            method: "DELETE",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": "Bearer " + jwt,
            }
        });
        return createApiResult(data);
    };

    /**
     * WIP
     * @param {*} entity
     * @param {*} data
     */
    put = (entity, data) => {
        console.error("not implemented yet");
        return new ApiResult(ApiCodes.Unknown);
    };
}

export default new Backend();
export {ApiResult};
