/**
 * creates a fixed, identity equatable object instance
 * @param {*} name
 */
const enumValue = (name) => Object.freeze({ toString: () => name });

/**
 * enum like data structure representing managed (and unmanaged) possible API results
 */
const ApiCodes = Object.freeze({
  Ok: enumValue("ApiResult: Ok"),
  ServerValidationError: enumValue("ApiResult: Server validation Error"),
  NotFound: enumValue("ApiResult: Not Found"),
  InternalError: enumValue("ApiResult: Internal Server Error"),
  Unknown: enumValue("ApiResult: Unknown Server Error"),
  IntegrityViolation : enumValue("ApiResult: Call violates data integrity"),
  WithWarnings : enumValue("ApiResult: With Warnings")
});

/**
 * Represents the result of an API call
 */
class Result {
  constructor(apiCode, data, warning) {
    this._apiCode = apiCode;
    this._data = data;
    this._warning = warning;
  }

  /**
   * returns the response of the API call if result is "ok" or "integrityViolation", else throws an error!
   * 
   * as Object
   */
  get data() {
    if (this.ok || this.integrityViolation || this.withWarnings) {
      return this._data;
    } else {
      throw new Error("ApiResult is not okay - there is no valid data!");
    }
  }

  /**
   * returns possible warnings, you should check `withWarnings`
   * throws error, if no warning occured
   */
  get warning(){
    if(this.withWarnings){
      return this._warning
    } else {
      throw new Error("There were no warnings!");
    }
  }

  /**
   * bool: weither or not the status code is in {2xx}
   */
  get ok() {
    return this._apiCode === ApiCodes.Ok;
  }

  /**
   * if warnings occured at API Call
   */
  get withWarnings(){
    return this._apiCode === ApiCodes.WithWarnings;
  }

  /**
   * if status is 400
   */
  get serverValidationError() {
    return this._apiCode === ApiCodes.ServerValidationError;
  }

  /**
   * if status is 404
   */
  get notFound() {
    return this._apiCode === ApiCodes.NotFound;
  }

  /**
   * if status is 500
   */
  get internalError() {
    return this._apiCode === ApiCodes.InternalError;
  }

  /**
   * an unmanaged error, e.g. a status code not in {2xx,404,400,500}
   */
  get unknown() {
    return this._apiCode === ApiCodes.Unknown;
  }

  get integrityViolation(){
    return this._apiCode === ApiCodes.IntegrityViolation;
  }
}

export { ApiCodes };
export default Result;
