/**
 * Author: Anton Rhein
 * Date: 2020-06-11
 */

import ApiResult, { ApiCodes } from "./ApiResult";

let id = 0;

function addSelf(data){
  let result = Object.assign({},data);
  result._links = {
    self: {
      href: `abcdef${++id}`
    }
  };
  return result;
}

class Backend {
  post = async (entity, data) => {
    await new Promise((resolve, reject) => {
      process.nextTick(() => {
        resolve();
      })
    });
    return new ApiResult(ApiCodes.Ok, addSelf(data));
  };

  patch = async (entity, data) => {
    await new Promise((resolve, reject) => {
      process.nextTick(() => {
        resolve();
      })
    });
    return new ApiResult(ApiCodes.Ok, data);
  };

  
  delete = async (uri) => {
    return new Promise((resolve, reject) => {
      process.nextTick(() => {
        resolve(new ApiResult(ApiCodes.Ok));
      })
    });
  };

  put = (entity, data) => {
    new Promise((resolve, reject) => {
      process.nextTick(() => {
        resolve();
      })
    });
    return new ApiResult(ApiCodes.Ok, data)
  };
}

export default new Backend();
export { ApiResult };
