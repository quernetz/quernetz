/**
 * Author: Hartmut Knaack
 *
 * @fileoverview Helper functions to retrieve information of the backend server
 * defined in .env environment files.
 */

/**
 * Wrap undefined variables to empty strings.
 *
 * @param {variable} variable  A pontentially undefined variable to return its content.
 * @return {variable} The content of the variable (empty String if undefined).
 */
function wrapUndefined(variable) {
  return ((typeof variable === 'undefined') ? '' : variable);
}

/**
 * Return Hostname of the backend server specified in .env file.
 *
 * @return {string} The Hostname of the backend server.
 */
export function hostname() {
  return wrapUndefined(process.env.REACT_APP_BACKEND_HOSTNAME);
}

/**
 * Return Port number of the backend server specified in .env file.
 *
 * @return {variable} The Port number of the backend server.
 */
export function portNr() {
  return wrapUndefined(process.env.REACT_APP_BACKEND_PORT);
}

/**
 * Return base directory prefix of the backend server specified in .env file.
 *
 * @return {string} The base directory prefix of the backend server.
 */
export function baseDirectory() {
  return wrapUndefined(process.env.REACT_APP_BACKEND_BASEDIR);
}

/**
 * Return protocol of the backend server specified in .env file.
 *
 * @return {string} The protocol of teh backend server.
 */
export function protocol() {
  return wrapUndefined(process.env.REACT_APP_BACKEND_PROTOCOL);
}

/**
 * Return URL of the backend server as specified in .env file
 *
 * @return {string} The URL of the backend server, containing protocol,
 *                  hostname, port and base directory.
 */
export default function backendUrl() {
  const proto = (protocol() === '' ? '' : protocol() + ':');
  const host = (hostname() === '' ? '' : '//' + hostname());
  const port = (portNr() === '' ? '' : ':' + portNr());

  /* To do: Throw Exception on given protocol but empty hostname? */
  return proto + host + port + baseDirectory();
}
