/**
 * Author: Hartmut Knaack
 * 
 * @fileoverview Test cases for authentication management
 */

import Auth from "./Auth.js";

let payload = {
    "sub" : null,
    "exp" : null,
    "iat" : null
};

/**
 * generate a mock JWT with random generated payload content
 * - exp: expiration time is [10-110] seconds in the future or past
 * - iat: right now
 * - sub: random string of length [5-50]
 * 
 * @param {boolean} true, if it should be expired already; false, if it should be valid
 */
function generateMockToken(isExpired = false, hasExp = true, hasWrongTypeExp = false,
                           hasSub = true, hasWrongTypeSub = false) {
    const header = {
        "alg" : "HS256",
        "typ" : "JWT"
    };
    const currentTime = Math.floor(Date.now() / 1000);
    const offsetSeconds = Math.floor(Math.random() * 100) + 10;
    const name = Math.random().toString(36).substr(2, Math.floor(Math.random() * 100) + 5);
    const signature = Math.random().toString(36).substr(2, 44);
    const expiration = (isExpired ? (currentTime - offsetSeconds) : (currentTime + offsetSeconds));
    const exp = (hasWrongTypeExp ? "abc" : expiration);
    const subject = (hasWrongTypeSub ? Math.random() * 1e10 : name);
    payload = {
        "iat" : currentTime
    }
    if (hasExp) {
        payload.exp = exp;
    }

    if (hasSub) {
        payload.sub = subject;
    }

    return window.btoa(JSON.stringify(header)) + '.' + window.btoa(JSON.stringify(payload)) + '.' + signature;
}

describe("Auth", () => {
    beforeEach(() => sessionStorage.setItem("token", generateMockToken(false, true, false, true, false)));

    afterEach(() => sessionStorage.removeItem("token"));

  test("Auth.authenticateUser()", () => {
        /* Precondition: no token stored
         * Tests:
         *  - using valid token:
         *      + no error thrown
         *      + object "token" is stored in sessionStorage
         *  - using expired token:
         *      + throws an error
         *      + object "token" does not exist in sessionStorage
         */
        sessionStorage.removeItem("token");

        const validToken = generateMockToken(false, true, false, true, false);

        expect(() => {Auth.authenticateUser(validToken);}).not.toThrowError("expired");
        expect(Object.is(sessionStorage.getItem("token"), validToken)).toBe(true);

        sessionStorage.removeItem("token");

        const invalidToken = generateMockToken(true, true, false, true, false);

        expect(() => {Auth.authenticateUser(invalidToken);}).toThrowError("expired");
        expect(Object.is(sessionStorage.getItem("token"), validToken)).toBe(false);

});

    test("Auth.deauthenticateUser()", () => {
        /* Precondition: none
         * Tests:
         *  - token exists before deauthenticateUser is called
         *  - does not throw exception on regular usage
         *  - token entry in sessionStorage is null after deauthenticateUser has been called
         *  - calling deauthenticateUser without existing token in sessionStorage throws no error
         */
        expect(sessionStorage.getItem("token")).not.toBeNull();
        expect(() => {Auth.deauthenticateUser();}).not.toThrow();
        expect(sessionStorage.key("token")).toBeNull();
        expect(() => {Auth.deauthenticateUser();}).not.toThrow();
    });

    test("Auth.getToken()", () => {
        /* Precondition: none
         * Tests:
         *  - existing token in sessionStorage:
         *      + no error thrown
         *      + received object is not null
         *  - no existing token in sessionStorage:
         *      + no error thrown
         *      + received object is null
         */
        expect(() => {Auth.getToken();}).not.toThrow();
        expect(Auth.getToken()).not.toBeNull();

        sessionStorage.removeItem("token");
        expect(() => {Auth.getToken();}).not.toThrow();
        expect(Auth.getToken()).toBeNull();
    });

    test("Auth.getTokenExpirationTime()", () => {
        /* Precondition: none
         * Tests:
         *  - invalid Token format:
         *      + error thrown
         *  - token missing exp property:
         *      + error thrown
         *  - token with wrong exp property type:
         *      + error thrown
         * - valid token:
         *      + no error thrown
         *      + tokens exp property equals payload.exp
         */
        const invalidFormatedToken = Math.random().toString(36).substr(2, 100);
        expect(() => {Auth.getTokenExpirationTime(invalidFormatedToken);}).toThrowError("Token payload extraction failed");

        const missingExpToken = generateMockToken(false, false, false, true, false);
        expect(() => {Auth.getTokenExpirationTime(missingExpToken);}).toThrowError("Token payload misses Property: exp");

        const wrongTypeExpToken = generateMockToken(false, true, true, true, false);
        expect(() => {Auth.getTokenExpirationTime(wrongTypeExpToken);}).toThrowError("Token expiration property is not a number");

        const validToken = generateMockToken(false, true, false, true, false);
        expect(() => {Auth.getTokenExpirationTime(validToken);}).not.toThrow();
        expect(Auth.getTokenExpirationTime(validToken)).toBe(payload.exp);
    });

    test("Auth.getTokenSubject()", () => {
        /* Precondition: none
         * Tests:
         *  - invalid Token format:
         *      + error thrown
         *  - token missing sub property:
         *      + error thrown
         *  - token with wrong sub property type:
         *      + error thrown
         * - valid token:
         *      + no error thrown
         *      + tokens sub property equals payload.sub
         */
        const invalidFormatedToken = Math.random().toString(36).substr(2, 100);
        expect(() => {Auth.getTokenSubject(invalidFormatedToken);}).toThrow();

        const missingSubToken = generateMockToken(false, true, false, false, false);
        expect(() => {Auth.getTokenSubject(missingSubToken);}).toThrow();

        const wrongTypeSubToken = generateMockToken(false, true, false, true, true);
        expect(() => {Auth.getTokenSubject(wrongTypeSubToken);}).toThrow();

        const validToken = generateMockToken(false, true, false, true, false);
        expect(() => {Auth.getTokenSubject(validToken);}).not.toThrow();
        expect(Auth.getTokenSubject(validToken)).toBe(payload.sub);
    });

    test("Auth.isUserAuthenticated()", () => {
        /* Precondition: none
         * Tests:
         *  - default condition (valid token stored in sessionStorage):
         *      + results in true
         *      + sessionStorage contains key "token"
         *  - no token stored in sessionStorage:
         *      + results in false
         *      + sessionStorage contains no key "token"
         *  - expired token stored in sessionStorage:
         *      + results in false
         *      + sessionStorage contains no key "token"
         *  - invalid formated token stored in sessionStorage
         *      + results in false
         */
        expect(Auth.isUserAuthenticated()).toBe(true);
        expect(sessionStorage.key("token")).not.toBeNull();

        sessionStorage.removeItem("token");
        expect(sessionStorage.key("token")).toBeNull();
        expect(Auth.isUserAuthenticated()).toBe(false);
        expect(sessionStorage.key("token")).toBeNull();

        sessionStorage.setItem("token", generateMockToken(true, true, false, true, false));
        expect(sessionStorage.key("token")).not.toBeNull();
        expect(Auth.isUserAuthenticated()).toBe(false);
        expect(sessionStorage.key("token")).toBeNull();

        const invalidFormatedToken = Math.random().toString(36).substr(2, 100);
        sessionStorage.setItem("token", invalidFormatedToken);
        expect(Auth.isUserAuthenticated()).toBe(false);
        expect(sessionStorage.key("token")).not.toBeNull();
    });
});
