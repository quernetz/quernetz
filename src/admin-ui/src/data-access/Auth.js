/**
 * Author: Hartmut Knaack
 *
 * @fileoverview JSON web token authentication management.
 * Inspired by: http://vladimirponomarev.com/blog/authentication-in-react-apps-jwt
 */
export default class Auth {
    /**
     * Get token payload as JSON
     * 
     * @returns {string} JWT payload as JSON
     * @throws DOMException if payload is not valid base64 encoded
     * @throws SyntaxError if decoded payload is no valid JSON
     */
    static getTokenPayload(token) {
        const splitToken = token.split('.');

        if (splitToken.length !== 3) {
            throw new Error("Not a valid JWT!");
        }

        const base64 = splitToken[1].replace('-', '+').replace('_', '/');

        return JSON.parse(window.atob(base64));
    }

    /**
     * Get token expiration time
     *
     * @returns {int} Expiration time in Unix time format (seconds)
     * @throws Error if no valid expiration time type was found
     */
    static getTokenExpirationTime(token) {
        let payload;

        try {
            payload = this.getTokenPayload(token);
        } catch (error) {
            throw new Error("Token payload extraction failed: " + error.message);
        }

        if (!payload.hasOwnProperty('exp')) {
            throw new Error("Token payload misses Property: exp");
        }

        if (typeof(payload.exp) !== 'number') {
            throw new Error("Token expiration property is not a number");
        }

        return payload.exp;
    }

    /**
     * Get token subject
     *
     * @returns {string} subject of the JWT (the username)
     * @throws Error if no valid subject was found
     */
    static getTokenSubject(token) {
        let payload;

        try {
            payload = this.getTokenPayload(token);
        } catch (error) {
            throw new Error("Token payload extraction failed: " + error.message);
        }

        if (!payload.hasOwnProperty('sub')) {
            throw new Error("Token payload misses Property: sub");
        }

        if (typeof(payload.sub) !== 'string') {
            throw new Error("Token subject property is not a string");
        }

        return payload.sub;
    }

    /**
     * Authenticate a user by saving a token string in sessionStorage.
     * 
     * @param {string}  JSON web token
     * @throws Error if token is invalid
     * @throws Exception if storage is full
     */
    static authenticateUser(token) {
        const tokenExpirationTime = this.getTokenExpirationTime(token);

        if (tokenExpirationTime <= Math.floor(Date.now() / 1000)) {
            throw new Error("Token already expired");
        }

        sessionStorage.setItem("token", token);
    }

    /**
     * Get a token value.
     * 
     * @returns {string}
     */
    static getToken() {
        return sessionStorage.getItem("token");
    }

    /**
     * Check if a user is authenticated by checking if a token is in sessionStorage.
     * 
     * @returns {boolean}
     */
    static isUserAuthenticated() {
        const token = this.getToken();

        if (token !== null) {
            try {
                const tokenExpirationTime = this.getTokenExpirationTime(token);

                if (tokenExpirationTime <= Math.floor(Date.now() / 1000)) {
                    this.deauthenticateUser();

                    return false;
                }
            } catch (error) {
                console.log(error.message);

                return false;
            }
        }

        return (token !== null);
    }

    /**
     * Deauthenticate a user by removing token from sessionStorage.
     * 
     */
    static deauthenticateUser() {
        sessionStorage.removeItem("token");
    }
}
