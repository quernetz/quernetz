import React from "react";
import { NavLink, Redirect } from "react-router-dom";
import "./Nav.css";
import Authentication from "../../components/Authentication/Authentication.js";


const linkStyle = {
    color: 'lightgrey',
    paddingLeft: '20px',
    paddingRight: '20px',
    paddingBottom: '10px',
    textDecoration: 'none',
}

const activeStyle = {
    color: 'white',
    backgroundColor: '#342854',
    borderBottom: '2px solid #00987a'
}

export default class Nav extends React.Component {

    authenticatedNavigation(isAuthenticated) {
        if (isAuthenticated) {
            return (
                <form className="form-inline">
                    <ul className="navbar-nav">
                        <NavLink style={linkStyle} activeStyle={activeStyle} exact to={"/organisationen" && "/"}>
                            <li className="nav-item">Organisationen</li>
                        </NavLink>
                        <NavLink style={linkStyle} activeStyle={activeStyle} exact to="/projekte">
                            <li className="nav-item">Projekte</li>
                        </NavLink>
                        <NavLink style={linkStyle} activeStyle={activeStyle} exact to="/interviews">
                            <li className="nav-item">Interviews</li>
                        </NavLink>
                        <NavLink style={linkStyle} activeStyle={activeStyle} exact to="/kategorien">
                            <li className="nav-item">Kategorien</li>
                        </NavLink>
                        <NavLink style={linkStyle} activeStyle={activeStyle} exact to="/tags">
                            <li className="nav-item">Tags</li>
                        </NavLink>
                    </ul>
                </form>
            );
        } else {
            return (
                <Redirect to="/welcome" />
            );
        }
    }

    render() {
        const isAuthenticated = this.props.authenticated;

        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark justify-content-between">
                <div className="navbar-brand">
                    Das Quernetz-Inhaltspflege
                </div>
                {this.authenticatedNavigation(isAuthenticated)}
                <Authentication updateAuthenticationStatus={this.props.updateAuthenticationStatus} />
            </nav>
        )
    }
};
