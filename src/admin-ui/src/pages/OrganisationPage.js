import React from "react";
import fetchData from "../data-access/fetchData";
import BackendUrl from "../data-access/backendconfig";
import Backend from "../data-access/Backend";
import OrganisationTable from "../components/Organisations/OrganisationTable";
import fetchLogo from "../data-access/fetchLogo";
import {extractIdFromSelf} from "../utils/extractIdFromSelf";

export default class OrganisationPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            organisations: [],
            isDataFetched: false
        };
    }

    componentDidMount = async () => {
        if (this.state.isDataFetched) {
            return;
        }
        const organisations = await fetchData(BackendUrl() + "/api/organisations?projection=withTagsCategoriesProjectsAndInterviews");
        for (const org of organisations._embedded.organisations) {
            const logo = await fetchLogo(extractIdFromSelf(org._links.self.href));
            if (logo) {
                org.logo = logo;
            }
        }

        this.setState({
            organisations: organisations._embedded.organisations,
            isDataFetched: true,
        });
    };

    render() {
        return (
            this.state.isDataFetched &&
            <OrganisationTable backend={Backend} organisations={this.state.organisations}/>
        );
    }
}