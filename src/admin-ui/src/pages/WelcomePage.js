import React from "react";
import "./WelcomePage.css";
import "bootstrap";

export default class WelcomePage extends React.Component {

    render() {
        return (
            <div className="alert alert-primary" id="welcomePageContent" role="alert">
                Bitte loggen Sie sich ein, um Inhalte bearbeiten zu können.
            </div>
        );
    }
}
