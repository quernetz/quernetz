import React from "react";
import Backend from "../data-access/Backend";
import fetchData from "../data-access/fetchData";
import BackendUrl from "../data-access/backendconfig";
import CategoriesList from "../components/Lists/CategoriesList";


export default class CategoryPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            isDataFetched: false
        };
    }

    componentDidMount = async () => {
        const categories = await fetchData(BackendUrl() + "/api/categories");

        this.setState({
            categories: categories._embedded.categories,
            isDataFetched: true
        });
    };

    render() {
        return (
            this.state.isDataFetched &&
            <CategoriesList backend={Backend} categories={this.state.categories} />
        );
    }
}