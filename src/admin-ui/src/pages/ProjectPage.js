import React from "react";
import fetchData from "../data-access/fetchData";
import BackendUrl from "../data-access/backendconfig";
import Backend from "../data-access/Backend";
import ProjectTable from "../components/Project/ProjectTable";

export default class ProjectPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            projects: [],
            isDataFetched: false
        };
    }

    componentDidMount = async () => {
        if (this.state.isDataFetched) {
            return;
        }
        const projects = await fetchData(BackendUrl() + "/api/projects?projection=withTagsCategoriesOrganisationsAndInterviews");

        this.setState({
            projects: projects._embedded.projects,
            isDataFetched: true,
        });
    };

    render() {
        return (
            this.state.isDataFetched &&
            <ProjectTable backend={Backend} projects={this.state.projects}/>
        );
    }
}