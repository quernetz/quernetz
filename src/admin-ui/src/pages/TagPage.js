import React from "react";
import Backend from "../data-access/Backend";
import fetchData from "../data-access/fetchData";
import BackendUrl from "../data-access/backendconfig";
import TagsList from "../components/Lists/TagsList";

export default class TagPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tags: [],
            isDataFetched: false
        };
    }

    componentDidMount = async () => {
        const tags = await fetchData(BackendUrl() + "/api/tags");

        this.setState({
            tags: tags._embedded.tags,
            isDataFetched: true
        });
    };

    render() {
        return (
            this.state.isDataFetched &&
            <TagsList backend={Backend} tags={this.state.tags} />
        );
    }
}