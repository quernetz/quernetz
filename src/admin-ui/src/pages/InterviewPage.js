import React from "react";
import fetchData from "../data-access/fetchData";
import BackendUrl from "../data-access/backendconfig";
import Backend from "../data-access/Backend";
import InterviewTable from "../components/Interviews/InterviewTable";

export default class InterviewPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            interviews: [],
            isDataFetched: false
        };
    }

    componentDidMount = async () => {
        if (this.state.isDataFetched) {
            return;
        }
        const interviews = await fetchData(BackendUrl() + "/api/interviews?projection=withOrganisations");
        this.setState({
            interviews: interviews._embedded.interviews,
            isDataFetched: true
        });
    };

    render() {
        return (
            this.state.isDataFetched &&
            <InterviewTable backend={Backend} interviews={this.state.interviews}/>
        );
    }
}