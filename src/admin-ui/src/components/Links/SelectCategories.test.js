import React from 'react';
import ReactDOM from "react-dom";
import SelectCategories from "./SelectCategories";
import OrganisationForm from "../Organisations/OrganisationForm";
import ProjectForm from "../Project/ProjectForm";

let categories = [
    {
        categoryName: 'Müll'
    },
    {
        categoryName: 'Umwelt'
    }
];

describe('tests select form for choosing categories', () => {

    
    it('renders multi selector without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<SelectCategories/>, div)
    });

    test('tests if handleSelect passes state correctly', () => {
        // shallow render Organisation Form
        const wrapper = shallow(<OrganisationForm/>);

        //tests if right amount of categories is passed through handleSelect-function
        const instance = wrapper.instance();
        instance.handleCategorySelect(categories);
        expect(wrapper.state('selectedCategories').length).toBe(2);
    })
})