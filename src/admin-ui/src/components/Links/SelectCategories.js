import React from 'react';
import Select from 'react-select';
import fetchData from "../../data-access/fetchData.js";
import BackendUrl from "../../data-access/backendconfig";


export default class SelectCategory extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            selectedCategories: this.props.selectedCategories
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.connectedCategories !== prevProps.connectedCategories) {
            this.setState({selectedCategories: this.props.connectedCategories})
        }
    }

    handleChange = (selectedCategories) => {
        this.setState({selectedCategories: selectedCategories});
        this.props.setCategories(selectedCategories);
    }

    componentDidMount() {
        fetchData(BackendUrl() + '/api/categories').then((data) => {
            this.setState({categories: data._embedded.categories});
        });
    }

    render() {
        return (
            <Select
                closeMenuOnSelect={false}
                isMulti
                options={this.state.categories}
                getOptionLabel={(option) => option.categoryName}
                getOptionValue={((option) => option.categoryName)}
                value={this.state.selectedCategories}
                onChange={this.handleChange}
            />
        )
    }
}