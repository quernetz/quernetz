import React from 'react';
import ReactDOM from "react-dom";
import SelectDate from "./SelectDate";
import ProjectForm from "../Project/ProjectForm";

let exampleDate = "2020-04-05";

describe('tests date  picker', () => {

    it('renders date picker without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<SelectDate/>, div)
    });

    test('tests if date selector passes state correctly', () => {
        // shallow render Project Form
        const wrapper = shallow(<ProjectForm/>);

        const instance = wrapper.instance();
        //tests if date is passed correctly through handleStartDateSelect
        instance.handleStartDateSelect(exampleDate);
        expect(wrapper.state('selectedStartDate')).toEqual(exampleDate)

        //tests if date is passed correctly through handleEndDateSelect
        instance.handleEndDateSelect(exampleDate);
        expect(wrapper.state('selectedEndDate')).toEqual(exampleDate)
    })
})