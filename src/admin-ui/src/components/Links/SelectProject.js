import React from 'react';
import Select from 'react-select';
import fetchData from "../../data-access/fetchData.js";
import BackendUrl from "../../data-access/backendconfig";


export default class SelectProject extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            projects: [],
            selectedProjects: this.props.selectedProjects
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.selectedProjects !== prevProps.selectedProjects) {
            this.setState({selectedProjects: this.props.selectedProjects})
        }
    }

    handleChange = (selectedProjects) => {
        this.setState({selectedProjects: selectedProjects});
        this.props.setProjects(selectedProjects);
    }

    componentDidMount() {
        fetchData(BackendUrl() + '/api/projects').then((data) => {
            this.setState({projects: data._embedded.projects});
        });
    }

    render() {
        return (
            <Select
                closeMenuOnSelect={false}
                isMulti
                options={this.state.projects}
                getOptionLabel={(option) => option.name}
                getOptionValue={((option) => option.name)}
                value={this.state.selectedProjects}
                onChange={this.handleChange}
            />
        )
    }
}