import React from 'react';
import Select from 'react-select';
import fetchData from "../../data-access/fetchData.js";
import BackendUrl from "../../data-access/backendconfig";


export default class SelectOrganisation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            organisations: [],
            selectedOrganisations: this.props.selectedOrganisations,
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.connectedOrganisation !== prevProps.connectedOrganisation) {
            this.setState({selectedOrganisations: this.props.connectedOrganisation})
        }
    }

    handleChange = (selectedOrganisations) => {
        this.setState({selectedOrganisations: selectedOrganisations});
        this.props.setOrganisations(selectedOrganisations);
    }

    componentDidMount() {
        fetchData(BackendUrl() + '/api/organisations').then((data) => {
            this.setState({organisations: data._embedded.organisations});
        });
    }

    render() {
        return (
            <Select
                closeMenuOnSelect={false}
                isMulti
                options={this.state.organisations}
                getOptionLabel={(option) => option.name}
                getOptionValue={((option) => option.name)}
                value={this.state.selectedOrganisations}
                onChange={this.handleChange}
            />
        )
    }
}



