import React from 'react';
import ReactDOM from "react-dom";
import SelectProject from "./SelectOrganisation";
import OrganisationForm from "../Organisations/OrganisationForm";

let projects = [
    {
        name: 'Erstes Projekt'
    },
    {
        name: 'Projekt im Mai'
    }
];

describe('tests select form for choosing projects', () => {

    it('renders multi selector without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<SelectProject/>, div)
    });

    test('tests if handleSelect passes state correctly', () => {
        // shallow render Project Form with fake organisations
        const wrapper = shallow(<OrganisationForm/>);

        //tests if right amount of projects is passed through handleSelect-function
        const instance = wrapper.instance();
        instance.handleProjectSelect(projects);
        expect(wrapper.state('selectedProjects').length).toBe(2);
    })
})