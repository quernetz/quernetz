import React from 'react';
import ReactDOM from "react-dom";
import SelectOrganisation from "./SelectOrganisation";
import ProjectForm from "../Project/ProjectForm";

let organisations = [
    {
        name: 'Erste Organisation'
    },
    {
        name: 'Zweite Organisation'
    }
];

describe('tests select form for choosing organisations', () => {

        it('renders multi selector without crashing', () => {
            const div = document.createElement('div');
            ReactDOM.render(<SelectOrganisation/>, div)
        });

        test('tests if handleSelect passes state correctly', () => {
            // shallow render Project Form with fake organisations
            const wrapper = shallow(<ProjectForm/>);

            //tests if right amount of organisations is passed through handleSelect-function
            const instance = wrapper.instance();
            instance.handleOrganisationSelect(organisations);
            expect(wrapper.state('selectedOrganisations').length).toBe(2);
        })
})