import React from 'react';
import '../Styles/SelectDateStyle.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default class SelectDate extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            date: this.props.date
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.date !== prevProps.date) {
            this.setState({date: this.props.date})
        }
    }

    handleChange = (selectedDate) => {
        this.setState({date: selectedDate});
        this.props.setDate(selectedDate);
    }

    render() {
        return (
            <div>
                <DatePicker
                    onSelect={this.handleChange}
                    selected={this.state.date}
                    onChange={this.handleChange}
                    dateFormat="dd.MM.yyyy"
                    placeholderText="Datum auswählen"
                />
            </div>
        );
    }
}
