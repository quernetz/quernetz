import React from 'react';
import Select from 'react-select';
import fetchData from "../../data-access/fetchData.js";
import BackendUrl from "../../data-access/backendconfig";


export default class SelectInterview extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            interviews: [],
            selectedInterviews: this.props.selectedInterviews
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.connectedInterviews !== prevProps.connectedInterviews) {
            this.setState({selectedInterviews: this.props.connectedInterviews})
        }
    }

    handleChange = (selectedInterviews) => {
        this.setState({selectedInterviews: selectedInterviews});
        this.props.setInterviews(selectedInterviews);
    }

    componentDidMount() {
        fetchData(BackendUrl() + '/api/interviews').then((data) => {
            this.setState({interviews: data._embedded.interviews});
        });
    }

    render() {
        return (
            <Select
                closeMenuOnSelect={false}
                isMulti
                options={this.state.interviews}
                getOptionLabel={(option) => option.name}
                getOptionValue={((option) => option.name)}
                value={this.state.selectedInterviews}
                onChange={this.handleChange}
            />
        )
    }
}