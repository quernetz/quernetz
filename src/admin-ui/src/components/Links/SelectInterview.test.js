import React from 'react';
import ReactDOM from "react-dom";
import SelectInterview from "./SelectInterview";
import OrganisationForm from "../Organisations/OrganisationForm";

let interviews = [
    {
        name: 'Quergefragt #1'
    },
    {
        name: 'Quergefragt #2'
    }
];

describe('tests select form for choosing interviews', () => {


    it('renders multi selector without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<SelectInterview/>, div)
    });

    test('tests if handleSelect passes state correctly', () => {
        // shallow render Organisation Form
        const wrapper = shallow(<OrganisationForm/>);

        //tests if right amount of interviews is passed through handleSelect-function
        const instance = wrapper.instance();
        instance.handleInterviewSelect(interviews);
        expect(wrapper.state('selectedInterviews').length).toBe(2);
    })
})