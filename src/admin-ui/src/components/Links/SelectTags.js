import React from 'react';
import Select from 'react-select';
import fetchData from "../../data-access/fetchData.js";
import BackendUrl from "../../data-access/backendconfig";


export default class SelectTags extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tags: [],
            selectedTags: this.props.selectedTags
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.selectedTags !== prevProps.selectedTags) {
            this.setState({selectedTags: this.props.selectedTags})
        }
    }

    handleChange = (selectedTags) => {
        this.setState({selectedTags: selectedTags});
        this.props.setTags(selectedTags);
    }

    componentDidMount() {
        fetchData(BackendUrl() + '/api/tags').then((data) => {
            this.setState({tags: data._embedded.tags});
        });
    }

    render() {
        return (
            <Select
                closeMenuOnSelect={false}
                isMulti
                options={this.state.tags}
                getOptionLabel={(option) => option.text}
                getOptionValue={((option) => option.text)}
                value={this.state.selectedTags}
                onChange={this.handleChange}
            />
        )
    }
}