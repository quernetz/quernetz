import React from 'react';
import ReactDOM from "react-dom";
import SelectTags from "./SelectTags";
import OrganisationForm from "../Organisations/OrganisationForm";

let tags = [
    {
        text: '#TogetherWeStrand'
    },
    {
        text: '#yesWeBeer'
    }
];

describe('tests select form for choosing tags', () => {


    it('renders multi selector without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<SelectTags/>, div)
    });

    test('tests if handleSelect passes state correctly', () => {
        // shallow render Organisation Form
        const wrapper = shallow(<OrganisationForm/>);

        //tests if right amount of tags is passed through handleSelect-function
        const instance = wrapper.instance();
        instance.handleTagSelect(tags);
        expect(wrapper.state('selectedTags').length).toBe(2);
    })
})