import renderer from "react-test-renderer";
import React from "react";
import OrganisationLogo from "./OrganisationLogo";

test("snapshot", () => {
    const tree = renderer
        .create(<OrganisationLogo/>)
        .toJSON();
    expect(tree).toMatchSnapshot();
});