import React from "react";
import "bootstrap"
import SelectCategories from "../Links/SelectCategories.js";
import SelectTags from "../Links/SelectTags";
import SelectProject from "../Links/SelectProject";
import SelectInterview from "../Links/SelectInterview"
import {isEmpty, isURL} from "validator";
import LogoUploadUI from "./logoUpload/LogoUploadUI";

export default class OrganisationForm extends React.Component {

  constructor(props) {
    super(props);
    const organisationInEdit = (typeof this.props.organisationInEdit === 'undefined') ? false : this.props.organisationInEdit;
    this.state = {
      selectedCategories: [],
      selectedTags: [],
      selectedProjects: [],
      selectedInterviews: [],
      organisationInEdit: organisationInEdit,
      validationErrors : [],
      logoDataFromChild: null,
      inEdit: this.props.inEdit
    }
  }
  logoCallback = (file) => {
    this.setState({logoDataFromChild: file});
  };

    createFalseCallback = () => {
        this.props.createFalseCallbackMain();
    };

  deleteLogoCallback = () => {
      this.props.deleteLogoCallbackMain();
  }

    componentDidUpdate = async (prevProps) => {
        if (this.props.inEdit !== prevProps.inEdit) {
      await this.clearForm();
      await this.setState({inEdit: this.props.inEdit});
        }
        if (this.props.organisationInEdit !== prevProps.organisationInEdit && typeof this.props.organisationInEdit !== 'undefined') {
            this.nameInput.value = this.props.organisationInEdit.name;
            this.linkInput.value = this.props.organisationInEdit.website;
            this.descInput.value = this.props.organisationInEdit.description;

           await this.setState({
                organisationInEdit: this.props.organisationInEdit,
                selectedCategories: (this.props.organisationInEdit.selectedCategories) ? this.props.organisationInEdit.selectedCategories : [],
                selectedTags: (this.props.organisationInEdit.selectedTags) ? this.props.organisationInEdit.selectedTags : [],
                selectedProjects: (this.props.organisationInEdit.selectedProjects) ? this.props.organisationInEdit.selectedProjects : [],
                selectedInterviews: (this.props.organisationInEdit.selectedInterviews) ? this.props.organisationInEdit.selectedInterviews : []
            });
        }
      if (this.props.connectedTags !== prevProps.connectedTags) {
        this.setState({selectedTags: this.props.connectedTags})
      }
      if (this.props.connectedCategories !== prevProps.connectedCategories) {
        this.setState({selectedCategories: this.props.connectedCategories})
      }
      if (this.props.connectedProjects !== prevProps.connectedProjects) {
        this.setState({selectedProjects: this.props.connectedProjects})
      }
      if (this.props.connectedInterviews !== prevProps.connectedInterviews) {
        this.setState({selectedInterviews: this.props.connectedInterviews})
      }
      if (typeof this.props.organisationInEdit !== 'undefined' && this.props.organisationInEdit !== prevProps.organisationInEdit) {
        this.nameInput.value = this.props.organisationInEdit.name;
        this.linkInput.value = this.props.organisationInEdit.website;
        this.descInput.value = this.props.organisationInEdit.description;
      }
    }

  handleCategorySelect = (selectedCategories) => {
    this.setState({selectedCategories: selectedCategories})
  }

  handleTagSelect = (selectedTags) => {
    this.setState({selectedTags: selectedTags})
  }

  handleProjectSelect = (selectedProjects) => {
    this.setState({selectedProjects: selectedProjects})
  }

  handleInterviewSelect = (selectedInterviews) => {
    this.setState({selectedInterviews: selectedInterviews})
  }

    validateInputs = () => {
        let errors = [];
        if (isEmpty(this.nameInput.value)) {
            errors.push({
                field: "name",
                reason: "Ein Organisationsname ist erforderlich!"
            })
        }
        if (!isEmpty(this.linkInput.value)) {
            if (!isURL(this.linkInput.value, {
                require_protocol: true,
                protocols: ["http", "https"],
            })) {
                errors.push({
                    field: "link",
                    reason: "Wenn angegeben muss es sich um eine gültige URL handeln!"
                });
            }
        }
        if (isEmpty(this.descInput.value)) {
            errors.push({
                field: "description",
                reason: "Eine Beschreibung ist erforderlich!"
            })
        }
        if (!(this.state.selectedCategories) || this.state.selectedCategories.length === 0) {
            errors.push({
                field: "categories",
                reason: "Es muss mindestens eine Kategorie gewählt werden!"
            })
        }
        this.setState({
            validationErrors: errors
        });
        return (errors.length <= 0);
    }

  hasError = (field) => this.state.validationErrors.some(e => e.field === field)

  getError = (field) => {
    let error = this.state.validationErrors.find(e => e.field === field);
    if(error){
      return error.reason;
    }
    return "";
  }

    onSubmit = (event) => {
        this.props.createCallback();
        event.preventDefault();
        const isValid = this.validateInputs();
        if (isValid) {
            let newOrganisation = {
                name: this.nameInput.value,
                website: this.linkInput.value,
                description: this.descInput.value,
                categories: this.state.selectedCategories,
                tags: this.state.selectedTags,
                projects: this.state.selectedProjects,
                interviews: this.state.selectedInterviews
            }
            if (this.props.inEdit) {
                newOrganisation.uri = this.props.organisationInEdit._links.self.href;
            }
            this.props.onSave(newOrganisation, this.state.logoDataFromChild);
            this.clearForm();
        }
    }

    clearForm = async () => {
        await this.setState({
            selectedCategories: [],
            selectedTags: [],
            selectedProjects: [],
            selectedInterviews: [],
            validationErrors: [],
        })
        this.setState({logoDataFromChild: null})
    }

    render() {
        return (
            <div>
                <div className="modal fade" id="OrganisationModal" tabIndex="-1" role="dialog"
                     aria-labelledby="OrganisationModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="OrganisationModal">Organisation erstellen</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"
                                        onClick={ () => {
                                            this.clearForm();
                                            this.props.createCallback();
                                        }}>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form id="organisation-form" onSubmit={this.onSubmit}>
                                    <div className="form-group">
                                        <label>Name:</label>
                                        <input
                                            className={`form-control ${this.hasError("name") ? "is-invalid" : ""}`}
                                            placeholder="Name eingeben"
                                            ref={nameInput => this.nameInput = nameInput}/>
                                        <div className="invalid-feedback">
                                            {this.getError("name")}
                                        </div>
                                    </div>
                                  <div className="form-group">
                                    <label>Logo:</label>
                                    <LogoUploadUI
                                        organisationLogo={this.props.organisationLogo}
                                        callbackFromParent={this.logoCallback}
                                        create={this.props.create}
                                        deleteLogo={this.props.deleteLogo}
                                        deleteLogoCallback={this.deleteLogoCallback}
                                        createFalseCallback={this.createFalseCallback}
                                    />
                                  </div>
                                    <div className="form-group">
                                        <label>Link</label>
                                        <input
                                            className={`form-control ${this.hasError("link") ? "is-invalid" : ""}`}
                                            placeholder="Link eingeben"
                                            ref={linkInput => this.linkInput = linkInput}/>
                                        <div className="invalid-feedback">
                                            {this.getError("link")}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>Beschreibung:</label>
                                        <input
                                            className={`form-control ${this.hasError("description") ? "is-invalid" : ""}`}
                                            placeholder="Beschreibung eingeben"
                                            ref={descInput => this.descInput = descInput}/>
                                        <div className="invalid-feedback">
                                            {this.getError("description")}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>Kategorien:</label>
                                        <SelectCategories className="form-control"
                                                          selectedCategories={this.state.selectedCategories}
                                                          connectedCategories={this.props.connectedCategories}
                                                          setCategories={this.handleCategorySelect}/>
                                        <input hidden className={this.hasError("categories") ? "is-invalid" : ""}/>
                                        <div className="invalid-feedback">
                                            {this.getError("categories")}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>Tags:</label>
                                        <SelectTags className="form-control" selectedTags={this.state.selectedTags}
                                                    connectedTags={this.props.connectedTags}
                                                    setTags={this.handleTagSelect}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Verknüpfte Projekte:</label>
                                        <SelectProject className="form-control"
                                                       selectedProjects={this.state.selectedProjects}
                                                       connectedProjects={this.state.connectedProjects}
                                                       setProjects={this.handleProjectSelect}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Verknüpfte Interviews:</label>
                                        <SelectInterview className="form-control"
                                                         selectedInterviews={this.state.selectedInterviews}
                                                         connectedInterviews={this.props.connectedInterviews}
                                                         setInterviews={this.handleInterviewSelect}/>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal"
                                        onClick={ () => {
                                            this.clearForm();
                                            this.props.createCallback();
                                        }}>Schließen
                                </button>
                                <button type="button" className="btn btn-primary" onClick={this.onSubmit} /*onClick={ () => {
                                    this.onSubmit();
                                    this.props.createCallback();
                                        }}*/>
                                    Organisation speichern
                                </button>
                                {this.props.serverConnectionError
                                    ? (<div className="alert alert-danger"> Fehler beim Zugriff auf den
                                        Server! </div>)
                                    : ("")
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}