import {shallow} from "enzyme";
import OrganisationTable from "./OrganisationTable";
import Backend from "../../data-access/__mocks__/Backend";
import React from "react";

const organisations = [
    {
        name: 'Stadtreinigung',
        logo: 'Logo Stadtreinigung',
        website: 'https://www.stadtreinigung-leipzig.de',
        description: 'Die Stadtreinigung der Stadt Leipzig',
        _links: {self: {href: "http://localhost:8080/api/organisations/9"}}
    },
    {
        name: 'Druckerei Monochrome',
        logo: 'Logo Druckerei',
        website: 'https://www.monochrome.de',
        description: 'Die Leipziger Druckerei Monochrome',
        _links: {self: {href: "http://localhost:8080/api/organisations/15"}}
    }
];

const categories = [
    {
        categoryName: 'Müll',
        _links: {self: {href: "http://localhost:8080/api/categories/4"}}
    },
    {
        categoryName: 'Umwelt',
        _links: {self: {href: "http://localhost:8080/api/categories/5"}}
    }
];

const tags = [
    {
        text: '#unitedWeStrand',
        _links: {self: {href: "http://localhost:8080/api/tags/1"}}
    }
];

const interviews = [
    {
        name: 'Quergefragt #1 - Patrice von Leipspeis',
        link: 'https://leipspeis.de/',
        youtubeLink: 'https://www.youtube.com/embed/2es-4oQgLTU',
        date: '2020-05-2021',
        description: '"Stolz präsentieren wir unsere neue Interview-Rubrik Quergefragt! In der ersten Folge verrät uns Patrice von Leipspeis ...',
        imageUrls: ["https://www.duckduckgo.com", "https.//wikipedia.com"],
        organisation: [
            {
                name: "Leipspeis"
            },
            {
                name: "Stadtreinigung"
            }
        ],
        _links: {self: {href: "http://localhost:8080/api/interviews/24"}}
    },
    {
        name: 'Quergefragt #2 - Lisa & Eberhard vom Cafe kaputt',
        link: 'https://www.leipzig-leben.de/cafe-kaputt-leipzig/',
        youtubeLink: 'https://www.youtube.com/embed/Ocrs0NJd§YA',
        date: '2020-05-2021',
        description: '"In der zweiten Folge verraten uns Lisa und Eberhard vom Cafe kaputt, warum sich ...',
        imageUrls: ["https://www.duckduckgo.com", "https.//wikipedia.com"],
        organisation: [
            {
                name: "Cafe kaputt"
            },
            {
                name: "Leibspeis"
            }
        ],
        _links: {self: {href: "http://localhost:8080/api/interviews/25"}}
    },
];

const projects = [
    {
        description: 'Das war unser erstes Projekt',
        name: 'Erstes Projekt',
        from: '2019, 01, 01',
        to: '2019, 01, 02',
        _links: {self: {href: "http://localhost:8080/api/projects/19"}}
    },
    {
        description: 'Das war unser neues Projekt',
        name: 'Neues Projekt',
        from: '208, 03, 04',
        to: '2020, 01, 02',
        _links: {self: {href: "http://localhost:8080/api/projects/20"}}
    }
];

const categorieLinks = [
    {
        Id: 9,
        relatedObjects: [4]
    },
    {
        Id: 15,
        relatedObjects: [5]
    },
];

const projectLinks = [
    {
        Id: 9,
        relatedObjects: [19]
    },
    {
        Id: 9,
        relatedObjects: [20]
    },
];

const interviewLinks = [
    {
        Id: 15,
        relatedObjects: [24]
    },
];

const organisationTags = [
    {
        Id: 15,
        relatedObjects: [1]
    },
];


test('snapshot', () => {
    const tree = shallow(<OrganisationTable backend={Backend} organisations={organisations}/>);
    tree.instance().setState({
        isDataFetched: true,
        organisationTags: organisationTags,
        projectLinks: projectLinks,
        categorieLinks: categorieLinks,
        interviewLinks: interviewLinks
    });
    expect(tree).toMatchSnapshot();
})