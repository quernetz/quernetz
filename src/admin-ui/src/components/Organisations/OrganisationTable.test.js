import React from 'react'
import ReactDOM from 'react-dom'
import OrganisationTable from "./OrganisationTable";
import Backend from "../../data-access/__mocks__/Backend";
import {shallow} from "enzyme";

const organisations = [
    {
        name: 'Stadtreinigung',
        logo: 'testFile',
        categories: [
            {
                categoryName: 'Müll',
                _links: {self: {href: "http://localhost:8080/api/categories/4"}}
            },
            {
                categoryName: 'Umwelt',
                _links: {self: {href: "http://localhost:8080/api/categories/5"}}
            }
        ],
        interviews: [
            {
                name: 'Quergefragt #1 - Patrice von Leipspeis',
                _links: {self: {href: "http://localhost:8080/api/interviews/24"}}
            },
            {
                name: 'Quergefragt #2 - Lisa & Eberhard vom Cafe kaputt',
                _links: {self: {href: "http://localhost:8080/api/interviews/25"}}
            }
        ],
        projects: [
            {
                name: 'Neues Projekt',
                _links: {self: {href: "http://localhost:8080/api/projects/20"}}
            }
        ],
        tags: [
            {
                text: '#unitedWeStrand',
                _links: {self: {href: "http://localhost:8080/api/tags/1"}}
            }
        ],
        website: 'https://www.stadtreinigung-leipzig.de',
        description: 'Die Stadtreinigung der Stadt Leipzig',
        _links: {self: {href: "http://localhost:8080/api/organisations/9"}}
    },
    {
        name: 'Druckerei Monochrome',
        logo: 'testFile',
        categories: [
            {
                categoryName: 'Müll', _links: {self: {href: "http://localhost:8080/api/categories/4"}}
            },
            {
                categoryName: 'Umwelt', _links: {self: {href: "http://localhost:8080/api/categories/5"}}
            }
        ],
        interviews: [],
        projects: [
            {
                name: 'Erstes Projekt',
                _links: {self: {href: "http://localhost:8080/api/projects/19"}}
            },
            {
                name: 'Neues Projekt',
                _links: {self: {href: "http://localhost:8080/api/projects/20"}}
            }
        ],
        tags: [],
        website: 'https://www.monochrome.de',
        description: 'Die Leipziger Druckerei Monochrome',
        _links: {self: {href: "http://localhost:8080/api/organisations/15"}}
    }
];

const categorieLinks = [
    {
        Id: 15,
        relatedObjects: [4, 5]
    },
    {
        Id: 9,
        relatedObjects: [4, 5]
    },
];

const projectLinks = [
    {
        Id: 15,
        relatedObjects: [19, 20]
    },
    {
        Id: 9,
        relatedObjects: [20]
    },
];

const interviewLinks = [
    {
        Id: 9,
        relatedObjects: [24, 25]
    },
    {
        Id: 15,
        relatedObjects: []
    }
];

const organisationTags = [
    {
        Id: 9,
        relatedObjects: [1]
    },
    {
        Id: 15,
        relatedObjects: []
    },
];

const generateBlob = () => {
    const fakeData = [];
    while (fakeData.length < 499999) {
        fakeData.push("1");
    }

    const testFile = new Blob(fakeData);
    testFile.name = "someFile.jpg";
    return testFile
}

describe('test render', () => {
    test('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<OrganisationTable backend={Backend} organisations={organisations}/>, div)
    });
})

describe('tests organisation table', () => {
    afterEach(() => {
        delete global.fetch;
        delete global.URL.createObjectURL;
    });
    beforeEach(() => {
        global.fetch = jest.fn();
        global.URL.createObjectURL = jest.fn();
    });

    test('tests adding new organisations', async () => {
        //given
        const organisationToAdd = {
            name: "Wohnprojekt Quelle e.V.",
            categories: [
                {
                    categoryName: "Soziales",
                    _links: {self: {href: "http://localhost:8080/api/categories/1337"}}
                }
            ],
            interviews: [{name: "Quergefragt5", _links: {self: {href: "http://localhost:8080/api/interviews/0815"}}}],
            projects: [{name: "Neues Projekt", _links: {self: {href: "http://localhost:8080/api/projects/44"}}}],
            tags: [{text: "#PeopleHelpingPeople", _links: {self: {href: "http://localhost:8080/api/tags/1000"}}}],
            website: "https://www.stadtreinigung-leipzig.de",
            description: "Ein Leipziger Verein, welcher betreutes Wohnen anbietet",
            _links: {self: {href: "http://localhost:8080/api/organisations/9"}}
        }
        const logo = generateBlob();
        global.URL.createObjectURL.mockImplementationOnce(() => logo);
        fetch.mockImplementationOnce(() => Promise.resolve({
            status: 202,
            blob: () => Promise.resolve(logo)
        }));
        // shallow render Organisation Table with fake data
        const wrapper = shallow(<OrganisationTable backend={Backend} organisations={organisations}/>);
        wrapper.instance().setState({
            isDataFetched: true,
            organisationTags: organisationTags,
            projectLinks: projectLinks,
            categorieLinks: categorieLinks,
            interviewLinks: interviewLinks
        });
        // when amount of organisations are passed
        // then
        expect(wrapper.state('organisations').length).toBe(2);

        // when the amount of organisations increases by one after adding a organisation
        await wrapper.instance().onSave(organisationToAdd, logo);
        // then
        expect(wrapper.state('organisations').length).toBe(3);
        const table = wrapper.find('table');
        const tbody = table.find('tbody');

        // when tbody has the same amount of tr tags as data rows
        const rows = tbody.find('tr');
        // then
        expect(rows).toHaveLength(wrapper.state('organisations').length)

        // loops through each row and checks content
        rows.forEach((tr, rowIndex) => {
            const cells = tr.find('td');
            expect(cells.at(0).text()).toEqual(wrapper.state('organisations')[rowIndex].name);
            expect(cells.at(1).childAt(0).props().logo).toEqual(wrapper.state('organisations')[rowIndex].logo);
            expect(cells.at(2).text()).toEqual("Webpage " + wrapper.state('organisations')[rowIndex].name);
            expect(cells.at(3).text()).toEqual(wrapper.state('organisations')[rowIndex].description);

            // loops through projects and accesses the names
            let i, x = "";
            for (i in wrapper.state('organisations')[rowIndex].projects) {
                x += wrapper.state('organisations')[rowIndex].projects[i].name;
            }
            // then checks if the names are equal to the table cell content
            expect(cells.at(6).text()).toEqual(x);


            // loops through interviews and accesses the names
            let j, y = "";
            for (j in wrapper.state('organisations')[rowIndex].interviews) {
                y += wrapper.state('organisations')[rowIndex].interviews[j].name;
            }
            // then checks if the names are equal to the table cell content
            expect(cells.at(7).text()).toEqual(y);


            // loops through categories and accesses the names
            let k, z = "";
            for (k in wrapper.state('organisations')[rowIndex].categories) {
                z += wrapper.state('organisations')[rowIndex].categories[k].categoryName;
            }
            // then checks if the names are equal to the table cell content
            expect(cells.at(4).text()).toEqual(z);

            // loops through tags and accesses the text
            let l, a = "";
            for (l in wrapper.state('organisations')[rowIndex].tags) {
                a += wrapper.state('organisations')[rowIndex].tags[l].text;
            }
            // when the names are equal to the table cell content
            // then
            expect(cells.at(5).text()).toEqual(a);


            // loops through projects and accesses the names
            let m, b = "";
            for (m in wrapper.state('organisations')[rowIndex].projects) {
                b += wrapper.state('organisations')[rowIndex].projects[m].name;
            }
            // and checks if the names are equal to the table cell content
            expect(cells.at(6).text()).toEqual(b);


            // loops through interviews and accesses the names
            let n, c = "";
            for (n in wrapper.state('organisations')[rowIndex].interviews) {
                c += wrapper.state('organisations')[rowIndex].interviews[n].name;
            }
            // and checks if the names are equal to the table cell content
            expect(cells.at(7).text()).toEqual(c);
        })
    });

    test('if one organisation is deleted', async () => {
        //given
        fetch.mockImplementationOnce(() => Promise.resolve());
        window.confirm = jest.fn().mockImplementation(() => true)
        const wrapper = shallow(<OrganisationTable backend={Backend} organisations={organisations}/>);
        wrapper.instance().setState({
            isDataFetched: true,
            organisationTags: organisationTags,
            projectLinks: projectLinks,
            categorieLinks: categorieLinks,
            interviewLinks: interviewLinks
        });
        expect(wrapper.state('organisations').length).toBe(2);
        // when
        await wrapper.instance().onDelete('http://localhost:8080/api/organisations/9');
        // then
        expect(wrapper.state('organisations').length).toBe(1);
    })

    test("Organisation Table function onSave should update an existing organisation", async () => {
        // given
        const componentUnderTest = shallow(<OrganisationTable backend={Backend} organisations={organisations}/>);
        const logo = generateBlob();
        const logoUrl = "http://localhost:3000/my-wonderful-logo";
        const organisationToUpdate = {
            name: "Wohnprojekt Quelle e.V.",
            categories: [
                {
                    categoryName: "Soziales",
                    _links: {self: {href: "http://localhost:8080/api/categories/1337"}}
                }
            ],
            interviews: [{name: "Quergefragt5", _links: {self: {href: "http://localhost:8080/api/interviews/0815"}}}],
            projects: [{name: "Neues Projekt", _links: {self: {href: "http://localhost:8080/api/projects/44"}}}],
            tags: [{text: "#PeopleHelpingPeople", _links: {self: {href: "http://localhost:8080/api/tags/1000"}}}],
            website: "https://www.stadtreinigung-leipzig.de",
            description: "Ein Leipziger Verein, welcher betreutes Wohnen anbietet",
            _links: {self: {href: "http://localhost:8080/api/organisations/9"}}
        }

        fetch.mockImplementationOnce(() => Promise.resolve({
            status: 202,
            blob: () => Promise.resolve(logo)
        }));
        URL.createObjectURL.mockImplementationOnce(() => logoUrl);

        // when
        componentUnderTest.instance().setState({
            isDataFetched: true,
            inEdit: true,
            organisationTags: organisationTags,
            projectLinks: projectLinks,
            categorieLinks: categorieLinks,
            interviewLinks: interviewLinks
        });
        await componentUnderTest.instance().onSave(organisationToUpdate, logo);

        // then
        expect(componentUnderTest.state().organisations.length).toEqual(organisations.length);
        expect(componentUnderTest.state().organisations).toContainEqual(organisationToUpdate);
        expect(componentUnderTest.state().organisations.find(organisation => organisation._links.self.href === "http://localhost:8080/api/organisations/9").logo).toEqual(logoUrl);

        expect(componentUnderTest.state().interviewLinks.find(interviewLink => interviewLink.Id === "9").relatedObjects).toContainEqual(
            {
                name: "Quergefragt5",
                _links: {self: {href: "http://localhost:8080/api/interviews/0815"}}
            }
        );

        expect(componentUnderTest.state().projectLinks.find(projectLink => projectLink.Id === "9").relatedObjects).toContainEqual(
            {
                name: "Neues Projekt",
                _links: {self: {href: "http://localhost:8080/api/projects/44"}}
            }
        );

        expect(componentUnderTest.state().organisationTags.find(organisationTag => organisationTag.Id === "9").relatedObjects).toContainEqual(
            {
                text: "#PeopleHelpingPeople",
                _links: {self: {href: "http://localhost:8080/api/tags/1000"}}
            }
        );
    });
});
