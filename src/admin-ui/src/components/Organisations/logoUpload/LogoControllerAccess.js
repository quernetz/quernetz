import backendUrl from "../../../data-access/backendconfig";
import auth from "../../../data-access/Auth"

export default async function uploadLogo(id, file) {
    let image = null;
    const formData = new FormData();
    formData.append("file", file);

    await fetch(backendUrl() + '/api/organisations/' + id + '/logo', {
        method: 'PUT',
        headers: getHeaders(),
        body: formData,
    })
        .then(response => {
            if (response.status === 202) {
                return response.blob();
            }
        })
        .then((myBlob) => {
            image = URL.createObjectURL(myBlob)
        })
        .catch(error => {
        });
    return image;
}

function getHeaders() {
    let header = {};
    if (auth.isUserAuthenticated()) {
        header["Authorization"] = "Bearer " + sessionStorage.getItem("token");
    }
    return header;
}