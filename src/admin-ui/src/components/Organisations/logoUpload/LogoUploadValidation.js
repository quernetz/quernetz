export var getExtension = (file) => {
    var parts = file.name.split(".");
    return parts[parts.length - 1];
}

export var validateFormat = (file) => {
    if (file) {
        var ext = getExtension(file)
        switch (ext.toLowerCase()) {
            case 'jpg':
            case 'svg':
            case 'png':
                return file;
            default:
                return null;
        }
    } else {
        return null;
    }
}

export var validateSize = (file) => {
    if (file != null) {
        if (file.size < 500000) {
            return file;
        } else {
            return null;
        }
    } else {
        return null;
    }
}