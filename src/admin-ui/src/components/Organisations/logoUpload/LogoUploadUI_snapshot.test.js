import renderer from "react-test-renderer";
import React from "react";
import LogoUploadUI from "./LogoUploadUI";

test("snapshot", () =>{
    const tree = renderer
        .create(<LogoUploadUI/>)
        .toJSON();
    expect(tree).toMatchSnapshot();
});
