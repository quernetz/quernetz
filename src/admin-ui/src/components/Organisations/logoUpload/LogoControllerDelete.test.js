import deleteLogo from "./LogoControllerDelete";
import React from 'react'

let fakeData = [];
while (fakeData.length < 499999) {
    fakeData.push("1");
};

let testFile = new Blob(fakeData);
testFile.name = "someFile.jpg";

global.fetch = jest.fn(() =>
    Promise.resolve({
        json: () => Promise.resolve({rates: {logo: testFile}}),
    })
)

const organisation = {
    name: 'Stadtreinigung',
    logo: testFile,
    website: 'https://www.stadtreinigung-leipzig.de',
    description: 'Die Stadtreinigung der Stadt Leipzig',
    _links: {self: {href: "http://localhost:8080/api/organisations/9"}}
}


beforeEach(() => {
    fetch.mockClear();
});

it("test", async () => {
    const file =  await  deleteLogo(organisation);
    expect(file).toEqual();
    expect(fetch).toHaveBeenCalledTimes(1);
});