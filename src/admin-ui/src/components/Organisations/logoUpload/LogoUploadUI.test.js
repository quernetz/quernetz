import React from 'react'
import LogoUploadUI from "./LogoUploadUI";
import Enzyme, {shallow} from "enzyme";

const {validateFormat} = require("./LogoUploadValidation");
const {validateSize} = require("./LogoUploadValidation");
const {img_swapper} = require("./LogoUploadUI");
const {cancel} = require("./LogoUploadUI");

test("should return 1 if the file is a jpg/svg/png and < 500kb, else 0", () =>{

    let fakeData = [];
    while (fakeData.length < 499999){
        fakeData.push("1");
    };

    var testFile2 = new Blob(fakeData);
    testFile2.name = "someFile.ext";

    var testFile4 = new Blob(fakeData);
    testFile4.name = "someFile.jpg";

    fakeData.push("1");

    var testFile1 = new Blob(fakeData);
    testFile1.name = "someFile.ext";

    var testFile3 = new Blob(fakeData);
    testFile3.name = "someFile.jpg";

    var check;
    if(validateSize(validateFormat(testFile1))){
        check = 11;
    }else{
        check = 10;
    }
    expect(check).toBe(10);

    if(validateSize(validateFormat(testFile2))){
        check = 21;
    }else{
        check = 20;
    }
    expect(check).toBe(20);

    if(validateSize(validateFormat(testFile3))){
        check = 31;
    }else{
        check = 30;
    }
    expect(check).toBe(30);

    if(validateSize(validateFormat(testFile4))){
        check = 41;
    }else{
        check = 40;
    }
    expect(check).toBe(41);
});

//TODO: DOM Tests with Enzyme
test("should be >>Datei auswählen!<<",()=>{
    const wrapper = shallow(<LogoUploadUI/>);
    const text = wrapper.find("div div label");
    expect(text.text()).toBe("Datei auswählen!");
});