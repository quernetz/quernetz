import LogoControllerAccess from "./LogoControllerAccess";
import React from 'react'
import uploadLogo from "./LogoControllerAccess";

let fakeData = [];
while (fakeData.length < 499999) {
    fakeData.push("1");
};

let testFile = new Blob(fakeData);
testFile.name = "someFile.jpg";

global.fetch = jest.fn(() =>
    Promise.resolve({
        json: () => Promise.resolve({rates: {logo: testFile}}),
    })
)

beforeEach(() => {
    fetch.mockClear();
});

it("test", async () => {
    const file =  await  uploadLogo(9, testFile);
    expect(file).toEqual(null);
    expect(fetch).toHaveBeenCalledTimes(1);
});