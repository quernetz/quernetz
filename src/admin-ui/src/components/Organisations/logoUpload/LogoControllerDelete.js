import backendUrl from "../../../data-access/backendconfig";
import auth from "../../../data-access/Auth";

export default async function deleteLogo(id) {
    await fetch(backendUrl() + '/api/organisations/' + id + '/logo', {
        method: 'DELETE',
        headers: getHeaders(),
    })
}


function getHeaders() {
    let header = {};
    if (auth.isUserAuthenticated()) {
        header["Authorization"] = "Bearer " + sessionStorage.getItem("token");
    }
    return header;
}