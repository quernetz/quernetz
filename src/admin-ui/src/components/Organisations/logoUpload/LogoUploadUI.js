import React from "react";
import "./LogoUploadUI.css";
import {validateFormat, validateSize} from "./LogoUploadValidation";
import DeleteButton from "../../Buttons/DeleteButton";

class LogoUploadUI extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showWarning: false,
            previewDefaultText: null,
            organisationLogo: this.props.organisationLogo,
            deleteLogo: this.props.deleteLogo
        };
    }

    componentDidUpdate () {
        if (this.props.create) {
            const logo = this.props.organisationLogo;
            if (logo !== null && logo !== undefined) {
                try {
                    this.inEdit();
                } catch (e) {
                    this.resetImagePreview();
                }
            } else if (this.props.deleteLogo) {
                this.resetImagePreview();
                this.props.deleteLogoCallback();
                this.setState({deleteLogo: this.props.deleteLogo})
            }
        } else if (this.props.deleteLogo) {
            this.resetImagePreview();
            this.props.deleteLogoCallback();
            this.setState({deleteLogo: this.props.deleteLogo})
        }
    }

    onImgInputClick = (event) => {
        event.target.value = null;
    }

    img_swapper = () => {
        const previewContainer = document.getElementById("imagePreview");
        const previewImage = previewContainer.querySelector(".imagePreviewImage");
        const previewDefaultText = previewContainer.querySelector(".image-preview__default-text");
        this.setState({previewDefaultText: previewContainer.querySelector(".image-preview__default-text")});

        const file = document.getElementById('inpFile').files[0];
        this.props.callbackFromParent(file);
        if (validateSize(validateFormat(file))) {
            this.setState({showWarning: false});
            const reader = new FileReader();
            previewDefaultText.style.display = "none";
            previewImage.style.display = "block";

                reader.addEventListener("load", function () {
                    previewImage.setAttribute("src", this.result);
                });
                reader.readAsDataURL(file);

                document.getElementById('custom-file-label').innerText = file.name;
            } else {
                this.setState({showWarning: true});
            }
        }

    inEdit = () => {
                const previewContainer = document.getElementById("imagePreview");
                const previewImage = previewContainer.querySelector(".imagePreviewImage");
                const previewDefaultText = previewContainer.querySelector(".image-preview__default-text");

                const file = this.props.organisationLogo;
                previewDefaultText.style.display = "none";
                previewImage.style.display = "block";
                previewImage.setAttribute("src", file);
                document.getElementById('custom-file-label').innerText = "Logo aus Datenbank";

    }

    resetImagePreview = () =>{
        try{
            const previewContainer = document.getElementById("imagePreview");
            const previewImage = previewContainer.querySelector(".imagePreviewImage");
            const previewDefaultText = previewContainer.querySelector(".image-preview__default-text");

            previewImage.setAttribute("src", "");
            previewDefaultText.style.display = "";
            previewImage.style.display = "none";
            document.getElementById('custom-file-label').innerText = "Datei auswählen!";
        } catch (e) {
        }

    }

    cancel = () => {
        this.props.callbackFromParent(null);
        this.resetImagePreview();
        this.props.createFalseCallback();
        this.setState({deleteLogo: this.props.create})
    }

    render() {
        return (
            <div>
                <div className="custom-file mb-3">
                    <input type="file" className="custom-file-input" onClick={this.onImgInputClick} onChange={this.img_swapper} id="inpFile"
                           name="inpFile"/>
                    <label className="custom-file-label" id="custom-file-label" htmlFor="inpFile">Datei auswählen!</label>
                </div>
                <div className="warning">
                    {
                        this.state.showWarning ?
                            <div>
                                Nur .jpg .png .svg Dateien bis 500KB erlaubt!
                            </div>
                            : null
                    }
                </div>
                <div id="parent">
                    <div className="imagePreview" id="imagePreview">
                        <img src="" alt="ImagePreview" className="imagePreviewImage"/>
                        <span className="image-preview__default-text">Image Preview</span>
                    </div>
                    <div className="delButton">
                        <DeleteButton onClick={this.cancel} />
                    </div>
                </div>
            </div>
        );
    }
}
export default LogoUploadUI;