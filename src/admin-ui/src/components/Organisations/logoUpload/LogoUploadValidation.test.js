const {getExtension} = require("./LogoUploadValidation");
const {validateFormat} = require("./LogoUploadValidation");
const {validateSize} = require("./LogoUploadValidation");

test("should return the extention/ type of a file", () => {
    var file = new Blob();
    file.name = "someFile.ext";
    const text = getExtension(file);
    expect(text).toBe("ext");
});

test("should return null if the file is a jpg/svg/png, else return null", () => {
    var testFile = new Blob();
    testFile.name = "someFile.ext";
    const file = validateFormat(testFile);
    expect(file).toBe(null);

    testFile.name = "someFile.jpg";
    const file1 = validateFormat(testFile);
    expect(file1).toBe(testFile);

    testFile.name = "someFile.svg";
    const file2 = validateFormat(testFile);
    expect(file2).toBe(testFile);

    testFile.name = "someFile.png";
    const file3 = validateFormat(testFile);
    expect(file3).toBe(testFile);
});

test("should return null if the file size is >= 500kb or if it gets null as input, else it returns the file", () => {

    const file1 = validateSize(null);
    expect(file1).toBe(null);

    let fakeData = [];
    while (fakeData.length < 499999) {
        fakeData.push("1");
    }
    ;

    var testFile = new Blob(fakeData);
    testFile.name = "someFile.ext";
    const file2 = validateSize(testFile);
    expect(file2).toBe(testFile);

    fakeData.push("1");
    var testFile = new Blob(fakeData);
    testFile.name = "someFile.ext";
    const file3 = validateSize(testFile);
    expect(file3).toBe(null);
});