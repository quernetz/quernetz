import React from "react";
import "./OrganisationLogoCSS.css"

class OrganisationLogo extends React.Component {
    render() {
        return (
            <div>
                <div>
                    <img src={this.props.logo} alt="kein logo" style={{'maxWidth': '100px', 'height': 'auto'}}/>
                </div>
            </div>
        );
    }
}

export default OrganisationLogo;