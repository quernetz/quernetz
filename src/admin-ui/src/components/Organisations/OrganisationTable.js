import React from "react";
import OrganisationForm from "./OrganisationForm";
import ModalError from "../misc/ModalError";
import '../Styles/TableStyle.css';
import '../Styles/ButtonStyle.css';
import OrganisationLogo from "./OrganisationLogo";
import $ from "jquery";
import ModifyButton from "../Buttons/ModifyButton";
import DeleteButton from "../Buttons/DeleteButton";
import uploadLogo from "./logoUpload/LogoControllerAccess";
import deleteLogo from "./logoUpload/LogoControllerDelete";
import {extractIdFromSelf} from "../../utils/extractIdFromSelf";
import {getConnectedObjects, getLinks, updateLinks} from "../../utils/helperConnections";

window.jQuery = $;
window.$ = $;
global.jQuery = $;

const REST_API_ERROR_MODAL_ID = "OrganisationBackendErrorModal";

export default class OrganisationTable extends React.Component {
    state = {
        organisations: this.props.organisations,
        inEdit: false,
        organisationInEdit: [],
        backendErrorMessage: "",
        serverConnectionError: false,
        logo: this.props.organisations.logo,
        logoInEdit: null,
        isDataFetched: false,
        create: true,
        deleteLogo: true
    };

    createFalseCallbackMain = () => {
        this.setState({create: false})
    };

    deleteLogoCallbackMain = () => {
        this.setState({deleteLogo: false})
    };

    componentDidMount = async () => {
        const organisationTags = await getLinks(this.props.organisations, "tags");
        const projectLinks = await getLinks(this.props.organisations, "projects");
        const categorieLinks = await getLinks(this.props.organisations, "categories");
        const interviewLinks = await getLinks(this.props.organisations, "interviews");
        this.setState({
            isDataFetched: true,
            organisationTags: organisationTags,
            categorieLinks: categorieLinks,
            projectLinks: projectLinks,
            interviewLinks: interviewLinks
        });
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevState.organisations.length === 0 && this.props.organisations.length > 0) {
            this.setState({organisations: this.props.organisations});
        }
    }

    createCallback = () => {
        this.setState({create: true})
    };

    onEdit = index => {
        this.setState({inEdit: true});
        this.setState({organisationInEdit: index});
    };

    onAdd = () => {
        document.getElementById("organisation-form").reset();
        this.setState({
            inEdit: false,
            organisationInEdit: null,
        });
    };

    onClosingBackendError = () => {
        this.setState({backendErrorMessage: ""});
    };

    onSave = async (organisation, logo) => {
        if (this.state.inEdit) {
            let result = await this.props.backend.patch("organisation", organisation);
            if (result.ok || result.withWarnings) {
                let savedOrga = result.ok ? result.data : result.data.data;
                const Id = savedOrga._links.self.href.substring(savedOrga._links.self.href.lastIndexOf("/") + 1);
                let logoFromDb = null;
                if (logo !== null) {
                    logoFromDb = await uploadLogo(Id, logo);
                } else {
                    await deleteLogo(Id);
                }
                savedOrga.logo = logoFromDb;

                this.hideModal("#OrganisationModal");

                this.setState({
                    organisations: [...this.state.organisations].map(organisationFromTable =>
                        organisationFromTable._links.self.href === savedOrga._links.self.href
                            ? savedOrga
                            : organisationFromTable
                    )
                });

                if (!result.withWarnings) {
                    this.setState({
                        categorieLinks: updateLinks(organisation.categories, this.state.categorieLinks, savedOrga),
                        organisationTags: updateLinks(organisation.tags, this.state.organisationTags, savedOrga),
                        projectLinks: updateLinks(organisation.projects, this.state.projectLinks, savedOrga),
                        interviewLinks: updateLinks(organisation.interviews, this.state.interviewLinks, savedOrga)
                    })
                }
                if (result.withWarnings) {
                    this.setState({backendErrorMessage: `Achtung:${result.data.warning}`});
                    this.showModal(`#${REST_API_ERROR_MODAL_ID}`);
                }
            } else if (result.integrityViolation) {
                this.hideModal("#OrganisationModal");
                this.setState({backendErrorMessage: result.data.reason});
                this.showModal(`#${REST_API_ERROR_MODAL_ID}`);
            }
        } else {
            let result = await this.props.backend.post("organisation", organisation);
            if (result.ok) {
                const savedOrganisation = result.data;

                const Id = savedOrganisation._links.self.href.substring(savedOrganisation._links.self.href.lastIndexOf("/") + 1);
                let logoFromDb = null;
                if (logo !== null) {
                    logoFromDb = await uploadLogo(Id, logo);
                } else {
                    await deleteLogo(Id);
                }
                savedOrganisation.logo = logoFromDb;
                this.setState({
                    organisations: this.state.organisations.concat(savedOrganisation),
                    categorieLinks: updateLinks(organisation.categories, this.state.categorieLinks, savedOrganisation, true),
                    organisationTags: updateLinks(organisation.tags, this.state.organisationTags, savedOrganisation, true),
                    projectLinks: updateLinks(organisation.projects, this.state.projectLinks, savedOrganisation, true),
                    interviewLinks: updateLinks(organisation.interviews, this.state.interviewLinks, savedOrganisation, true)
                });
                this.hideModal("#OrganisationModal");
            } else if (result.serverValidationError) {
                //handle serverside validation error
            } else if (result.internalError) {
                this.setState({serverConnectionError: true});
            } else if (result.notFound) {
                //handle notFound error
            } else if (result.unknown) {
                //handle other errors
            } else {
                console.error("Undefined ApiResult Error!");
            }
        }
    };

    showModal = modalId => {
        $(modalId).modal("show");
    };

    hideModal = modalId => {
        $(modalId).modal("hide");
    };

    onDelete = async uri => {
        if (window.confirm("Soll die Organisation wirklich gelöscht werden?")) {
            await deleteLogo(extractIdFromSelf(uri));
            let result = await this.props.backend.delete(uri);
            if (result.ok) {
                const newList = this.state.organisations.filter(organisation => organisation._links.self.href !== uri);
                this.setState({organisations: newList});
            } else if (result.integrityViolation) {
                this.setState({backendErrorMessage: result.data.reason});
                this.showModal(`#${REST_API_ERROR_MODAL_ID}`);
            } else {
                this.setState({backendErrorMessage: "Unbekannter Fehler beim Löschen der Organisation!"});
                this.showModal(`#${REST_API_ERROR_MODAL_ID}`);
            }
        }
    };

    render() {
        return (
            this.state.isDataFetched &&
            <div className="table-component">
                <h1 className="float-left">Organisationen:</h1>
                <button type="button" className="btn btn-quernetzer float-right" data-toggle="modal"
                        data-target="#OrganisationModal"
                        onClick={() => {
                            this.onAdd();
                            this.setState({
                                logoInEdit: null,
                                create: false,
                                deleteLogo: true
                            })
                        }}>
                    Organisation erstellen
                </button>
                <div className="table-responsive">
                    <table className="table table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Logo</th>
                            <th>Link</th>
                            <th>Beschreibung</th>
                            <th>Kategorien</th>
                            <th>Tags</th>
                            <th>Verknüpfte Projekte</th>
                            <th>Verknüpfte Interviews</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.organisations.map((organisation, index) => (
                            <tr key={index}>
                                <td>{organisation.name ? organisation.name : null}</td>
                                <td>{organisation.logo ? <OrganisationLogo logo={organisation.logo}/> : null}</td>
                                <td>{organisation.website ?
                                    <a style={{color: "#00987a"}}
                                       href={organisation.website}>Webpage {organisation.name}</a>
                                    : null}
                                </td>
                                <td>{organisation.description ? organisation.description : null}</td>
                                <td>
                                    <ul>{getConnectedObjects(organisation, this.state.categorieLinks).map((cat, index) =>
                                        <li key={index}>{cat.categoryName}</li>)}
                                    </ul>
                                </td>
                                <td>
                                    <ul>{getConnectedObjects(organisation, this.state.organisationTags).map((tag, index) =>
                                        <li key={index}>{tag.text}</li>)}
                                    </ul>
                                </td>
                                <td>
                                    <ul>{getConnectedObjects(organisation, this.state.projectLinks).map((project, index) =>
                                        <li key={index}>{project.name}</li>)}
                                    </ul>
                                </td>
                                <td>
                                    <ul>{getConnectedObjects(organisation, this.state.interviewLinks).map((interview, index) =>
                                        <li key={index}>{interview.name}</li>)}
                                    </ul>
                                </td>
                                <td>
                                    <div data-toggle="modal" onClick={() => {
                                        this.onEdit(index);
                                        this.setState({logoInEdit: null})
                                        this.setState({logoInEdit: organisation.logo})
                                        if (this.state.logoInEdit === null || this.state.logoInEdit === undefined) {
                                            this.setState({deleteLogo: true})
                                        }

                                    }} data-target="#OrganisationModal">
                                        <ModifyButton/>
                                    </div>
                                </td>
                                <td>
                                    <div onClick={() => this.onDelete(organisation._links.self.href)}>
                                        <DeleteButton/>
                                    </div>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
                <OrganisationForm onSave={this.onSave} inEdit={this.state.inEdit}
                                  connectedTags={this.state.inEdit ? getConnectedObjects(this.state.organisations[this.state.organisationInEdit], this.state.organisationTags) : []}
                                  connectedCategories={this.state.inEdit ? getConnectedObjects(this.state.organisations[this.state.organisationInEdit], this.state.categorieLinks) : []}
                                  connectedProjects={this.state.inEdit ? getConnectedObjects(this.state.organisations[this.state.organisationInEdit], this.state.projectLinks) : []}
                                  connectedInterviews={this.state.inEdit ? getConnectedObjects(this.state.organisations[this.state.organisationInEdit], this.state.interviewLinks) : []}

                                  serverConnectionError={this.state.serverConnectionError}
                                  organisationInEdit={this.state.organisations[this.state.organisationInEdit]}
                                  organisationLogo={this.state.logoInEdit}
                                  create={this.state.create}
                                  createCallback={this.createCallback}
                                  deleteLogo={this.state.deleteLogo}
                                  deleteLogoCallbackMain={this.deleteLogoCallbackMain}
                                  createFalseCallbackMain={this.createFalseCallbackMain}
                />
                <ModalError modalId={REST_API_ERROR_MODAL_ID} errorTitle="Fehler beim Löschen der Organisation!"
                            errorMessage={this.state.backendErrorMessage} onClosing={this.onClosingBackendError}/>
            </div>
        );
    }
}