import {shallow} from "enzyme";
import ProjectTable from "./ProjectTable";
import Backend from "../../data-access/__mocks__/Backend";
import React from "react";

const projects = [
    {
        description: 'Das war unser erstes Projekt',
        name: 'Erstes Projekt',
        from: '2019-01-01',
        to: '2019-01-02',
        imageUrls: [ "https://www.duckduckgo.com" ],
        _links: {self: {href: "http://localhost:8080/api/projects/19"}}
    },
    {
        description: 'Das war unser neues Projekt',
        name: 'Neues Projekt',
        from: '2018-03-04',
        to: '2020-02-01',
        imageUrls: [ "https://www.duckduckgo.com" ],
        _links: {self: {href: "http://localhost:8080/api/projects/20"}}
    }
];

const organisations = [
    {
        id: 5,
        name: 'Erste Organisation',
        description: 'Das ist eine Organisation',
        _links: {self: {href: "http://localhost:8080/api/organisations/5"}}
    }
];

const categories = [
    {
        categoryName: 'Müll',
        _links: {self: {href: "http://localhost:8080/api/categories/4"}}
    },
    {
        categoryName: 'Umwelt',
        _links: {self: {href: "http://localhost:8080/api/categories/5"}}
    }
];

const tags = [
    {
        text: '#unitedWeStrand',
        _links: {self: {href: "http://localhost:8080/api/tags/1"}}
    }
];

const organisationLinks = [
    {
        Id: 9,
        relatedObjects: []
    },
];

const projectTags = [
    {
        Id: 1,
        relatedObjects: []
    },
];

const categorieLinks = [
    {
        Id: 4,
        relatedObjects: []
    },
    {
        Id: 5,
        relatedObjects: []
    },
];

test('snapshot', () => {
    const tree = shallow(<ProjectTable backend={Backend} projects={projects} organisations={organisations}
                                       categories={categories} tags={tags}/>);
    tree.instance().setState({isDataFetched: true, organisationLinks: organisationLinks, projectTags: projectTags, categorieLinks: categorieLinks});
    expect(tree).toMatchSnapshot();
})