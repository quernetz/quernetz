import React from 'react'
import ReactDOM from 'react-dom'
import ProjectTable from "./ProjectTable";
import {shallow} from 'enzyme';
import Backend from "../../data-access/__mocks__/Backend";


const projects = [
    {
        description: 'Das war unser erstes Projekt',
        categories: [
            {
                categoryName: 'Müll',
                _links: {self: {href: "http://localhost:8080/api/categories/4"}}
            },
            {
                categoryName: 'Umwelt',
                _links: {self: {href: "http://localhost:8080/api/categories/5"}}
            }
        ],
        tags: [
            {
                text: '#unitedWeStrand',
                _links: {self: {href: "http://localhost:8080/api/tags/1"}}
            }
        ],
        organisations: [
            {
                name: 'Erste Organisation',
                _links: {self: {href: "http://localhost:8080/api/organisations/9"}}
            },
            {
                name: 'Zweite Organisation',
                _links: {self: {href: "http://localhost:8080/api/organisations/10"}}
            }
        ],
        year: "2090",
        name: 'Erstes Projekt',
        from: '2019-01-01',
        to: '2019-01-02',
        projectLink: "https://www.duckduckgo.com",
        imageUrl: "https://m.media-amazon.com/images/I/51AuCnI0-4L._SS500_.jpg",
        _links: {self: {href: "http://localhost:8080/api/projects/19"}}
    },
    {
        description: 'Das war unser neues Projekt',
        categories: [
            {
                categoryName: 'Umwelt',
                _links: {self: {href: "http://localhost:8080/api/categories/5"}}
            }
        ],
        tags: [
            {
                text: '#unitedWeStrand',
                _links: {self: {href: "http://localhost:8080/api/tags/1"}}
            }
        ],
        organisations: [
            {
                name: 'Zweite Organisation',
                _links: {self: {href: "http://localhost:8080/api/organisations/10"}}
            }
        ],
        year: "2090",
        name: 'Neues Projekt',
        from: '2018-03-04',
        to: '2020-02-01',
        projectLink: "https://www.wikipedia.de",
        imageUrl: "https://m.media-amazon.com/images/I/51AuCnI0-4L._SS500_.jpg",
        _links: {self: {href: "http://localhost:8080/api/projects/20"}}
    }
];

const organisationLinks = [
    {
        Id: 19,
        relatedObjects: [9, 10]
    },
    {
        Id: 20,
        relatedObjects: [10]
    }
];

const projectTags = [
    {
        Id: 19,
        relatedObjects: [1]
    },
    {
        Id: 20,
        relatedObjects: [1]
    },
];

const categorieLinks = [
    {
        Id: 19,
        relatedObjects: [4, 5]
    },
    {
        Id: 20,
        relatedObjects: [5]
    },
];

describe('tests project table', () => {

    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<ProjectTable backend={Backend} projects={projects}/>, div)
    });

    test('Project Table function onSave should update an existing project', async () => {
        // given
        const componentUnderTest = shallow(<ProjectTable backend={Backend} projects={projects}/>);
        // when
        componentUnderTest.instance().setState({
            isDataFetched: true,
            inEdit: true,
            organisationLinks: organisationLinks,
            projectTags: projectTags,
            categorieLinks: categorieLinks
        });
        // then
        expect(componentUnderTest.instance().onEdit()).toEqual(componentUnderTest.state.inEdit);

        // given
        const projectToUpdate = {
            description: "neue Beschreibung",
            name: "neuer Name",
            from: "2018, 01, 02",
            to: "2019, 03, 04",
            year: "2018",
            categories: [{name: "Umwelt", _links: {self: {href: "http://localhost:8080/api/categories/5"}}}],
            projectLink: "https://www.neuerlink.com",
            tags: [{text: '#unitedWeStrand', _links: {self: {href: "http://localhost:8080/api/tags/1"}}}],
            imageUrl: ["https.//wikipedia.com"],
            organisations: [{
                name: "Erste Organisation",
                _links: {self: {href: "http://localhost:8080/api/organisations/5"}}
            }],
            _links: {self: {href: "http://localhost:8080/api/projects/19"}}
        }
        // when
        await componentUnderTest.instance().onSave(projectToUpdate);
        // then
        expect(componentUnderTest.state().projects).toContainEqual(projectToUpdate)
        // when the current project was edited and no new project was added
        // then
        expect(componentUnderTest.state('projects').length).toBe(2);
        // when the connection data structure got the new entity's
        // then
        expect(componentUnderTest.state().organisationLinks.find(organisationLink => organisationLink.Id === "19").relatedObjects).toContainEqual(
            {
                name: "Erste Organisation",
                _links: {self: {href: "http://localhost:8080/api/organisations/5"}}
            }
        )


        expect(componentUnderTest.state().categorieLinks.find(categorieLink => categorieLink.Id === "19").relatedObjects).toContainEqual(
            {
                name: "Umwelt",
                _links: {self: {href: "http://localhost:8080/api/categories/5"}}
            }
        );


        expect(componentUnderTest.state().projectTags.find(projectTag => projectTag.Id === "19").relatedObjects).toContainEqual(
            {
                text: '#unitedWeStrand',
                _links: {self: {href: "http://localhost:8080/api/tags/1"}}
            }
        );
    })
});
