import React from "react";
import ProjectForm from "./ProjectForm";
import ModalError from "../misc/ModalError";
import '../Styles/TableStyle.css';
import '../Styles/ButtonStyle.css';
import $ from "jquery";
import ModifyButton from "../Buttons/ModifyButton";
import DeleteButton from "../Buttons/DeleteButton";
import {getConnectedObjects, getLinks, updateLinks} from "../../utils/helperConnections";
import {convertToGermanDateString} from "../../utils/dateFormatting";


window.jQuery = $;
window.$ = $;
global.jQuery = $;

export default class ProjectTable extends React.Component {
    state = {
        projects: this.props.projects,
        validationErrors: [],
        serverConnectionError: false,
        inEdit: false,
        modalErrorTitle: "",
        modalErrorMessage: "",
        isDataFetched: false,
    };

    componentDidMount = async () => {
        const projectTags = await getLinks(this.props.projects, "tags");
        const categorieLinks = await getLinks(this.props.projects, "categories");
        const organisationLinks = await getLinks(this.props.projects, "organisations");
        this.setState({
            organisationLinks: organisationLinks,
            isDataFetched: true,
            projectTags: projectTags,
            categorieLinks: categorieLinks
        });
    };

    /**
     * @param {String} modalId example : "#myModal"
     */
    hideModal = modalId => {
        $(modalId).modal("hide");
    };

    showModal = modalId => {
        $(modalId).modal("show");
    };

    clearModalErrors = () => {
        this.setState({modalErrors: []});
    };

    onAdd = () => {
        document.getElementById("input-form").reset();
        this.setState({
            inEdit: false,
            projectInEdit: null,
            serverConnectionError: false,
        });
    };

    onSave = async newProject => {
        if (this.state.inEdit) {
            let result = await this.props.backend.patch("project", newProject);
            if (result.ok) {
                let project = result.data;
                this.setState({
                    projects: [...this.state.projects].map(projectFromTable => projectFromTable._links.self.href === newProject._links.self.href ? newProject : projectFromTable),
                    categorieLinks: updateLinks(newProject.categories, this.state.categorieLinks, project),
                    projectTags: updateLinks(newProject.tags, this.state.projectTags, project),
                    organisationsLinks: updateLinks(newProject.organisations, this.state.organisationLinks, project)

                });
                this.hideModal("#addProjectModal");
            } else if (result.serverValidationError) {
                //handle serverside validation error
            } else if (result.unknown) {
                console.error("Unknown error");
            } else {
                this.setState({serverConnectionError: true});
            }
        } else {
            let result = await this.props.backend.post("project", newProject);
            if (result.ok) {
                let project = result.data;
                this.setState({
                    projects: this.state.projects.concat(project),
                    categorieLinks: updateLinks(newProject.categories, this.state.categorieLinks, project, true),
                    projectTags: updateLinks(newProject.tags, this.state.projectTags, project, true),
                    organisationsLinks: updateLinks(newProject.organisations, this.state.organisationLinks, project, true)
                });
                this.hideModal("#addProjectModal");
            } else if (result.serverValidationError) {
                //handle serverside validation error
            } else if (result.unknown) {
                console.error("Unknown error");
            } else {
                this.setState({serverConnectionError: true});
            }
        }
    };

    onEdit = index => {
        this.setState({
            inEdit: true,
            projectInEdit: index
        });
    };

    onDelete = async uri => {
        if (window.confirm("Soll das Projekt wirklich gelöscht werden?")) {
            const result = await this.props.backend.delete(uri);
            if (result.ok) {
                const newList = this.state.projects.filter(p => p._links.self.href !== uri);
                this.setState({projects: newList});
            }
        }
    };

    render() {
        return (
            this.state.isDataFetched &&
            <div className="table-component">
                <h1 className="float-left">Projekte:</h1>
                <button type="button" className="btn btn-quernetzer float-right" data-toggle="modal"
                        data-target="#addProjectModal" onClick={this.onAdd}>
                    Projekt erstellen
                </button>
                <div className="table-responsive">
                    <table className="table table-hover">
                        <thead>
                        <tr>
                            <th>Über das Projekt</th>
                            <th>Projekttitel</th>
                            <th>Beginn</th>
                            <th>Ende</th>
                            <th>Kategorie</th>
                            <th>Link</th>
                            <th>Tags</th>
                            <th>Bilder</th>
                            <th>Beteiligte Organisationen</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.projects.map((project, index) => (
                            <tr key={index}>
                                <td>{project.description ? project.description : null}</td>
                                <td>{project.name ? project.name : null}</td>
                                <td>{convertToGermanDateString(project.from)}</td>
                                <td>{convertToGermanDateString(project.to)}</td>
                                <td>
                                    <ul>{getConnectedObjects(project, this.state.categorieLinks).map((cat, index) =>
                                        <li key={index}>{cat.categoryName}</li>)}
                                    </ul>
                                </td>
                                <td>{project.projectLink ? <a style={{color: "#00987a"}} href={project.projectLink}>Weitere
                                    Infos</a> : null}</td>
                                <td>
                                    <ul>{getConnectedObjects(project, this.state.projectTags).map((tag, index) =>
                                        <li key={index}>{tag.text}</li>)}
                                    </ul>
                                </td>
                                <td>{project.imageUrl !== "" ?
                                    <a style={{color: "#00987a"}} href={project.imageUrl} target="_blank"
                                       rel="noopener noreferrer">Bild-Link</a> : null}</td>
                                <td>
                                    <ul>{getConnectedObjects(project, this.state.organisationLinks).map((organisation, index) =>
                                        <li key={index}>{organisation.name}</li>)}
                                    </ul>
                                </td>
                                <td>
                                    <div data-toggle="modal" onClick={() => this.onEdit(index)}
                                         data-target="#addProjectModal">
                                        <ModifyButton/>
                                    </div>
                                </td>
                                <td>
                                    <div onClick={() => this.onDelete(project._links.self.href)}>
                                        <DeleteButton/>
                                    </div>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                    <ProjectForm serverConnectionError={this.state.serverConnectionError} onSave={this.onSave}
                                 inEdit={this.state.inEdit}
                                 connectedTags={this.state.inEdit ? getConnectedObjects(this.state.projects[this.state.projectInEdit], this.state.projectTags) : []}
                                 connectedOrganisation={this.state.inEdit ? getConnectedObjects(this.state.projects[this.state.projectInEdit], this.state.organisationLinks) : []}
                                 connectedCategories={this.state.inEdit ? getConnectedObjects(this.state.projects[this.state.projectInEdit], this.state.categorieLinks) : []}
                                 projectInEdit={this.state.projects[this.state.projectInEdit]}/>
                    <ModalError onClosing={this.clearModalErrors} modalId="projectTableErrorModal"
                                errorTitle={this.state.modalErrorTitle} errorMessage={this.state.modalErrorMessage}/>
                </div>
            </div>
        );
    }
}
