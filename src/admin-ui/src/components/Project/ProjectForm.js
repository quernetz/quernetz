import React from "react";
import "bootstrap";
import fetchData from "../../data-access/fetchData";
import BackendUrl from "../../data-access/backendconfig.js";
import SelectOrganisation from "../Links/SelectOrganisation";
import SelectCategories from "../Links/SelectCategories";
import SelectTags from "../Links/SelectTags";
import SelectDate from "../Links/SelectDate";
import {isEmpty, isURL} from "validator";
import {format} from "date-fns";
import {convertToDate} from "../../utils/dateFormatting";

export default class ProjectForm extends React.Component {
    constructor(props) {
        super(props);
        const projectInEdit = (typeof this.props.projectInEdit === 'undefined') ? false : this.props.projectInEdit;
        this.state = {
            categories: [],
            inEdit: this.props.inEdit,
            validationErrors: [],
            selectedOrganisations: [],
            selectedCategories: [],
            selectedTags: [],
            selectedStartDate: undefined,
            selectedEndDate: undefined,
            projectInEdit: projectInEdit
        }
    }

    componentDidMount() {
        fetchData(BackendUrl() + "/api/categories").then((data) => {
            this.setState({categories: data._embedded.categories});
        });
    }

    componentDidUpdate(prevProps) {
        if (this.props.inEdit !== prevProps.inEdit) {
            this.clearForm();
            this.setState({inEdit: this.props.inEdit});
        }
        if (typeof this.props.projectInEdit !== 'undefined' && this.props.projectInEdit !== prevProps.projectInEdit) {
            this.descInput.value = this.props.projectInEdit.description;
            this.nameInput.value = this.props.projectInEdit.name;
            this.linkInput.value = this.props.projectInEdit.projectLink;
            this.picInput.value = this.props.projectInEdit.imageUrl;
            this.setState({
                selectedStartDate: this.props.projectInEdit.from,
                selectedEndDate: this.props.projectInEdit.to,
                selectedCategories: this.props.projectInEdit.selectedCategories,
                selectedTags: this.props.projectInEdit.selectedTags,
                selectedOrganisations: this.props.projectInEdit.selectedOrganisations,
                projectInEdit: this.props.projectInEdit,
            })
        }
        if (this.props.connectedOrganisation !== prevProps.connectedOrganisation) {
            this.setState({selectedOrganisations: this.props.connectedOrganisation})
        }
        if (this.props.connectedTags !== prevProps.connectedTags) {
            this.setState({selectedTags: this.props.connectedTags})
        }
        if (this.props.connectedCategories !== prevProps.connectedCategories) {
            this.setState({selectedCategories: this.props.connectedCategories})
        }
    }

    onSubmit = (event) => {
        event.preventDefault();
        let isValid = this.validateInputs();
        if (isValid) {
            let newProject = {
                description: this.descInput.value,
                name: this.nameInput.value,
                from: this.state.selectedStartDate,
                to: this.state.selectedEndDate,
                categories: this.state.selectedCategories,
                projectLink: this.linkInput.value,
                tags: this.state.selectedTags,
                imageUrl: this.picInput.value,
                organisations: this.state.selectedOrganisations
            };
            if (this.props.inEdit) {
                newProject.uri = this.props.projectInEdit._links.self.href;
            }
            this.props.onSave(newProject);
            this.clearForm();
        }
    };

    handleOrganisationSelect = (selectedOrganisations) => {
        this.setState({selectedOrganisations: selectedOrganisations})
    }

    handleCategorySelect = (selectedCategories) => {
        this.setState({selectedCategories: selectedCategories})
    }

    handleTagSelect = (selectedTags) => {
        this.setState({selectedTags: selectedTags})
    }

    handleStartDateSelect = (startDate) => {
        let date = format(new Date(startDate), 'yyyy-MM-dd');
        this.setState({selectedStartDate: date})
    }

    handleEndDateSelect = (endDate) => {
        let date = format(new Date(endDate), 'yyyy-MM-dd');
        this.setState({selectedEndDate: date})
    }

    validateInputs = () => {
        let errors = [];
        if (isEmpty(this.nameInput.value)) {
            errors.push({field: "name", reason: "Darf nicht leer sein!"});
        }
        if (!isEmpty(this.linkInput.value)) {
            if (
                !isURL(this.linkInput.value, {
                    require_protocol: true,
                    protocols: ["http", "https"],
                })
            ) {
                errors.push({
                    field: "link",
                    reason: "Es wurde keine gültige URL angegeben!",
                });
            }
        }
        if (!isEmpty(this.picInput.value)) {
            if (
                !isURL(this.picInput.value, {
                    require_protocol: true,
                    protocols: ["http", "https"],
                })
            ) {
                errors.push({
                    field: "projectUrl",
                    reason: "Es wurde keine gültige URL angegeben!",
                });
            }
        }
        if (!(this.state.selectedStartDate)) {
            errors.push({
                field: "from",
                reason: "Projekte müssen ein gültiges Startdatum besitzen!",
            });
        }
        if (!(this.state.selectedOrganisations) || this.state.selectedOrganisations.length === 0) {
            errors.push({
                field: "organisations",
                reason: "Es muss mindestens eine Organisation gewählt sein!",
            });
        }
        this.setState({validationErrors: errors});
        return errors.length === 0;
    }

    clearForm = () => {
        this.setState({
            validationErrors: [],
            selectedOrganisations: [],
            selectedCategories: [],
            selectedTags: [],
            selectedStartDate: undefined,
            selectedEndDate: undefined
        });
    };

    hasError(propertyName) {
        return this.state.validationErrors.some((e) => e.field === propertyName);
    }

    getError(propertyName) {
        let error = this.state.validationErrors.find(
            (e) => e.field === propertyName
        );
        if (error) {
            return error.reason;
        }
        return "";
    }

    render() {
        return (
            <div className="modal fade" id="addProjectModal" tabIndex="-1" role="dialog"
                 aria-labelledby="addProjectModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="addProjectModal">
                                Projekt erstellen
                            </h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"
                                    onClick={this.clearForm}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form id="input-form" onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <label>Über das Projekt:</label>
                                    <input className="form-control" placeholder="Beschreibung eingeben"
                                           ref={(descInput) => (this.descInput = descInput)}/>
                                </div>
                                <div className="form-group">
                                    <label>Projekttitel</label>
                                    <input className={`form-control ${this.hasError("name") ? "is-invalid" : ""}`}
                                           placeholder="Titel eingeben"
                                           ref={(nameInput) => (this.nameInput = nameInput)}/>
                                    <div className="invalid-feedback">
                                        {this.getError("name")}
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label>Beginn</label>
                                    <SelectDate className="form-control"
                                                date={convertToDate(this.state.selectedStartDate)}
                                                setDate={this.handleStartDateSelect}/>
                                    <input hidden
                                           className={`form-control ${this.hasError("to") ? "is-invalid" : ""}`}/>
                                    <div className="invalid-feedback">
                                        {this.getError("from")}
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label>Ende:</label>
                                    <SelectDate className="form-control"
                                                date={convertToDate(this.state.selectedEndDate)}
                                                setDate={this.handleEndDateSelect}/>
                                    <input hidden
                                           className={`form-control ${this.hasError("from") ? "is-invalid" : ""}`}/>
                                    <div className="invalid-feedback">
                                        {this.getError("from")}
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label>Kategorie:</label>
                                    <SelectCategories className="form-control"
                                                      selectedCategories={this.state.selectedCategories}
                                                      connectedCategories={this.props.connectedCategories}
                                                      setCategories={this.handleCategorySelect}/>
                                </div>
                                <div className="form-group">
                                    <label>Link:</label>
                                    <input className={`form-control ${this.hasError("link") ? "is-invalid" : ""}`}
                                           placeholder="Link eingeben"
                                           ref={(linkInput) => (this.linkInput = linkInput)}/>
                                    <div className="invalid-feedback">
                                        {this.getError("link")}
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label>Tags:</label>
                                    <SelectTags className="form-control" selectedTags={this.state.selectedTags}
                                                connectedTags={this.props.connectedTags}
                                                setTags={this.handleTagSelect}/>
                                </div>
                                <div className="form-group">
                                    <label>Bild:</label>
                                    <input className={`form-control ${this.hasError("projectUrl") ? "is-invalid" : ""}`} placeholder="URL eingeben"
                                           ref={(picInput) => (this.picInput = picInput)}/>
                                    <div className="invalid-feedback">
                                        {this.getError("projectUrl")}
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label>Beteiligte Organisationen:</label>
                                    <SelectOrganisation className="form-control"
                                                        selectedOrganisations={this.state.selectedOrganisations}
                                                        connectedOrganisation={this.props.connectedOrganisation}
                                                        setOrganisations={this.handleOrganisationSelect}/>
                                    <input hidden className={`${this.hasError("organisations") ? "is-invalid" : ""}`}/>
                                    <div className="invalid-feedback">
                                        {this.getError("organisations")}
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal"
                                    onClick={this.clearForm}> Schließen
                            </button>
                            <button type="button" className="btn btn-primary" onClick={this.onSubmit}> Projekt
                                speichern
                            </button>
                            {this.props.serverConnectionError ? (
                                <div className="alert alert-danger">
                                    Fehler beim Zugriff auf den Server!
                                </div>
                            ) : (
                                ""
                            )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

