import React from 'react'
import ReactDOM from 'react-dom'
import AddButton from "./AddButton";

/*
    author: Max Karehnke
    Does the AddButton.js render?
 */

it('renders without crashing', () => {
    const svg = document.createElement('svg');
    ReactDOM.render(<AddButton/>, svg)
});