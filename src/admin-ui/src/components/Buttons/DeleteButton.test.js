import React from 'react'
import ReactDOM from 'react-dom'
import DeleteButton from "./DeleteButton";

/*
    author: Max Karehnke
    Does the DeleteButton.js render?
 */

it('renders without crashing', () => {
    const svg = document.createElement('svg');
    ReactDOM.render(<DeleteButton/>, svg)
});