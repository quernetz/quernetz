import React from 'react'
import ReactDOM from 'react-dom'
import DeleteButtonFloatRight from "./DeleteButtonFloatRight";

/*
    author: Max Karehnke
    Does the DeleteButtonFloatRight.js render?
 */

it('renders without crashing', () => {
    const svg = document.createElement('svg');
    ReactDOM.render(<DeleteButtonFloatRight/>, svg)
});
