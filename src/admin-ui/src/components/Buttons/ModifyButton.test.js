import React from 'react'
import ReactDOM from 'react-dom'
import ModifyButton from "./ModifyButton";

/*
    author: Max Karehnke
    Does the ModifyButton.js render?
 */

it('renders without crashing', () => {
    const svg = document.createElement('svg');
    ReactDOM.render(<ModifyButton/>, svg)
});