import React from "react";
import "bootstrap"
import SelectOrganisation from "../Links/SelectOrganisation.js";
import SelectDate from "../Links/SelectDate";
import {isEmpty, isURL} from "validator";
import {format} from "date-fns";
import isDate from "../../utils/isValidDate";
import {convertToDate} from "../../utils/dateFormatting";

const VALID_DATE_FORMAT = "yyyy-mm-dd";

export default class InterviewForm extends React.Component {
    constructor(props) {
        super(props);
        const interviewInEdit = (typeof this.props.interviewInEdit === 'undefined') ? false : this.props.interviewInEdit;
        this.state = {
            selectedOrganisations: [],
            selectedDate: undefined,
            interviewInEdit: interviewInEdit,
            validationErrors: [],
        };
    }

    componentDidUpdate(prevProps) {
        if (this.props.inEdit !== prevProps.inEdit) {
            this.setState({ 
              inEdit: this.props.inEdit,
              selectedOrganisations: []
            });
            this.clearForm();
        }
        if (
            typeof this.props.interviewInEdit !== 'undefined' && this.props.interviewInEdit !== prevProps.interviewInEdit
        ) {
            let updateSelectedOrganisation = true;
            if(prevProps.connectedOrganisation && this.props.connectedOrganisation){
              updateSelectedOrganisation = !(prevProps.connectedOrganisation[0] === this.props.connectedOrganisation[0]);
            }
            this.nameInput.value = this.props.interviewInEdit.name;
            this.linkInput.value = this.props.interviewInEdit.link;
            this.ytLinkInput.value = this.props.interviewInEdit.youtubeLink;
            this.descInput.value = this.props.interviewInEdit.description;
            this.setState({
                selectedOrganisations: updateSelectedOrganisation ? this.props.connectedOrganisation : this.state.selectedOrganisations,
                selectedDate: this.props.interviewInEdit.date,
                interviewInEdit: this.props.interviewInEdit,
            });
        }
    }

    handleOrganisationSelect = (selectedOrganisations) => {
        this.setState({
            selectedOrganisations: selectedOrganisations,
        });
    };

    handleDateSelect = (selectedDate) => {
        let date = format(new Date(selectedDate), 'yyyy-MM-dd');
        this.setState({selectedDate: date})
    }

    validateInputs = () => {
        let errors = [];
        if (isEmpty(this.nameInput.value)) {
            errors.push({
                field: "name",
                reason: "Die Angabe eines Interview-Namens ist erforderlich!"
            });
        }
        if (!isEmpty(this.linkInput.value)) {
            if (!isURL(this.linkInput.value)) {
                errors.push({
                    field: "link",
                    reason: "Wenn angegeben, muss es sich um eine gültige URL handeln!"
                })
            }
        }
        if (!isEmpty(this.ytLinkInput.value)) {
            if (!isURL(this.ytLinkInput.value)) {
                errors.push({
                    field: "ytlink",
                    reason: "Wenn angegeben, muss es sich um eine gültige URL handeln!"
                })
            }
        }
        if(!(this.state.selectedDate) || !isDate(this.state.selectedDate, VALID_DATE_FORMAT)){
            errors.push({
                field: "date",
                reason: `Bei diesem Feld muss es sich um ein gültiges Datum im Format ${VALID_DATE_FORMAT} handeln!`
            })
        }
        if (isEmpty(this.descInput.value)) {
            errors.push({
                field: "description",
                reason: `Die Angabe einer Beschreibung ist erforderlich!`
            })
        }
        if ((!this.state.selectedOrganisations) || this.state.selectedOrganisations.length < 1) {
            errors.push({
                field: "organisation",
                reason: "Es muss eine Organisation angegeben werden!"
            })
        }
        this.setState({validationErrors: errors});
        return (errors.length === 0);

    }

    hasError = (field) => {
        return this.state.validationErrors.some(e => e.field === field);
    }

    getError = (field) => {
        const error = this.state.validationErrors.find(e => e.field === field);
        if (error) {
            return error.reason;
        }
        return "";
    }

    onSubmit = (event) => {
        event.preventDefault();
        if (this.validateInputs()) {
            let newInterview = {
                name: this.nameInput.value,
                link: this.linkInput.value,
                youtubeLink: this.ytLinkInput.value,
                date: this.state.selectedDate,
                description: this.descInput.value,
                organisations: this.state.selectedOrganisations,
            };
            if (this.props.inEdit) {
                newInterview.uri = this.props.interviewInEdit._links.self.href;
            }
            this.props.onSave(newInterview);
            this.clearForm();
        }
    };

    clearForm = () => {
        this.setState({
            selectedDate: undefined,
            validationErrors: [],
        });
    };

    render() {
        return (
            <div>
                <div className="modal fade" id="InterviewModal" tabIndex="-1" role="dialog"
                     aria-labelledby="InterviewModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="InterviewModal">
                                    Interview erstellen
                                </h5>
                                <button type="button" onClick={this.clearForm} className="close" data-dismiss="modal"
                                        aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form id="interview-form" onSubmit={this.onSubmit}>
                                    <div className="form-group">
                                        <label>Name:</label>
                                        <input className={`form-control ${this.hasError("name") ? "is-invalid" : ""}`}
                                               placeholder="Name eingeben"
                                               ref={(nameInput) => (this.nameInput = nameInput)}/>
                                        <div className="invalid-feedback">
                                            {this.getError("name")}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>Link</label>
                                        <input className={`form-control ${this.hasError("link") ? "is-invalid" : ""}`}
                                               placeholder="Link eingeben"
                                               ref={(linkInput) => (this.linkInput = linkInput)}/>
                                        <div className="invalid-feedback">
                                            {this.getError("link")}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>Youtube-Link</label>
                                        <input className={`form-control ${this.hasError("ytlink") ? "is-invalid" : ""}`}
                                               placeholder="Youtube-Link eingeben"
                                               ref={(ytLinkInput) => (this.ytLinkInput = ytLinkInput)}/>
                                        <div className="invalid-feedback">
                                            {this.getError("ytlink")}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>Datum:</label>
                                        <SelectDate className="form-control"
                                                    date={convertToDate(this.state.selectedDate)}
                                                    setDate={this.handleDateSelect}/>
                                        <input hidden className={`form-control ${ this.hasError("date") ? "is-invalid" : "" }`} />
                                        <div className="invalid-feedback">
                                            {this.getError("date")}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>Beschreibung:</label>
                                        <input
                                            className={`form-control ${this.hasError("description") ? "is-invalid" : ""}`}
                                            placeholder="Beschreibung eingeben"
                                            ref={(descInput) => (this.descInput = descInput)}/>
                                        <div className="invalid-feedback">
                                            {this.getError("description")}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>Verknüpfte Organisationen:</label>
                                        <SelectOrganisation className="form-control"
                                                            selectedOrganisations={this.state.selectedOrganisations}
                                                            setOrganisations={this.handleOrganisationSelect}
                                                            connectedOrganisation={this.props.connectedOrganisation}/>
                                        <input hidden
                                               className={`form-control ${this.hasError("organisation") ? "is-invalid" : ""}`}/>
                                        <div className="invalid-feedback">
                                            {this.getError("organisation")}
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal"
                                        onClick={this.clearForm}> Schließen
                                </button>
                                <button type="button" className="btn btn-primary" onClick={this.onSubmit}> Interview
                                    speichern
                                </button>
                                {this.props.serverConnectionError
                                    ? (<div className="alert alert-danger"> Fehler beim Zugriff auf den Server! </div>)
                                    : ("")
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
