import React from "react";
import InterviewForm from "./InterviewForm";
import ModalError from "../misc/ModalError";
import '../Styles/ButtonStyle.css';
import DeleteButton from "../Buttons/DeleteButton";
import ModifyButton from "../Buttons/ModifyButton";
import $ from "jquery";
import {extractIdFromSelf} from "../../utils/extractIdFromSelf";
import {getConnectedObjects, getLinks, updateLinks} from "../../utils/helperConnections";
import {convertToGermanDateString} from "../../utils/dateFormatting";


window.jQuery = $;
window.$ = $;
global.jQuery = $;

const REST_API_ERROR_MODAL_ID = "InterviewBackendErrorModal";

export default class InterviewTable extends React.Component {
    state = {
        interviews: this.props.interviews,
        inEdit: false,
        interviewInEdit: [],
        serverConnectionError: false,
        backendErrorMessage: "",
        connectedOrganisation: [],
        isDataFetched: false,
    };

    componentDidMount = async () => {
        const organisationLinks = await getLinks(this.props.interviews, "organisations");
        this.setState({
            organisationLinks: organisationLinks,
            isDataFetched: true
        });
    };

    showModal = modalId => {
        $(modalId).modal("show");
    };

    hideModal = modalId => {
        $(modalId).modal("hide");
    };

    onEdit = index => {
        this.setState({
            interviewInEdit: index,
            inEdit: true
        });
    };

    onAdd = () => {
        document.getElementById("interview-form").reset();
        this.setState({
            inEdit: false,
            interviewInEdit: null,
        });

    };

    onSave = async interview => {
        if (this.state.inEdit) {
            const result = await this.props.backend.patch("interview", interview);
            if (result.ok) {
                const updatedInterview = result.data;
                this.setState({
                    interviews: this.state.interviews.map((el, index) => (index === this.state.interviewInEdit ? Object.assign({}, el, updatedInterview) : el))
                    ,
                    organisationLinks: updateLinks(interview.organisations, this.state.organisationLinks, updatedInterview)
                });
                this.hideModal("#InterviewModal");
            } else if (result.serverValidationError) {
                //handle serverside validation error
            } else if (result.unknown) {
                console.error("Unknown error");
            } else {
                this.setState({serverConnectionError: true});
            }
        } else {
            const result = await this.props.backend.post("interview", interview);
            if (result.ok) {
                const savedInterview = result.data;
                this.setState({
                    interviews: this.state.interviews.concat(savedInterview),
                    organisationLinks: updateLinks(interview.organisations, this.state.organisationLinks, result.data, true)
                });
                this.hideModal("#InterviewModal");
            } else if (result.internalError) {
                this.setState({serverConnectionError: true});
            } else if (result.serverValidationError) {
                //handle serverside validation error
            } else if (result.integrityViolation) {
                // handle integrity violation
            } else if (result.notFound) {
                //handle notFound error
            } else if (result.unknown) {
                //handle other errors
            } else {
                console.error("Unspecified ApiResult Error!");
            }
        }
    };

    onClosingBackendError = () => {
        this.setState({backendErrorMessage: ""});
    };

    onDelete = async uri => {
        if (window.confirm("Soll das Interview wirklich gelöscht werden?")) {
            const result = await this.props.backend.delete(uri);
            if (result.ok) {
                const newList = this.state.interviews.filter(interview => interview._links.self.href !== uri);
                this.removeDeletedInterview(uri, this.state.organisationLinks);
                this.setState({interviews: newList});
            } else if (result.integrityViolation) {
                this.setState({backendErrorMessage: result.data.reason});
                this.showModal(`#${REST_API_ERROR_MODAL_ID}`);
            } else {
                this.setState({backendErrorMessage: "Unbekannter Fehler beim Löschen des Interviews!"});
                this.showModal(`#${REST_API_ERROR_MODAL_ID}`);
            }
        }
    };

    removeDeletedInterview = (uri, organisationLinks) => {
        const interviewId = extractIdFromSelf(uri);
        organisationLinks.forEach(organisationLink => {
            if (interviewId === organisationLink.Id) {
                organisationLinks = organisationLinks.filter(orgaLink => orgaLink.Id !== interviewId);
            }
        })
        this.setState({organisationLinks: organisationLinks})
    }

    render() {
        return (
            this.state.isDataFetched &&
            <div className="table-component">
                <h1 className="float-left">Interviews:</h1>
                <button type="button" className="btn btn-quernetzer float-right" data-toggle="modal"
                        data-target="#InterviewModal" onClick={this.onAdd}>
                    Interview erstellen
                </button>
                <div className="table-responsive">
                    <table className="table table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Link</th>
                            <th>Youtube-Link</th>
                            <th>Datum</th>
                            <th>Beschreibung</th>
                            <th>Verknüpfte Organisationen</th>
                            <th/>
                            <th/>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.interviews.map((interview, index) => (
                            <tr key={index}>
                                <td>{interview.name ? interview.name : null}</td>
                                <td>{interview.link ?
                                    <a style={{color: "#00987a"}} href={interview.link}>Weitere Infos</a>
                                    : null}
                                </td>
                                <td>{interview.youtubeLink ?
                                    <a style={{color: "#00987a"}} href={interview.youtubeLink}>Hier zum Interview</a>
                                    : null}
                                </td>
                                <td>{convertToGermanDateString(interview.date)}</td>
                                <td>{interview.description ? interview.description : null}</td>
                                <td>
                                    <ul>{getConnectedObjects(interview, this.state.organisationLinks).map((organisation, indexOfConnectedOrganisations) =>
                                        <li key={indexOfConnectedOrganisations}>{organisation.name}</li>)}
                                    </ul>
                                </td>
                                <td>
                                    <div data-toggle="modal" onClick={() => this.onEdit(index)}
                                         data-target="#InterviewModal">
                                        <ModifyButton/>
                                    </div>
                                </td>
                                <td>
                                    <div onClick={() => this.onDelete(interview._links.self.href)}>
                                        <DeleteButton/>
                                    </div>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                    <InterviewForm onSave={this.onSave} serverConnectionError={this.state.serverConnectionError}
                                   connectedOrganisation={this.state.inEdit ? getConnectedObjects(this.state.interviews[this.state.interviewInEdit], this.state.organisationLinks) : []}
                                   inEdit={this.state.inEdit}
                                   interviewInEdit={this.state.interviews[this.state.interviewInEdit]}/>
                    <ModalError modalId={REST_API_ERROR_MODAL_ID} errorTitle="Fehler beim Löschen des Interviews!"
                                errorMessage={this.state.backendErrorMessage} onClosing={this.onClosingBackendError}/>
                </div>
            </div>
        );
    }
}