import React from 'react'
import InterviewTable from "./InterviewTable"
import {shallow} from 'enzyme';
import {mount} from "enzyme";
import Backend from "../../data-access/__mocks__/Backend";


let interviews = [
    {
        name: 'Quergefragt #1 - Patrice von Leipspeis',
        link: 'https://leipspeis.de/',
        youtubeLink: 'https://www.youtube.com/embed/2es-4oQgLTU',
        date: '2020-05-2021',
        description: '"Stolz präsentieren wir unsere neue Interview-Rubrik Quergefragt! In der ersten Folge verrät uns Patrice von Leipspeis ...',
        imageUrls: ["https://www.duckduckgo.com", "https.//wikipedia.com"],
        organisation: [
            {
                name: "Leipspeis"
            },
            {
                name: "Stadtreinigung"
            }
        ],
        _links: {self: {href: "http://localhost:8080/api/interviews/24"}}
    },
    {
        name: 'Quergefragt #2 - Lisa & Eberhard vom Cafe kaputt',
        link: 'https://www.leipzig-leben.de/cafe-kaputt-leipzig/',
        youtubeLink: 'https://www.youtube.com/embed/Ocrs0NJd§YA',
        date: '2020-05-2021',
        description: '"In der zweiten Folge verraten uns Lisa und Eberhard vom Cafe kaputt, warum sich ...',
        imageUrls: ["https://www.duckduckgo.com", "https.//wikipedia.com"],
        organisation: [
            {
                name: "Cafe kaputt"
            },
            {
                name: "Leibspeis"
            }
        ],
        _links: {self: {href: "http://localhost:8080/api/interviews/25"}}
    },
];

const organisationLinks = [
    {
        Id: 24,
        relatedObjects: [5]
    },
    {
        Id: 25,
        relatedObjects: [5]
    }
];

describe('tests interview table', () => {

    test ('if one interviews is deleted', async () => {
        // shallow render Interview Table with fake data
        const wrapper = mount(<InterviewTable backend={Backend} interviews={interviews}/>);
        wrapper.instance().setState({isDataFetched: true, organisationLinks: organisationLinks, interviews: interviews});
        // tests if amount of interviews decreases after delete
        expect(wrapper.state('interviews').length).toBe(2);
        window.confirm = jest.fn().mockImplementation(() => true)
        const instance = wrapper.instance();
        await instance.onDelete('http://localhost:8080/api/interviews/24');
        expect(wrapper.state('interviews').length).toBe(1);
    })
});


