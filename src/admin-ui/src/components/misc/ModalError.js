import React from "react"

export default class ModalError extends React.Component {
  render(){
    return (
      <div className="modal fade" id={this.props.modalId} tabIndex="-1" role="dialog" >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="addProjectModal">
                {this.props.errorTitle}
              </h5>
              <button type="button" className="close" onClick={this.props.onClosing} data-dismiss="modal" aria-label="Close" >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body"> 
              <div className="alert alert-danger">
                {this.props.errorMessage}
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.props.onClosing}>Schließen</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
