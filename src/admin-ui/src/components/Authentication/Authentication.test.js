/**
 * Author: Hartmut Knaack
 *
 * @fileoverview Snapshot test for Authentication component.
 */

import React from "react";
import Authentication from "./Authentication.js";
import renderer from "react-test-renderer";
import ReactDOM from "react-dom";
import App from "../../App.js";

describe("Authentication component", () => {
    test("renders correctly", () => {
        const tree = renderer.create(<Authentication />).toJSON();

        expect(tree).toMatchSnapshot();
    });
});
