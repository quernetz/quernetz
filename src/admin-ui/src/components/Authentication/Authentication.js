/**
 * Author: Hartmut Knaack
 *
 * @fileoverview Authentication component to handle User login and session
 * management based on JSON web token.
 */

import React from "react";
import LoginForm from "./LoginForm";
import "./Authentication.css";
import Auth from "../../data-access/Auth.js";
import {fetchDataPost} from "../../data-access/fetchData.js";
import BackendUrl from "../../data-access/backendconfig.js";

/**
 * Authentication component.
 * Contains two states:
 *   - Loggedin
 *   - Loggedout
 * Renders a status message and a Login or Logout button.
 */
export default class Authentication extends React.Component {

    constructor(props) {
        super(props);

        if(Auth.isUserAuthenticated()) {
            const statustext = this.buildStatustext();
            this.state = {
                isLoggedIn: true,
                statustext: statustext
            };
            this.updateStatustext();
        } else {
            this.state = {
                isLoggedIn: false,
                statustext: "Nicht angemeldet"
            };
        }
    }

    componentWillUnmount() {
        clearTimeout(this.timeoutID);
        clearTimeout(this.setupIntervalID);
        clearInterval(this.intervalID)
    }

    setTimers(token) {
        const tokenExpirationTime = Auth.getTokenExpirationTime(token);
        const tokenRemainingSeconds = tokenExpirationTime - Math.floor(Date.now() / 1000);

        /* automatically close session at token expiration */
        this.timeoutID = setTimeout( () => this.handleLogoutClick(), tokenRemainingSeconds * 1000);

        /* initialize remaining time status text interval to update every minute */
        const secondsToNextRemainingMinute = tokenRemainingSeconds % 60;
        this.setupIntervalID = setTimeout( () => this.setupInterval(), secondsToNextRemainingMinute * 1000);
    }

    handleLoginClick = (credentials) => {
        /* send credentials to backend server, receive token */
        fetchDataPost(BackendUrl() + "/authenticate", credentials).then((response) => {
            try {
                if (typeof response === 'undefined' || !response.hasOwnProperty('jwt')) {
                    throw new Error("Login fehlgeschlagen");
                };

                const token = response.jwt;

                Auth.authenticateUser(token);
                if (Auth.isUserAuthenticated()) {
                    /* valid Token stored */
                    this.setTimers(token);

                    /* update status message */
                    this.updateStatustext();

                    this.setState({
                        isLoggedIn: true,
                    }, this.props.updateAuthenticationStatus());
                }
            } catch (error) {
                this.setState({
                    isLoggedIn: false,
                    statustext: error.message
                });
            }
        });
    };

    handleLogoutClick = () => {
        clearTimeout(this.timeoutID);
        clearInterval(this.intervalID);
        clearInterval(this.setupIntervalID);
        Auth.deauthenticateUser();
        this.setState({
            isLoggedIn: false,
            statustext: "Nicht angemeldet"
        }, this.props.updateAuthenticationStatus());
    };

    setupInterval() {
        clearInterval(this.setupIntervalID);
        this.updateStatustext();
        this.intervalID = setInterval( () => this.updateStatustext(), 60000);
    }

    buildStatustext() {
        if (Auth.isUserAuthenticated) {
            const token = Auth.getToken();
            const tokenExpirationTime = Auth.getTokenExpirationTime(token);
            const tokenRemainingSeconds = tokenExpirationTime - Math.floor(Date.now() / 1000);
            const name = Auth.getTokenSubject(token);
            const statustext = "Angemeldet als " + name + " für weitere " + Math.floor(tokenRemainingSeconds / 60) + " Minuten";

            return statustext;
        }
    };

    updateStatustext() {
        const statustext = this.buildStatustext();

        this.setState({
            statustext: statustext
        });
    }

    LoggedIn() {
        return (
                <div className="Authentication">
                    {this.state.statustext}
                    <button className="btn btn-primary" onClick={this.handleLogoutClick}>
                        Logout
                    </button>
                </div>);
    }

    LoggedOut() {
        return (
                <div className="Authentication">
                    {this.state.statustext}
                    <button className="btn btn-success" data-toggle="modal" data-target="#loginFormModal">
                        Login
                    </button>
                    <LoginForm handleLoginClick={this.handleLoginClick} />
                </div>
        );
    }

    render() {
        const isLoggedIn = this.state.isLoggedIn;

        return isLoggedIn ? this.LoggedIn() : this.LoggedOut();
    }
}
