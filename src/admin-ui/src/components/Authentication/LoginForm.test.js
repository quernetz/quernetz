/**
 * Author: Hartmut Knaack
 *
 * @fileoverview Snapshot test for LoginForm component.
 */

import React from "react";
import LoginForm from "./LoginForm.js";
import renderer from "react-test-renderer";
import ReactDOM from "react-dom";
import App from "../../App.js";

describe("LoginForm component", () => {
    test("renders correctly", () => {
        const tree = renderer.create(<LoginForm />).toJSON();

        expect(tree).toMatchSnapshot();
    });
});
