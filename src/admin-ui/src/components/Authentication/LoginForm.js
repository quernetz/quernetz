/**
 * Author: Hartmut Knaack
 *
 * @fileoverview LoginForm component to get username and password for
 * authentication.
 */

import React from "react";
import "./LoginForm.css";
import "bootstrap";

/**
 * LoginForm component.
 * Calls handleLoginClick()-method of parent element on click of submit button,
 * passing login credetials as JSON object.
 */
export default class LoginForm extends React.Component {

    onSubmit = (event) => {
        const credentials = {
            username: this.nameInput.value,
            password: this.passwordInput.value
        }

       event.preventDefault();
       this.props.handleLoginClick(credentials);
       this.clearForm();
   }

   clearForm = () => {
       document.getElementById("login-form").reset();
   }

    render() {
        return (
            <div>
                <div className="modal fade" id="loginFormModal" tabIndex="-1" role="dialog" aria-labelledby="loginFormModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="addProjectModal">Einloggen zum Daten bearbeiten</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.clearForm}>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form id="login-form" onSubmit={this.onSubmit}>
                                    <div className="form-group">
                                        <label htmlFor="username">Benutzername</label>
                                        <input id="username" className="form-control" placeholder="Benutzername eingeben" ref={nameInput => this.nameInput = nameInput}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="password">Passwort</label>
                                        <input id="password" className="form-control" placeholder="Passwort eingeben" type="password" ref={passwordInput => this.passwordInput = passwordInput}/>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-danger btn-default" data-dismiss="modal" onClick={this.clearForm}>Schließen</button>
                                <button type="button" className="btn btn-success" data-dismiss="modal" onClick={this.onSubmit}>Login</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
