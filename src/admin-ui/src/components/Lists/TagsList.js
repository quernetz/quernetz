import React from "react";
import { isEmpty } from "validator";
import '../Styles/ButtonStyle.css';
import DeleteButtonFloatRight from "../Buttons/DeleteButtonFloatRight";
import AddButton from "../Buttons/AddButton";

export default class TagsList extends React.Component {

    state = {
        tags: this.props.tags,
        invalidTag: false
    };

    handleSubmit = async event => {
        event.preventDefault();
        if (isEmpty(this.newTagInput.value)) {
            this.setState({ invalidTag: true });
        } else {
            this.setState({ invalidTag: false });
            let newTag = {
                text: this.newTagInput.value,
            };
            const result = await this.props.backend.post("tag", newTag);
            if (result.ok) {
                const tag = result.data;
                this.setState({ tags: this.state.tags.concat(tag) });
                this.clearInput();
            }
        }
    };

    handleChange = () => {
        if (this.newTagInput.value.length > 0) {
            this.setState({ invalidTag: false });
        }
    };

    clearInput = () => {
        document.getElementById("tagInput").reset();
        this.setState({ invalidTag: false });
    };

    handleDelete = async uri => {
        if (window.confirm("Soll der Tag wirklich gelöscht werden?")) {
            const result = await this.props.backend.delete(uri);
            if (result.ok) {
                const newTagsList = this.state.tags.filter(tag => tag._links.self.href !== uri);
                this.setState({ tags: newTagsList });
            }
        }
        this.clearInput();
    };

    render() {
        return (
            <div className="table-component">
                <h1>Tags:</h1>
                <form onSubmit={this.handleSubmit} onChange={this.handleChange} id="tagInput">
                    <div className="input-group">
                        <input type="text" className={`form-control ${this.state.invalidTag ? "is-invalid" : ""}`} placeholder="#neuerTag" ref={newTagInput => (this.newTagInput = newTagInput)} />
                        <div className="input-group-btn">
                            <AddButton/>
                        </div>
                        <div className="invalid-feedback">Dieses Feld darf nicht leer sein!</div>
                    </div>
                </form>

                <ul className="list-group">
                    {this.state.tags.map(tag => (
                        <li className="list-group-item list-group-item-action" key={tag._links.self.href}>
                            <div>
                                {tag.text}
                                <div onClick={() => this.handleDelete(tag._links.self.href)}>
                                    <DeleteButtonFloatRight />
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}
