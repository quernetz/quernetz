import renderer from "react-test-renderer";
import CategoriesList from "./CategoriesList";
import Backend from "../../data-access/__mocks__/Backend";
import React from "react";

const categories = [
    {
        categoryName : "Umwelt",
        _links:{
            self:{
                href: "bcharlie.js"
            }
        }
    },
    {
        categoryName : "Politik",
        _links:{
            self:{
                href: "abravo.js"
            }
        }
    }
];

test('snapshot', () => {
    const tree = renderer.create(<CategoriesList backend={Backend} categories={categories}/>).toJSON();
    expect(tree).toMatchSnapshot();
})