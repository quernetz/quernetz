import React from 'react';
import {isEmpty} from "validator";
import '../Styles/ButtonStyle.css';
import DeleteButtonFloatRight from "../Buttons/DeleteButtonFloatRight";
import AddButton from "../Buttons/AddButton";

export default class CategoriesList extends React.Component {

    state = {
        categories: this.props.categories,
        invalidName: false
    };

    handleSubmit = async event => {
        event.preventDefault();
        if (isEmpty(this.newCategoryInput.value)) {
            this.setState({ invalidName: true });
        } else {
            let newCategory = {
                categoryName: this.newCategoryInput.value,
            };
            const result = await this.props.backend.post("category", newCategory);
            if (result.ok) {
                const category = result.data;
                this.clearInput();
                this.setState({
                    categories: this.state.categories.concat(category),
                    invalidName: false,
                });
            }
        }
    };

    handleChange = () => {
        if (this.newCategoryInput.value.length > 0) {
            this.setState({ invalidName: false });
        }
    };

    clearInput = () => {
        document.getElementById("categoryInput").reset();
        this.setState({ invalidName: false });
    };

    handleDelete = async uri => {
        if (window.confirm("Soll die Kategorie wirklich gelöscht werden?")) {
            const result = await this.props.backend.delete(uri);
            if (result.ok) {
                const newCategoriesList = this.state.categories.filter(category => category._links.self.href !== uri);
                this.setState({ categories: newCategoriesList });
            }
            this.clearInput();
        }
    };

    render() {
        return (
            <div className="table-component">
                <h1>Kategorien:</h1>
                <form onSubmit={this.handleSubmit} onChange={this.handleChange} id="categoryInput">
                    <div className="input-group">
                        <input type="text" className={`form-control ${this.state.invalidName ? "is-invalid" : ""}`} placeholder="Neue Kategorie" ref={newCategoryInput => (this.newCategoryInput = newCategoryInput)} />
                        <div className="input-group-btn">
                         <AddButton/>
                        </div>
                        <div className="invalid-feedback">Dieses Feld darf nicht leer sein!</div>
                    </div>
                </form>

                <ul className="list-group">
                    {this.state.categories.map(category => (
                        <li className="list-group-item list-group-item-action" key={category._links.self.href}>
                            {category.categoryName}
                            <div onClick={() => this.handleDelete(category._links.self.href)}>
                                <DeleteButtonFloatRight />
                            </div>
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}