import React from 'react'
import renderer from "react-test-renderer"
import ReactDOM from 'react-dom'
import CategoriesList from "./CategoriesList";
import {shallow} from 'enzyme';

import Backend from "../../data-access/__mocks__/Backend";

const categories = [
    {
        categoryName : "Umwelt",
        _links:{
          self:{
            href: "bcharlie.js"
          }
        }
    },
    {
        categoryName : "Politik",
        _links:{
          self:{
            href: "abravo.js"
          }
        }
    }
];

describe('tests Categories List', () => {

    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<CategoriesList backend={Backend} categories={categories}/>, div)
    });

    test('tests adding new categories', () => {

        // shallow render Categories List with fake data
        const wrapper = shallow(<CategoriesList backend={Backend} categories={categories}/>);

        // tests if correct amount of categories are passed
        expect((wrapper.state('categories')).length).toBe(2);
    });
});