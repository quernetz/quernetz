import renderer from "react-test-renderer";
import TagsList from "./TagsList";
import Backend from "../../data-access/__mocks__/Backend";
import React from "react";

const tags = [
    {
        text : "#yesWeCan",
        _links: {
            self:{href: "$abde"}
        }
    },
    {
        text : "#BLM",
        _links: {
            self:{href: "$abdke"}
        }
    },
    {
        text : "#Bauernhof",
        _links: {
            self:{href: "$abdkef"}
        }
    }
];

test('snapshot', () => {
    const tree = renderer.create(<TagsList backend={Backend} tags={tags}/>).toJSON();
    expect(tree).toMatchSnapshot();
})