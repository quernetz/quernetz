import React from 'react'
import renderer from "react-test-renderer"
import ReactDOM from 'react-dom'
import TagsList from "./TagsList";
import {shallow} from 'enzyme';

import Backend from "../../data-access/__mocks__/Backend";

const tags = [
    {
        text : "#yesWeCan",
        _links: {
          self:{href: "$abde"}
        }
    },
    {
        text : "#BLM",
        _links: {
          self:{href: "$abdke"}
        }
    },
    {
        text : "#Bauernhof",
        _links: {
          self:{href: "$abdkef"}
        }
    }
];

describe('tests Tags List', () => {

    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<TagsList backend={Backend} tags={tags}/>, div)
    });

    test('tests adding new categories', () => {

        // shallow render Tags List with fake data
        const wrapper = shallow(<TagsList backend={Backend} tags={tags}/>);

        // tests if correct amount of tags are passed
        expect((wrapper.state('tags')).length).toBe(3);
    });
});