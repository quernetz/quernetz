import {extractIdFromSelf} from "./extractIdFromSelf"

/**
 * @returns returns a array with related (tags, project....) over ids to the selectedEntitiy(project, organisation, interview....)
 * @param selectedEntity object(as example interview, project, organisation)
 * @param links data structure with relations over ids
 * @param linkedObjects is a array with all interviews/ projects/ organisations
 */
export function getConnectedObjects(selectedEntity, links) {
    let connectedObjects = [];
    if (selectedEntity === undefined) {
        return connectedObjects;
    } else {
        links.forEach(link => {
            if (link.Id === extractIdFromSelf(selectedEntity._links.self.href) && link.relatedObjects !== undefined) {
                link.relatedObjects.forEach((object) => {
                    connectedObjects.push(object);
                })
            }
        })
    }
    return connectedObjects;
}

/**
 * the function "updateLinks" receives a item(as example tags, projects, organisations...) and a data structure(linkToUpdate)
 * and returns a new structure with customized related (tags, project....) over ids.
 *
 * @returns returns a new structure with customized related (tags, project....) over ids.
 * @param newItemToLink object(as example tag, project, organisation...)
 * @param updatedEntity is the object inEdit(tag, project, organisation...)
 * @param httpPost is a bool that is true if the request is a post method
 * @param linkToUpdate is the data structure to update
 */
export function updateLinks(newItemToLink, linkToUpdate, updatedEntity, httpPost) {
    const newLinks = [];
    const updatedEntityId = extractIdFromSelf(updatedEntity._links.self.href);
    httpPost && linkToUpdate.push(linkToUpdate.push({Id: updatedEntityId, relatedObjects: []}));
    linkToUpdate.forEach(link => {
        if (link.Id === updatedEntityId && link.relatedObjects !== undefined) {
            newItemToLink.forEach((item) => {
                newLinks.push(item);
            })
            link.relatedObjects = newLinks;
        }
    })
    return linkToUpdate;
}

/**
 * the function "getLinks" receives a array with object as parameter(as example tags, projects, organisations...)
 * and returns a data structure with related (tags, project....) over ids.
 *
 * @returns returns data structure with related (tags, project....) over ids.
 * @param objects array with object as parameter(as example tags, projects, organisations...)
 * @param type as string(tags, projects, organisations...)
 */
export async function getLinks(objects, type) {
    let links = [];
    for (const object of objects) {
        const Id = extractIdFromSelf(object._links.self.href);
        const relatedObjects = object[type];
        links.push({
            Id: Id,
            relatedObjects: relatedObjects
        })
    }
    return links;
}
