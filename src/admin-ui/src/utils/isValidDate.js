// from https://gist.github.com/dwoodard/4348994
// Copy and pasted by Anton Rhein

/**
 * Returns true, if the passed value is a valid date, specified by userformat
 * Also checks for leap years
 * @param {*} value the value to evaluate
 * @param {*} userFormat the format of the passed (supposed) date string
 */
export default function isValidDate(value, userFormat) {
    // Set default format if format is not provided
    userFormat = userFormat || "mm/dd/yyyy";
    if (value !== undefined) {
        // Find custom delimiter by excluding the
        // month, day and year characters
        var delimiter = /[^mdy]/.exec(userFormat)[0];

        // Create an array with month, day and year
        // so we know the format by index
        var theFormat = userFormat.split(delimiter);

        // Get the user date now that we know the delimiter

        var theDate = value.split(delimiter);

        function isDate(date, format) {
            var m,
                d,
                y,
                i = 0,
                len = format.length,
                f;
            for (i; i < len; i++) {
                f = format[i];
                if (/m/.test(f)) m = date[i];
                if (/d/.test(f)) d = date[i];
                if (/y/.test(f)) y = date[i];
            }
            return (
                m > 0 &&
                m < 13 &&
                y &&
                y.length === 4 &&
                d > 0 &&
                // Is it a valid day of the month?
                d <= new Date(y, m, 0).getDate()
            );
        }
        return isDate(theDate, theFormat);
    }else return null;
}
