/**
 * the function "getLinkWithoutParams" receives a url as parameter in this case "serveraddress"
 * and looks for curly braves in this url, if there are curly braves they will cut out.
 * Then the function returns the url.
 *
 * @param link is a url as string
 * @return returns the link as string without the curly braces and everything between there
 */
function getLinkWithoutParams(link) {
    if (link.includes("{")) {
        return link.split("{")[0];
    }
    return link;
}

/**
 * the function "extractIdFromSelf" receives a url as parameter
 * and returns all character after the last '/'.
 *
 * @param link is a url as string
 * @returns returns all characters after the last '/'
 */
export function extractIdFromSelf(link) {
    return getLinkWithoutParams(link.substring(link.lastIndexOf("/") + 1));
}