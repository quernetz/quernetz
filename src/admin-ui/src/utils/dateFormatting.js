/**
 * Converts passed dateString ('yyyy-mm-dd') into german date string (dd.mm.yyyy)
 * @param {*} restDateString 
 * @returns string
 */
export function convertToGermanDateString(restDateString){
    if(restDateString){
        let parts = restDateString.split('-');
        return parts[2] + '.' + parts[1] + '.' + parts[0]
    }
    return "";
}

/**
 * parses the passed string into an date object
 * @param {*} restDateString 
 * @returns Date, or undefined is restDateString evaluates to falsy
 */
export function convertToDate(restDateString){
    if(restDateString){
        let parts = restDateString.split('-');
        return new Date(parts[0], parts[1] -1, parts[2]);
    }
}