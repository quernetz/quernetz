import React from "react";
import "./App.css";
import ProjectPage from "./pages/ProjectPage";
import OrganisationPage from "./pages/OrganisationPage";
import InterviewPage from "./pages/InterviewPage";
import CategoryPage from "./pages/CategoryPage";
import TagPage from "./pages/TagPage";
import WelcomePage from "./pages/WelcomePage";
import {BrowserRouter, Route, Redirect} from "react-router-dom";
import Nav from "./pages/navigation/Nav";
import Auth from "./data-access/Auth";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            authenticated: Auth.isUserAuthenticated(),
        };
    }

    updateAuthenticationStatus = () => {
        this.setState({authenticated: Auth.isUserAuthenticated()});
    }

    render() {
        const basedir = (typeof process.env.REACT_APP_FRONTEND_BASEDIR === 'undefined') ? '/' : process.env.REACT_APP_FRONTEND_BASEDIR;

        return (
            <div className="App">
                <BrowserRouter basename={basedir}>
                    <Nav updateAuthenticationStatus={this.updateAuthenticationStatus} authenticated={this.state.authenticated} />
                    <Route path="/" exact component={OrganisationPage} />
                    <Route path="/projekte" component={ProjectPage} />
                    <Route path="/organisationen" component={OrganisationPage} />
                    <Route path="/interviews" component={InterviewPage} />
                    <Route path="/kategorien" component={CategoryPage} />
                    <Route path="/tags" component={TagPage} />
                    <Route path="/welcome">
                        {this.state.authenticated ? <Redirect to="/" /> : <WelcomePage />}
                    </Route>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;