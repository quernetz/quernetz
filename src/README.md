# Code

Dieses Verzeichnis enthält den Programmcode des Softwaresystems, welches durch drei Komponenten realisiert wird.

Dieser wird durch folgende Komponenten-Verzeichnisse strukturiert:
- public - user interface
- admin - user interface
- server

------------------
#### Admin - ui
Das Verzeichnis admin-ui enthält den Quellcode der Frontend-Komponente der Deltasix-Applikation zur Verwaltung. Diese wird durch eine React-Webapp realisiert.

------------------
#### Public - ui
Das Verzeichnis public-ui enthält den Quellcode der öffentlichen Frontend-Komponente der Deltasix-Applikation. Diese wird durch eine React-Webapp realisiert.

------------------
#### Server
Das Verzeichnis backend enhält den Quellcode der Backend-Komponente der Deltasix-Applikation. Diese wird mittels des Java-Frameworks Spring Boot realisiert.
