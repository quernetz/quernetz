/**
 * the function "getLinkWithoutParams" receives a url as parameter in this case "serveraddress"
 * and looks for curly braves in this url, if there are curly braves they will cut out.
 * Then the function returns the url.
 *
 * @param link is a url as string
 * @return returns the link as string without the curly braces and everything between there
 */
export function getLinkWithoutParams(link) {
    if (link.includes("{")) {
        return link.split("{")[0];
    }
    return link;
}

/**
 * Authors: Ralf Stuerzner, Anton Diettrich
 * Date: 2020/08/01
 * the function "fetchData" parses data from the server
 *
 * @param serveraddress is a url as string
 * @param data are the fetched data from the url
 * @return returns the fetched data if everything is fetched
 */
export default async function fetchData(serveraddress) {
    try {
        let data = await fetch(getLinkWithoutParams(serveraddress));
        if (!data.ok) {
            throw new Error(data.status);
        }
        return await data.json();
    } catch (error) {
        console.log(error);
    }
}