import backendUrl from "./backendconfig"

export default async function fetchLogo (organisation) {
        let image = null;
        let id = organisation._links.self.href.substring(organisation._links.self.href.lastIndexOf("/") + 1);
        await fetch(backendUrl() + '/api/organisations/' + id + '/logo')
            .then(response => response.blob())
            .then((myBlob) => image = URL.createObjectURL(myBlob));
        return image;
}