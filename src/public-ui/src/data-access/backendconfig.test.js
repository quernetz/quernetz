/**
 * Author: Hartmut Knaack
 *
 * @fileoverview Test cases for backend configuration methods.
 */

import BackendUrl, {hostname, portNr, baseDirectory, protocol} from './backendconfig.js';

describe('BackendConfig', () => {
  test('hostname() with defined domain name', () => {
    expect(hostname()).toBe('www.bsi.bund.de');
  });

  test('portNr() with undefined port number', () => {
    expect(portNr()).toBe('');
  });

  test('baseDirectory() with undefined path', () => {
    expect(baseDirectory()).toBe('');
  });

  test('protocol() with defined protocol', () => {
    expect(protocol()).toBe('http');
  });

  test('BackendUrl() with some variables defined', () => {
    expect(BackendUrl()).toBe('http://www.bsi.bund.de');
  });
});
