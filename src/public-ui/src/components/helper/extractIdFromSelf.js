import {getLinkWithoutParams} from "../../data-access/fetchData";

/**
 * the function "extractIdFromSelf" receives a url as parameter
 * and returns all character after the last '/'.
 *
 * @param link is a url as string
 * @returns returns all characters after the last '/'
 */

export function extractIdFromSelf(link) {
    return getLinkWithoutParams(link.substring(link.lastIndexOf("/") + 1));
}