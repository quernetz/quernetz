import {extractIdFromSelf} from "./extractIdFromSelf";

it("extractIdFromSelf", () => {
    expect(extractIdFromSelf("http://localhost:8080/api/organisations/14")).toEqual("14");
    expect(extractIdFromSelf("http://localhost:8080/api/organisations/**###?!_")).toEqual("**###?!_");
    expect(extractIdFromSelf("http://localhost:8080/api/organisations/3.141592653589793238462643383279502884197169399375105820974944592")).toEqual("3.141592653589793238462643383279502884197169399375105820974944592");
    expect(extractIdFromSelf("http://localhost:8080/api/organisations/DasHierIstEinTest")).toEqual("DasHierIstEinTest");
});