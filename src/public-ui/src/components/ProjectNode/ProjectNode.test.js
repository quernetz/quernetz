import React from 'react'
import ReactDOM from 'react-dom'
import ProjectNode from "./ProjectNode";

const project =
    {
        id: 0,
        name: 'Erstes Projekt',
        description: 'Das war unser erstes Projekt',
        from: '2019, 01, 01',
        to: '2019, 01, 02',
        year: '2019',
        sectors: ["Umwelt", "Naturschutz"],
        _links: {self: {href: "http://localhost:8080/api/projects/0"}}
    };

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<svg><ProjectNode project={project}/></svg>, div)
});