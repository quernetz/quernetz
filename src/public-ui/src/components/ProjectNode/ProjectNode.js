import React, {Fragment} from "react";
import GenericPopUp from "../GenericPopUp";
import {extractIdFromSelf} from "../helper/extractIdFromSelf";
import {OverlayTrigger, Tooltip} from "react-bootstrap";
import Small from "../FontSize/Small";
import "./ProjectModal.css";
import {convertToGermanDateString} from "../../utils/dateFormatting";

export default class ProjectNode extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            categories: [],
            tags: [],
        };
    }

    handleClick = () => {
        this.setState({showModal: !this.state.showModal});
    };
    onMouseOver = (e) => {
        this.props.visibleNodesOnHover(true, e.target.id)
    };

    onMouseLeave = () => {
        this.props.visibleNodesOnHover(false)
    };

    render() {
        return (
            <Fragment>
                <OverlayTrigger key={this.props.project.index}
                                placement="right"
                                delay={{show: 250, hide: 400}}
                                overlay={<Tooltip id="button-tooltip">
                                    {this.props.project.name}
                                </Tooltip>}
                >
                    <circle strokeWidth="2"
                            stroke={"Black"}
                            r={this.props.project.enlarged ? 15 : 10}
                            fill={"#00977A"}
                            cx={this.props.project.x}
                            cy={this.props.project.y}
                            key={this.props.project.index}
                            opacity={this.props.project.transparent ? .1 : 1}
                            id={extractIdFromSelf(this.props.project._links.self.href)}
                            onClick={this.handleClick}
                            onMouseEnter={this.onMouseOver}
                            onMouseLeave={this.onMouseLeave}
                            cursor={"pointer"}
                    />

                </OverlayTrigger>
                {this.state.showModal ?
                    <GenericPopUp showModal={this.state.showModal} popupName={this.props.project.name}
                                  handleClick={this.handleClick}>
                        <div className="container" style={{color: "#333366"}}>
                            {/* Description of the Project*/}
                            <div><Small>{this.props.project.description}</Small></div>
                            {/* Renders 1-4 Pictures if existing*/}
                            <div className="grid-container">
                                <div className="grid-item">
                                    {this.props.project.imageUrl ? (
                                        <img src={this.props.project.imageUrl} alt={"Hier ist ein Bild des Projektes: " + this.props.project.name + " zu sehen."}/>
                                    ) : null}
                                </div>
                            </div>
                            <svg height="5" width="200">
                                <line x1="0" y1="0" x2="200" y2="0" style={{stroke: "grey", strokeWidth: "1"}}/>
                            </svg>
                            {/* All Categories of the Projekt: */}
                            <ul className="categories">
                                {this.props.project.categories.map(category => (
                                    <li key={category._links.self.href}>
                                        <div style={{padding: "3px"}}>
                                            <p>{category.categoryName}</p>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                            {/* All #Tags of the Projekt: */}
                            <ul className="tags">
                                {this.props.project.tags.map(tag => (
                                    <li key={tag._links.self.href}>
                                        <div style={{padding: "3px"}}>
                                            <p>{tag.text}</p>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                            {/* Time-Frame of the Projekt:*/}
                            <Small>{convertToGermanDateString(this.props.project.from)} {this.props.project.to ? (" - " + convertToGermanDateString(this.props.project.to)) : ""}</Small>
                            {/* Shows Link if existing*/}
                            {this.props.project.projectLink ? (
                                <div><a style={{color: "#00987a"}} href={this.props.project.projectLink}>Mehr
                                    Informationen</a></div>
                            ) : null}
                        </div>
                    </GenericPopUp>
                    : null}
            </Fragment>
        );
    }
}