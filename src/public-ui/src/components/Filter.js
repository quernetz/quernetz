import React from "react";
import '../styles/Filter.css';

export default class Filter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: [],
            categories: this.props.categories,
            filterExpanded: false
        };
    }

    componentDidMount() {
        const active = []
        this.state.categories.forEach(() => {
            active.push(false);
        })
        this.setState({active: active});
    }

    toggle = () => {
        this.setState({
            filterExpanded: !this.state.filterExpanded
        });
    }

    buttonPressed = (index) => {
        const activeButtons = this.state.active.map((val, tmpIndex) => {
            if (tmpIndex === index) {
                return !val;
            }
            return val;
        });
        this.setState({active: activeButtons});
        this.setFilteredNodes(activeButtons);
    }

    render() {
        return (
            <div>
                <button
                    onClick={this.toggle}>{this.state.filterExpanded ? "Filter verbergen" : "Filter anzeigen"}</button>
                <div>
                    {
                        this.state.filterExpanded && (
                            this.state.categories.map((categorie, index) => (
                                    <button key={index} style={this.state.active[index] ? {
                                        color: "#342854",
                                        textDecoration: "underline"
                                    } : {color: "grey"}}
                                            onClick={() => this.buttonPressed(index)}>{categorie.categoryName}</button>
                                )
                            )
                        )
                    }
                </div>
            </div>
        )
    }

    /**
     * Using selected buttons, we get the nodes in the selected categories and set them in the associated parameters.
     *
     * @param selectedButtons is an array that comes from filter.js and contains information about which buttons are selected
     */
    setFilteredNodes = (selectedButtons) => {
        const selectedCategories = this.getCategories(selectedButtons);
        const filteredProjects = getFilteredNodes(this.props.allProjects, selectedCategories);
        const filteredOrganisations = getFilteredNodes(this.props.allOrganisations, selectedCategories);
        const filteredProjectLinks = getFilteredLinks(filteredProjects, filteredOrganisations, this.props.allProjectLinks);
        const filteredInterviews = getFilteredInterviews(filteredOrganisations, this.props.allInterviewLinks, this.props.allInterviews);
        const filteredInterviewLinks = getFilteredLinks(filteredInterviews, filteredOrganisations, this.props.allInterviewLinks);
        this.props.filteredNodes(selectedCategories, filteredProjects, filteredOrganisations, filteredInterviewLinks, filteredProjectLinks, filteredInterviews);
    }

    /**
     * Compares two arrays with the same length and recognizes the selected categories.
     *
     * @param selectedButtons is an array that comes from filter.js and contains information about which buttons are selected
     * @returns the param which contains the names of the selected categories
     */
    getCategories = (selectedButtons) => {
        let selectedCategories = [];
        this.props.categories.forEach((categorie, indexCategory) => {
                selectedButtons.forEach((button, indexButton) => {
                    if (button === true && indexButton === indexCategory) {
                        selectedCategories.push(categorie.categoryName);
                    }
                })
            }
        )
        return selectedCategories;
    }
}

/**
 * A new array of nodes is created by filtering the selected categories and the categories in which a node is present using the category names.
 *
 * @param nodeData all fetched organisations or projects
 * @param selectedCategories an array with the names of the selected categories
 * @return returns an array with all nodes of the selected categories
 */
const getFilteredNodes = (nodeData, selectedCategories) => {
    return nodeData.filter(node => {
            for (const cat of node.categories) {
                const filtered = selectedCategories.includes(cat.categoryName)
                if (filtered) {
                    return true;
                }
            }
            return false;
        }
    )
}

/**
 * A new array with filtered connections is created.
 *
 * @param filteredProjects all filtered projects
 * @param filteredOrganisations all filtered organisations
 * @param nodeLinks all fetched links between the nodes
 * @return returns an array with all links between the filtered nodes
 */
const getFilteredLinks = (filteredProjects, filteredOrganisations, nodeLinks) => {
    const newNodeLinks = [];
    nodeLinks.forEach(link => {
        filteredProjects.forEach(project => {
            if (project.name === link.target.name) {
                filteredOrganisations.forEach(organisation => {
                    if (organisation.name === link.source.name) {
                        newNodeLinks.push(link);
                    }
                })
            }
        })
    })
    return newNodeLinks;
}

/**
 * A new array with interview nodes is created, with interviews that have connections to organisations.
 *
 * @param filteredOrganisations all filtered organisations
 * @param interviewLinks all fetched interviewLinks
 * @param allInterviews all fetched interviews
 * @return returns an array with all interviews which have connections to organisations
 */
const getFilteredInterviews = (filteredOrganisations, interviewLinks, allInterviews) => {
    let filteredInterviews = [];
    allInterviews.forEach(interview => {
        interviewLinks.forEach(interviewLink => {
            if (interviewLink.target.name === interview.name) {
                filteredOrganisations.forEach(organisation => {
                    if (organisation.name === interviewLink.source.name) {
                        filteredInterviews.push(interview);
                    }
                })
            }
        })
    })
    return filteredInterviews;
}