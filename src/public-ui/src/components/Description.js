import React from "react";
import {Accordion, Button, Card} from "react-bootstrap";
import '../styles/Description.css'

export default class Description extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false
        };
    }

    buttonPressed = () => {
        this.setState({active: !this.state.active});
    }

    render() {
        return (
            <Accordion defaultActiveKey="">
                <Card>
                    <Card.Header>
                        <Accordion.Toggle as={Button} variant="link" eventKey="0" onClick={() => this.buttonPressed()}>
                            <div className={'description-button'}>Was ist das Quernetz?
                                <div id={'description-arrow'} className={this.state.active
                                    ? 'description-arrow-active'
                                    : 'description-arrow'}> ->
                                </div>
                            </div>
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body>
                            Das Quernetz stellt eine bildliche Vernetzung unserer vergangenen
                            Projekte dar und ist
                            in Kooperation mit der HTWK entstanden. Ziel des Quernetzes ist es, unsere Kernaufgabe,
                            das „quernetzen“, also das Zusammenbringen von Akteuren:innen, die im gleichen Bereich
                            tätig sind, auch visuell zu präsentieren. Dabei werden Organisationen mit Projekten und
                            Interviews verknüpft und über Hashtags den Bereichen „nachhaltig“ oder „sozial“
                            zugeordnet.
                            Somit kann der Nutzer:innen sich ein Überblick verschaffen, welche Organisationen in
                            Leipzig in welchen Bereichen wie arbeiten.
                            Du hast Vorschläge, wie wir unser Quernetz weiter ausbauen können? Dann schreib uns
                            unter: die-quernetzer@gmx.de.
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        )
    }
}