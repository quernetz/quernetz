import React from "react";
import * as d3 from 'd3';
import ProjectNode from "./ProjectNode/ProjectNode";
import OrganisationNode from "./OrganisationNode/OrganisationNode";
import InterviewNode from "./InterviewNode/InterviewNode";
import {extractIdFromSelf} from "./helper/extractIdFromSelf";
import Legend from "./Legend";

export default class Graph extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.projects.length !== this.props.projects.length ||
            prevProps.organisations.length !== this.props.organisations.length ||
            prevProps.interviews.length !== this.props.interviews.length) {
            this.renderGraph();
        }
    }

    componentDidMount() {
        this.renderGraph();
    }

    componentWillUnmount() {
        this.force.stop();
    }

    render() {
        return (
            <svg width={this.props.width} height={this.props.height}>
                {this.props.links.map((link, index) => (
                    <line
                        x1={link.source.x}
                        y1={link.source.y}
                        x2={link.target.x}
                        y2={link.target.y}
                        key={`line-${index}`}
                        stroke="black"
                        opacity={link.transparent ? .1 : 1}
                    />
                ))}
                {this.props.projects.map((project, index) => (
                        <ProjectNode key={index} project={project}
                                     hover={this.state.hover}
                                     visibleNodesOnHover={this.visibleNodesOnHover}/>
                    )
                )}
                {this.props.organisations.map((organisation, index) => (
                        <OrganisationNode key={index} organisation={organisation}
                                          hover={this.state.hover}
                                          visibleNodesOnHover={this.visibleNodesOnHover}/>
                    )
                )}
                {this.props.interviews.map((interview, index) => (
                        <InterviewNode key={index} interview={interview}
                                       hover={this.state.hover}
                                       visibleNodesOnHover={this.visibleNodesOnHover}/>
                    )
                )}
                <Legend/>
            </svg>)
    }

    renderGraph = () => {
        const dataForLinks = this.props.projects.concat(this.props.organisations, this.props.interviews);
        /*
         * React has as example 10 nodes, to set the edges we use the IDs from the entity's. But d3 use the index of the nodes.
         * If we have IDs that are larger than the index, we have to adjust these IDs to the respective index of the entity. This is what these loops do.
         */
        dataForLinks.forEach((data, i) => {
            this.props.links.forEach((link) => {
                const id = extractIdFromSelf(data._links.self.href)
                if (link.target === id) {
                    link.target = i;
                }
                if (link.source === id) {
                    link.source = i;
                }
            })
        })

        this.force = d3.forceSimulation(this.props.projects.concat(this.props.organisations, this.props.interviews))
            .force("charge", d3.forceManyBody().strength(this.props.forceStrength))
            .force("link", d3.forceLink().distance(this.props.linkDistance).links(this.props.links))
            .force("x", d3.forceX(this.props.width / 2))
            .force("y", d3.forceY(this.props.height / 2))
            .force("collision", d3.forceCollide(10).strength(0.2))
            //Graph builds from the middle of the svg:
            .force("center", d3.forceCenter(this.props.width / 2, this.props.height / 2));
        //default Value is 300. The value that you insert will be removed : 300-299 = 1
        // if you want the graph to do a Spinning entry sett it 300 by giving him the value 0 or removing .tick(<value>)
        //.tick (299);

        this.force.on('tick', () => this.setState({
            links: this.props.links,
            projects: this.props.projects,
            organisations: this.props.organisations,
            interviews: this.props.interviews
        }));
    }

    visibleNodesOnHover = (isHovered, hoveredNodeId) => {
        const newProjects = [...this.props.projects];
        const newOrganisations = [...this.props.organisations];
        const newInterviews = [...this.props.interviews];
        const connectedNodes = [];
        const newLinks = [];
        const newInterviewLinks = [...this.props.interviewLinks];
        const newProjectLinks = [...this.props.projectLinks];
        const nodes = this.props.projects.concat(this.props.organisations, this.props.interviews);
        const links = this.props.interviewLinks.concat(this.props.projectLinks);

        /*
         * Here we get the connected nodes and the lines in between of the hovered node. Then we change the opacity of the other nodes and lines to false.
         */
        // If isHovered = true we go to take the connected nodes and lines with the help of the hoveredNodeId and send the element via callback to app.js.
        // If isHovered = false we set the transparent prop of all nodes and lines to true and send the elements via callback to the app.js.
        if (isHovered) {
            links.forEach((link) => {
                const idTarget = extractIdFromSelf(link.target._links.self.href);
                const idSource = extractIdFromSelf(link.source._links.self.href);
                //in this case we test if the hovered Node is a target or source of the line
                //the -1 condition is necessary to avoid always reloading the same element into the arrays
                if (idTarget === hoveredNodeId || idSource === hoveredNodeId) {
                    if (connectedNodes.indexOf(link.target) === -1) {
                        connectedNodes.push(link.target);
                        newLinks.push(link);
                    }
                    if (connectedNodes.indexOf(link.source) === -1) {
                        connectedNodes.push(link.source);
                        newLinks.push(link);
                    }
                }
            });
            // here we check the case if the hovered Node isn't connected with any other node
            if (connectedNodes.length === 0) {
                nodes.forEach((node) => {
                    if (extractIdFromSelf(node._links.self.href) === hoveredNodeId) {
                        connectedNodes.push(node);
                    }
                })
            }
            changePropertiesWhenHovering(newOrganisations, connectedNodes);
            changePropertiesWhenHovering(newProjects, connectedNodes);
            changePropertiesWhenHovering(newInterviews, connectedNodes);
            changePropertiesWhenHovering(newInterviewLinks, newLinks);
            changePropertiesWhenHovering(newProjectLinks, newLinks);

        } else {
            setHoverPropsToFalse(newOrganisations);
            setHoverPropsToFalse(newProjects);
            setHoverPropsToFalse(newInterviews);
            setHoverPropsToFalse(newInterviewLinks);
            setHoverPropsToFalse(newProjectLinks);
        }
        this.props.getHoveredNodes(newProjects, newInterviews, newOrganisations, newInterviewLinks, newProjectLinks);
    }
}

/**
 * Checks for each element of 'nodesToChange' if it is in 'connectedNodes', if yes => set transparent to false and enlarge to true, else to true and false.
 *
 * @param nodesToChange is an array with all element to check (organisations, projects, lines....)
 * @param connectedNodes is an array with the connected elements of the hovered node
 */

const changePropertiesWhenHovering = (nodesToChange, connectedNodes) => {
    nodesToChange.forEach(node => {
        node.transparent = !connectedNodes.includes(node);
        node.enlarged = connectedNodes.includes(node);
    })
}

const setHoverPropsToFalse = (objectsToChange) => {
    objectsToChange.forEach(obj => obj.transparent = false);
    objectsToChange.forEach(obj => obj.enlarged = false);
}

if (window.matchMedia("(max-width: 700px)").matches) {
    Graph.defaultProps = {
        width: 360,
        height: 500,
        linkDistance: 50,
        forceStrength: -40
    }
} else if (window.matchMedia("(max-width: 1050px)").matches && window.matchMedia("(max-height: 800px)").matches) {
    Graph.defaultProps = {
        width: 1024,
        height: 600,
        linkDistance: 150,
        forceStrength: -100
    }
} else if (window.matchMedia("(max-width: 770px)").matches && window.matchMedia("(max-height: 1050px)").matches) {
    Graph.defaultProps = {
        width: 770,
        height: 800,
        linkDistance: 50,
        forceStrength: -200
    }
} else {
    Graph.defaultProps = {
        width: 2000,
        height: 1000,
        linkDistance: 50,
        forceStrength: -250
    }
}