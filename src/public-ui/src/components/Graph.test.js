import Graph from "./Graph";
import React from "react";
import {mount, shallow} from "enzyme";
import App from "../App";

describe('<Graph />', () => {

    const projects = [
        {
            id: 0,
            name: 'Erstes Projekt',
            description: 'Das war unser erstes Projekt',
            from: '2019, 01, 01',
            to: '2019, 01, 02',
            year: '2019',
            sectors: ["Umwelt", "Naturschutz"],
            _links: {self: {href: "http://localhost:8080/api/projects/0"}}
        },
        {
            id: 1,
            name: 'Neues Projekt',
            description: 'Das ist unser neues Projekt',
            from: '2018, 03, 14',
            to: '2020, 01, 02',
            year: '2019',
            sectors: ["Soziales", "Umwelt"],
            _links: {self: {href: "http://localhost:8080/api/projects/1"}}
        },
        {
            id: 2,
            name: 'Drittes Projekt',
            description: 'Das war unser drittes Projekt',
            from: '2010, 02, 10',
            to: '2012, 02, 01',
            year: '2018',
            sectors: ["Umwelt"],
            _links: {self: {href: "http://localhost:8080/api/projects/2"}}
        },
        {
            id: 3,
            name: 'Viertes Projekt',
            description: 'Das war unser coolstes Projekt',
            from: '2010, 02, 10',
            to: '2012, 02, 01',
            year: '2018',
            sectors: ["Umwelt"],
            _links: {self: {href: "http://localhost:8080/api/projects/3"}}
        },
        {
            id: 4,
            name: 'Nächstes Projekt',
            description: 'Projekt im Mai',
            from: '2020, 12, 10',
            to: '2021, 10, 02',
            year: '2019',
            sectors: ["Umwelt", "Soziales"],
            _links: {self: {href: "http://localhost:8080/api/projects/4"}}
        },
    ];

    const organisations = [
        {
            id: 5,
            name: 'Erste Organisation',
            description: 'Das ist eine Organisation',
            _links: {self: {href: "http://localhost:8080/api/organisations/5"}}
        },
        {
            id: 6,
            name: 'Erste Organisation',
            description: 'Das ist eine Organisation',
            _links: {self: {href: "http://localhost:8080/api/organisations/6"}}
        }
    ];

    const interviews = [
        {
            id: 7,
            name: 'Erste Organisation',
            description: 'Das ist eine Organisation',
            _links: {self: {href: "http://localhost:8080/api/interviews/7"}}
        },
        {
            id: 8,
            name: 'Erste Organisation',
            description: 'Das ist eine Organisation',
            _links: {self: {href: "http://localhost:8080/api/interviews/8"}}
        },
        {
            id: 9,
            name: 'Erste Organisation',
            description: 'Das ist eine Organisation',
            _links: {self: {href: "http://localhost:8080/api/interviews/9"}}
        }
    ];

    const interviewLinks = [
        {
            source: 5,
            target: 7
        },
        {
            source: 5,
            target: 8
        },
        {
            source: 1,
            target: 2
        }
    ];

    const projectLinks = [
        {
            source: 6,
            target: 4
        },
        {
            source: 6,
            target: 2
        },
        {
            source: 5,
            target: 0
        },
        {
            source: 5,
            target: 1
        }
    ];

    const categories = [
        {
            categoryName: "Umwelt"
        },
        {
            categoryName: "Müll"
        }
    ];

    test('snapshot', () => {
        const tree = shallow(<Graph projects={projects}
                                    organisations={organisations}
                                    links={projectLinks.concat(interviewLinks)}
                                    interviews={interviews} categories={categories}/>);
        expect(tree).toMatchSnapshot();
    })

    it('test hover feature', () => {
        const app = mount((<App/>));
        app.setState({
            projects: projects,
            organisations: organisations,
            interviews: interviews,
            projectLinks: projectLinks,
            interviewLinks: interviewLinks,
            isDataFetched: true
        });

        const graph = app.find('Graph');
        const projectNode = graph.find('ProjectNode').filterWhere((node) => node.props().project.id === 0);
        const circle = projectNode.find('circle');
        expect(projectNode.props().project.id).toEqual(0);
        //now we hover over projectNode with id 0 and 0 ist connected with organisationNode 5

        circle.simulate('mouseenter');

        // 0 and 5 are visible and everything else need to be transparent
        expect(graph.find('ProjectNode').filterWhere((node) => node.props().project.id === 0).props().project.transparent).toEqual(false);
        expect(graph.find('OrganisationNode').filterWhere((node) => node.props().organisation.id === 5).props().organisation.transparent).toEqual(false);
        expect(graph.find('OrganisationNode').filterWhere((node) => node.props().organisation.id === 6).props().organisation.transparent).toEqual(true);
        graph.find('InterviewNode').forEach((node) => {
            expect(node.props().interview.transparent).toEqual(true);
        });
        graph.find('ProjectNode').forEach((node) => {
            if (node.props().project.id !== 0) {
                expect(node.props().project.transparent).toEqual(true);
            }
        });

        //now we leave this node and everything is false

        circle.simulate('mouseleave');

        graph.find('ProjectNode').forEach((node) => {
            expect(node.props().project.transparent).toEqual(false);
        });
        graph.find('OrganisationNode').forEach((node) => {
            expect(node.props().organisation.transparent).toEqual(false);
        });
        graph.find('InterviewNode').forEach((node) => {
            expect(node.props().interview.transparent).toEqual(false);
        });
    });
});