import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import InterviewNode from "./InterviewNode";

const interview =
    {
        id: 0,
        name: 'Erstes Interview',
        description: 'Das war unser erstes Interview',
        date: '2020, 04, 01',
        link: ["https://www.duckduckgo.com"],
        _links: {self: {href: "http://localhost:8080/api/interviews/0"}}
    };

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<svg><InterviewNode interview={interview}/></svg>, div)
});

describe("InterviewNode.reformatYoutubeLink()", () => {
    const wrapper = shallow(<InterviewNode interview={interview} />);
    const instance = wrapper.instance();

    test("Pattern: youtube.com/watch?v=videoID", () => {
        expect(instance.reformatYoutubeLink("https://m.youtube.com/watch?list=UL&hd=1&autoplay=1&v=4KU4EStjrUE&hl=en-US")).toBe("https://www.youtube-nocookie.com/embed/4KU4EStjrUE");
    });

    test("Pattern: youtube.com/v/videoID", () => {
        expect(instance.reformatYoutubeLink("http://youtube.com/v/u1yz6V1Q7wk?hd=1&t=15")).toBe("https://www.youtube-nocookie.com/embed/u1yz6V1Q7wk?t=15");
    });

    test("Pattern: youtube.com/embed/videoID", () => {
        expect(instance.reformatYoutubeLink("https://www.youtube.com/embed/Yo7uXaV6Q1s?list=UL")).toBe("https://www.youtube-nocookie.com/embed/Yo7uXaV6Q1s");
    });

    test("Pattern: youtu.be/videoID", () => {
        expect(instance.reformatYoutubeLink("https://youtu.be/Zp3oEJemnz0?t=675&rel=1")).toBe("https://www.youtube-nocookie.com/embed/Zp3oEJemnz0?t=675");
    });

    test("Pattern: youtube.com/watch? without v=videoID", () => {
        const url = "https://www.youtube.com/watch?list=UL&hd=1&autoplay=1&hl=en-US";

        expect(instance.reformatYoutubeLink(url)).toBe(url);
    });

    test("Pattern mismatch", () => {
        const url = "https://fim.htwk-leipzig.de/de/"

        expect(instance.reformatYoutubeLink(url)).toBe(url);
    });
});
