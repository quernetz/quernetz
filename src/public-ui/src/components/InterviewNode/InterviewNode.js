import React from "react";
import {Fragment} from 'react'
import GenericPopUp from "../GenericPopUp";
import {extractIdFromSelf} from "../helper/extractIdFromSelf";
import {Tooltip, OverlayTrigger} from "react-bootstrap";
import Small from "../FontSize/Small";
import {convertToGermanDateString} from "../../utils/dateFormatting";
import "./InterviewModal.css";

export default class InterviewNode extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
        };
    }

    handleClick = () => {
        this.setState({showModal: !this.state.showModal});
    };

    onMouseOver = (e) => {
        this.props.visibleNodesOnHover(true, e.target.id)
    };

    onMouseLeave = () => {
        this.props.visibleNodesOnHover(false)
    };

    /**
     * Reformat an URL pointing to a Youtube video to be embeddable
     *
     * Youtube links are commonly shared in different flavors, but to embed
     * video content on websites, Youtube is very strict on the URL. It only
     * accepts URLs to the endpoint pattern "youtube.com/embed/".
     * The following Youtube URL patterns are parsed:
     *  - youtube.com/watch?v=videoID
     *  - youtube.com/v/videoID
     *  - youtube.com/embed/videoID
     *  - youtu.be/videoID
     * A start time argument ("t=") will be passed to the returned URL
     *
     * @param {string} input URL
     * @returns {string} URL to embeddable Youtube video if a recognizable
     *                   Youtube URL was parsed, otherwise the input string
     *                   as fallback.
     */
    reformatYoutubeLink = (input) => {
        const youtubeComWatch = 'youtube.com/watch?';
        const youtubeComV = 'youtube.com/v/';
        const youtubeComEmbed = 'youtube.com/embed/';
        const youtuBe = 'youtu.be/';
        let args, vid;
        let url = "https://www.youtube-nocookie.com/embed/";

        try {
            if (input.toLowerCase().includes(youtubeComWatch)) {
                // separate arguments from domain name
                args = input.slice(input.indexOf(youtubeComWatch) + youtubeComWatch.length);
                // get video ID
                const viewArg = "v=";
                const viewIndex = Math.max(args.indexOf("?" + viewArg), args.indexOf("&" + viewArg));

                if (viewIndex < 0) {
                    throw new Error("No 'v=' argument found in URL.");
                }

                vid = args.slice(viewIndex + viewArg.length + 1).split("&")[0];
            } else if (input.toLowerCase().includes(youtubeComV)) {
                // separate arguments from domain name
                args = input.slice(input.indexOf(youtubeComV) + youtubeComV.length);
                vid = args.split("?")[0];
            } else if (input.toLowerCase().includes(youtubeComEmbed)) {
                // separate arguments from domain name
                args = input.slice(input.indexOf(youtubeComEmbed) + youtubeComEmbed.length);
                vid = args.split("?")[0];
            } else if (input.toLowerCase().includes(youtuBe)) {
                // separate arguments from domain name
                args = input.slice(input.indexOf(youtuBe) + youtuBe.length);
                vid = args.split("?")[0];
            } else {
                throw new Error("No valid Youtube URL pattern found.");
            }

            url += vid;

            const timeArg = "t=";
            const timeIndex = Math.max(args.indexOf("?" + timeArg), args.indexOf("&" + timeArg));

            if (timeIndex >= 0) {
                url += "?" + args.slice(timeIndex + 1).split("&")[0];
            }

            return url;
        } catch (error) {
            console.log("InterviewNode.js: youtubeLink(): " + error.message);

            return input;
        }
    };

    render() {
        return (
            this.props.interview.x !== undefined &&
            <Fragment>
                <OverlayTrigger key={this.props.interview.index}
                                placement="right"
                                delay={{show: 250, hide: 400}}
                                overlay={<Tooltip id="button-tooltip">
                                    {this.props.interview.name}
                                </Tooltip>}
                >
                    <path strokeWidth="2"
                          stroke={"black"}
                        //adjustment to the x coordinate are to middle nodes on their graph
                          d={this.props.interview.enlarged ? `M ${this.props.interview.enlarged ? this.props.interview.x - 17.5 : this.props.interview.x - 12.5} ${this.props.interview.y} l17.5 17.5 l17.5 -17.5 l-17.5 -17.5Z ` : `M ${this.props.interview.x - 12.5} ${this.props.interview.y} l12.5 12.5 l12.5 -12.5 l-12.5 -12.5Z `}
                          fill={"lightyellow"}
                          opacity={this.props.interview.transparent ? .1 : 1}
                          id={extractIdFromSelf(this.props.interview._links.self.href)}
                          onClick={this.handleClick}
                          onMouseEnter={this.onMouseOver}
                          onMouseLeave={this.onMouseLeave}
                          cursor={"pointer"}
                    />
                </OverlayTrigger>
                {this.state.showModal ?
                    <GenericPopUp showModal={this.state.showModal} popupName={this.props.interview.name}
                                  handleClick={this.handleClick}>
                        <div className="container" style={{color: "#333366"}}>
                            {/* Description of the Interview*/}
                            <Small>{this.props.interview.description}</Small>
                            {/* Youtube-Iframe of the Interview */}
                            <div className="iframe-container">
                                <iframe title={this.props.interview.name} width="420" height="315"
                                        src={this.reformatYoutubeLink(this.props.interview.youtubeLink)}>
                                </iframe>
                            </div>
                            <svg height="5" width="200">
                                <line x1="0" y1="0" x2="200" y2="0" style={{stroke: "grey", strokeWidth: "1"}}/>
                            </svg>
                            {/* Shows Date of the Interview if existing*/}
                            {this.props.interview.date ? (
                                <Small>{convertToGermanDateString(this.props.interview.date)}</Small>
                            ) : null}
                            {/* Shows Link if existing*/}
                            {this.props.interview.link ? (
                                <Small>
                                    <a style={{color: "#00987a"}} href={this.props.interview.link}>Mehr
                                        Informationen</a>
                                </Small>
                            ) : null}
                        </div>
                    </GenericPopUp>
                    : null}
            </Fragment>
        );
    }
}