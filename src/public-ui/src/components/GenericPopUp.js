import React from "react";
import {Modal} from 'react-bootstrap';
import '../styles/GenericPopUp.css';

/*The popup is displayed by clicking on another component, the truth value and the title are transferred as props.
 *The content for the PopUp is transferred using the container div.
 *As an example
 *<GenericPopUp>
 *<div className = "container">
 *... Amazing Content ...
 *</div>
 *</GenericPopUp>
 */


export default class GenericPopUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: props.showModal,
        }
    }

    render() {
        const {showModal, handleClick, projectData, projectName, popupName, ...rest} = this.props;
        return (
            <Modal show={this.state.showModal} onHide={this.props.handleClick}
                   {...rest}
                   size="lg"
                   aria-labelledby="contained-modal-title-vcenter"
                   centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter" style={{
                        color: "white",
                        textTransform: 'uppercase',
                    }}>
                        {this.props.popupName}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="container" style={{color: "#333366"}}>
                        {this.props.children}
                    </div>
                </Modal.Body>
            </Modal>
        );
    }
}
