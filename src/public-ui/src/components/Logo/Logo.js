import React, {Component} from 'react';
import fetchLogo from "../../data-access/fetchLogo";

class Logo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            organisation: props.organisation,
            logo: null
        };
    }

    componentDidMount = async () => {
        const image = await fetchLogo(this.props.organisation);
        this.setState({logo: image})
    };

    render() {
        return (
            <div>
                <img src={this.state.logo} alt="" style={{'maxWidth': '150px', 'height': 'auto', 'marginRight':'10px'}}/>
            </div>
        );
    }
}

export default Logo;