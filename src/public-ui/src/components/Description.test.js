import React from 'react'
import ReactDOM from 'react-dom'
import Description from "./Description";
import {mount} from "enzyme";
import {Accordion, Button} from "react-bootstrap";
import {expect} from 'chai';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Description/>, div)
});

describe('Description component', () => {
    it('should be present', () => {
        const componentUnderTest = mount(<Description/>);
        expect(componentUnderTest.find(Accordion)).to.exist
        expect(componentUnderTest.find(Button)).to.exist
    });

    it('should display text and change style by clicking', () => {
        const componentUnderTest = mount(<Description/>);
        expect(componentUnderTest.find(Button).text()).to.have.equal('Was ist das Quernetz? ->')
        expect(componentUnderTest.find('#description-arrow').hasClass('description-arrow')).to.equal(true);
        componentUnderTest.find('#description-arrow').simulate('click');
        expect(componentUnderTest.find('#description-arrow').hasClass('description-arrow-active')).to.equal(true);
    });
});