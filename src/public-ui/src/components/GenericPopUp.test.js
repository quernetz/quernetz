import React from 'react'
import ReactDOM from 'react-dom'
import project from './ProjectNode/ProjectNode'
import GenericPopUp from "./GenericPopUp";

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<svg><GenericPopUp project={project}/></svg>, div)
});