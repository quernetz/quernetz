import React from "react";
//Component for representing projects, organizations and interviews as nodes.
//The data should come from ProjectNode, OrganizationNode and InterviewNode.
/*
    author: Max Karehnke
    generates a SVG in the Display SVG where the Graph is
 */

export default class Legend extends React.Component {

    render() {
        return(
            <svg height={600} width={800}>
                {(window.matchMedia("(max-width: 700px)").matches) ?
                    <g transform="translate(-12 400)">
                        <circle
                            r={10}
                            cx={28}
                            cy={70}
                            fill={"#00977A"}
                            strokeWidth="2"
                            stroke={"black"}
                            opacity={1}
                            id={1}
                        />
                        <text x={43} y={76} style={{fontSize: 17}}>Projekt</text>
                        <rect
                            height={16}
                            width={16}
                            x={112}
                            y={62}
                            fill={"#333366"}
                            strokeWidth="2"
                            stroke={"black"}
                            opacity={1}
                            id={1}
                        />
                        <text x={138} y={76} style={{fontSize: 17}}>Organisation</text>
                        <path transform="translate(220, -105)"
                            // creates a Rhombus
                              d="M28 175 l12.5 12.5 l12.5 -12.5 l-12.5 -12.5Z"
                              fill={"lightyellow"}
                              strokeWidth="2"
                              stroke={"black"}
                              opacity={1}
                              id={1}
                        />
                        <text x={278} y={76} style={{fontSize: 17}}>Interview</text>
                    </g> :
                    <g>
                    <circle
                        r={10}
                        cx={40}
                        cy={70}
                        fill={"#00977A"}
                        strokeWidth="2"
                        stroke={"black"}
                        opacity={1}
                        id={1}
                    />
                    <text x={70} y={76} style={{fontSize:17}}>Projekt</text>
                    <rect
                    height={16}
                    width={16}
                    x={32}
                    y={115}
                    fill={"#333366"}
                    strokeWidth="2"
                    stroke={"black"}
                    opacity={1}
                    id={1}
                    />
                    <text x={70} y={129} style={{fontSize:17}}>Organisation</text>
                    <path
                    // creates a Rhombus
                    d="M28 175 l12.5 12.5 l12.5 -12.5 l-12.5 -12.5Z"
                    fill={"lightyellow"}
                    strokeWidth="2"
                    stroke={"black"}
                    opacity={1}
                    id={1}
                    />
                    <text x={70} y={182} style={{fontSize:17}}>Interview</text>
                    </g>
                }
                    </svg>
        )
    }
}