import React from 'react'
import ReactDOM from 'react-dom'
import Legend from "./Legend";

/*
    author: Max Karehnke
    Does the Legend.js render?
 */

it('renders without crashing', () => {
    const svg = document.createElement('svg');
    ReactDOM.render(<Legend/>, svg)
});