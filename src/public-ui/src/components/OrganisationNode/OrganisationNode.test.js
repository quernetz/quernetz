import React from 'react'
import ReactDOM from 'react-dom'
import OrganisationNode from "./OrganisationNode";

const organisation =
    {
        id: 5,
        name: 'Erste Organisation',
        description: 'Das ist eine Organisation',
        _links: {self: {href: "http://localhost:8080/api/organisations/5"}}
    };

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<svg><OrganisationNode organisation={organisation}/>
    </svg>, div)
});