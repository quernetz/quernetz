import React from "react";
import {Fragment} from 'react'
import GenericPopUp from "../GenericPopUp";
import {extractIdFromSelf} from "../helper/extractIdFromSelf";
import {Tooltip, OverlayTrigger} from "react-bootstrap";
import Logo from "../Logo/Logo";
import Small from "../FontSize/Small";
import "./OrganisationModal.css";


export default class OrganisationNode extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            organisation: props.organisation,
            showModal: false,
            tags: []
        };
    }

    handleClick = () => {
        this.setState({showModal: !this.state.showModal});

    };
    onMouseOver = (e) => {
        this.props.visibleNodesOnHover(true, e.target.id)
    };

    onMouseLeave = () => {
        this.props.visibleNodesOnHover(false)
    };

    render() {
        return (
            this.props.organisation.x !== undefined &&
            <Fragment>
                <OverlayTrigger key={this.props.organisation.index}
                                placement="right"
                                delay={{show: 250, hide: 400}}
                                overlay={<Tooltip id="button-tooltip">
                                    {this.props.organisation.name}
                                </Tooltip>}
                >
                    <rect strokeWidth="2"
                          stroke={"black"}
                          height={this.props.organisation.enlarged ? 25 : 16}
                          width={this.props.organisation.enlarged ? 25 : 16}
                          x={this.props.organisation.enlarged ? this.props.organisation.x - 12.0 : this.props.organisation.x - 8.0}
                          y={this.props.organisation.enlarged ? this.props.organisation.y - 12.0 : this.props.organisation.y - 8.0}
                          fill={"#333366"}
                          opacity={this.props.organisation.transparent ? .1 : 1}
                          id={extractIdFromSelf(this.props.organisation._links.self.href)}
                          onClick={this.handleClick}
                          onMouseEnter={this.onMouseOver}
                          onMouseLeave={this.onMouseLeave}
                          cursor={"pointer"}
                    />
                </OverlayTrigger>
                {this.state.showModal ?
                    <GenericPopUp showModal={this.state.showModal} popupName={this.props.organisation.name}
                                  handleClick={this.handleClick}>
                        <div className="container" style={{color: "#333366"}}>
                            {/* Description of the Organisation*/}
                            <div style={{float: "left"}}><Logo organisation={this.props.organisation}/></div>
                            <div><Small>{this.props.organisation.description}</Small></div>
                            <svg height="5" width="200">
                                <line x1="0" y1="0" x2="200" y2="0" style={{stroke: "grey", strokeWidth: "1"}}/>
                            </svg>
                            {/* All Categories of the Organisation: */}
                            <ul className="categories">
                                {this.props.organisation.categories.map(category => (
                                    <li key={category._links.self.href}>
                                        <div style={{padding: "3px"}}>
                                            <p>{category.categoryName}</p>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                            {/* All #Tags of the Organisation: */}
                            <ul className="tags">
                                {this.props.organisation.tags.map(tag => (
                                    <li key={tag._links.self.href}>
                                        <div style={{padding: "3px"}}>
                                            <p>{tag.text}</p>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                            {/* Shows Link if existing*/}
                            {this.props.organisation.website ? (
                                <div><a style={{color: "#00987a"}} href={this.props.organisation.website}>Mehr
                                    Informationen</a></div>
                            ) : null}
                        </div>
                    </GenericPopUp>
                    : null}
            </Fragment>
        );
    }
}