import React from 'react';
import "./FontSize.css";


class Large extends React.Component{

    render() {
        return (
            <div className="FontSize">
                <p className="large"> {this.props.children}</p>
            </div>
        );
    }

}

export default Large;