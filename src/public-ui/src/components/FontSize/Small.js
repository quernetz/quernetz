import React from 'react';
import "./FontSize.css";


 class Small extends React.Component{

    render() {
        return (
            <div className="FontSize">
                <p className="small"> {this.props.children}</p>
            </div>
        );
    }

}

export default Small;