import React from 'react';
import "./FontSize.css";


class Medium extends React.Component{

    render() {
        return (
            <div className="FontSize">
                <p className="medium"> {this.props.children}</p>
            </div>
        );
    }

}

export default Medium;