import React from 'react';
import './App.css';
import Graph from "./components/Graph";
import fetchData from "./data-access/fetchData";
import BackendUrl from "./data-access/backendconfig";
import {extractIdFromSelf} from "./components/helper/extractIdFromSelf";
import Filter from "./components/Filter";
import Description from "./components/Description";

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isDataFetched: false,
            projects: [],
            organisations: [],
            interviews: [],
            interviewLinks: [],
            projectLinks: [],
            categories: [],
            selectedCategories: []
        }
    }

    componentDidMount = async () => {
        if (this.state.isDataFetched) {
            return;
        }

        const [projects, interviews, organisations, categories] = await Promise.all([
            fetchData(BackendUrl() + '/api/projects?projection=withTagsCategoriesOrganisationsAndInterviews'),
            fetchData(BackendUrl() + '/api/interviews'),
            fetchData(BackendUrl() + '/api/organisations?projection=withTagsCategoriesProjectsAndInterviews'),
            fetchData(BackendUrl() + '/api/categories')
        ]);
        const organisationInterviewLinks = await getOrganisationsAssociations(organisations, 'interviews');
        const organisationProjectLinks = await getOrganisationsAssociations(organisations, 'projects');
        projects._embedded.projects.forEach((p) => {
            p.transparent = false
            p.enlarged = false
        })
        interviews._embedded.interviews.forEach((i) => {
            i.transparent = false
            i.enlarged = false
        })
        organisations._embedded.organisations.forEach((o) => {
            o.transparent = false
            o.enlarged = false
        })
        organisations._embedded.organisations.forEach((o) => {
            o.transparent = false
            o.enlarged = false
        })
        organisationInterviewLinks.forEach(o => o.transparent = false)
        organisationProjectLinks.forEach(o => o.transparent = false)
        this.setState({
            isDataFetched: true,
            projects: projects._embedded.projects,
            allProjects: projects._embedded.projects,
            interviews: interviews._embedded.interviews,
            allInterviews: interviews._embedded.interviews,
            organisations: organisations._embedded.organisations,
            allOrganisations: organisations._embedded.organisations,
            interviewLinks: organisationInterviewLinks,
            allInterviewLinks: organisationInterviewLinks,
            projectLinks: organisationProjectLinks,
            allProjectLinks: organisationProjectLinks,
            categories: categories._embedded.categories,
        })
    };

    render() {
        return (
            this.state.isDataFetched &&
            <div className="App">
                <h1 style={{color: "#00987a"}}>Quernetz</h1>
                <Filter categories={this.state.categories} allProjects={this.state.allProjects}
                        allInterviews={this.state.allInterviews} allOrganisations={this.state.allOrganisations}
                        allProjectLinks={this.state.allProjectLinks} allInterviewLinks={this.state.allInterviewLinks}
                        filteredNodes={this.setFilteredNodes}/>
                <Graph projects={this.state.projects} organisations={this.state.organisations}
                       links={this.state.interviewLinks.concat(this.state.projectLinks)}
                       interviews={this.state.interviews} getHoveredNodes={this.getHoveredNodes}
                       interviewLinks={this.state.interviewLinks}
                       projectLinks={this.state.projectLinks}/>
                <Description/>
            </div>

        )
    }

    setFilteredNodes = (selectedCategories, filteredProjects, filteredOrganisations, filteredInterviewLinks, filteredProjectLinks, filteredInterviews) => {
        if (selectedCategories.length > 0) {
            this.setState({
                projects: filteredProjects,
                organisations: filteredOrganisations,
                interviewLinks: filteredInterviewLinks,
                projectLinks: filteredProjectLinks,
                interviews: filteredInterviews
            })
        } else {
            this.setState({
                projects: this.state.allProjects,
                interviewLinks: this.state.allInterviewLinks,
                projectLinks: this.state.allProjectLinks,
                interviews: this.state.allInterviews,
                organisations: this.state.allOrganisations
            })
        }
    }

    getHoveredNodes = (newProjects, newInterviews, newOrganisations, newInterviewLinks, newProjectLinks) => {
        this.setState({
            projects: newProjects,
            interviews: newInterviews,
            organisations: newOrganisations,
            interviewLinks: newInterviewLinks,
            projectLinks: newProjectLinks
        })
    }
}

/**
 * Collect the IDs of all associated objects of the given type from all organisations.
 *
 * @param organisations - all fetched organisations
 * @param typeOfAssociatedObject - the type of the associated object, e.g.
 *                              * project
 *                              * interview
 * @returns {Promise<[]>} a promise which on success delivers an array with objects containing:
 *                          * source: id of the organisation
 *                          * target: id of the associated object
 */
const getOrganisationsAssociations = async (organisations, typeOfAssociatedObject) => {
    const associatedObjectIdsOfOrganisations = [];
    for (const organisation of organisations._embedded.organisations) {
        const associationsArray = organisation[typeOfAssociatedObject];
        if (associationsArray && associationsArray.length > 0) {
            associationsArray.forEach(associatedObject => {
                associatedObjectIdsOfOrganisations.push({
                    'source': extractIdFromSelf(organisation._links.self.href),
                    'target': extractIdFromSelf(associatedObject._links.self.href)
                });
            });
        }
    }
    return associatedObjectIdsOfOrganisations;
}