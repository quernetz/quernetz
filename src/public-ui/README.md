# React.js Frontend-Komponente 

### Befehle

### `npm start`

Startet die Applikation im Entwicklungsmodus.<br />
[http://localhost:3000](http://localhost:3000) im Browser öffnen, um die Webapp zu sehen.

Bei Änderungen im Code erfolgt ein Live-Reload, so dass der React-Server nicht jedes mal neugestartet werden muss.<br />
Ein Blick in die Browser-Konsole zeigt Fehler und Warnungen an (öffnet sich in Chrome & Firefox z.B. mit F12).

Empfehlenswert ist zudem die Installation der React Developer Tools im Browser:
* [Chrome](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)
* [Firefox](https://addons.mozilla.org/de/firefox/addon/react-devtools/)

-------------------
### `npm test`

Startet Tests.<br />

------------------
### `npm run build`

Startet einen Build für das Erzeugen produktionsreifer Dateien im build-Verzeichnis.<br />
Dies optimiert die React-App für den produktiven Betrieb und stellt die beste Performance bereit.
Zudem werden die Quellcode-Dateien minifiziert.
## React Dokumentation

* [Offizielle React Dokumentation](https://reactjs.org/)
* [Tests](https://facebook.github.io/create-react-app/docs/running-tests)
* [Deployment](https://facebook.github.io/create-react-app/docs/deployment)

