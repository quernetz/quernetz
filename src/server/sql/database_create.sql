
    create table category (
       id bigint not null,
        category_name varchar(255),
        primary key (id)
    ) engine=MyISAM;

    create table category_organisation (
       category_id bigint not null,
        organisation_id bigint not null,
        primary key (category_id, organisation_id)
    ) engine=MyISAM;

    create table category_project (
       category_id bigint not null,
        project_id bigint not null,
        primary key (category_id, project_id)
    ) engine=MyISAM;
    
    create table hibernate_sequence (
       next_val bigint
    ) engine=MyISAM;

    insert into hibernate_sequence values ( 1 );

    insert into hibernate_sequence values ( 1 );

    insert into hibernate_sequence values ( 1 );

    insert into hibernate_sequence values ( 1 );

    insert into hibernate_sequence values ( 1 );

    create table interview (
       id bigint not null,
        date date,
        description longtext,
        link varchar(255),
        name varchar(255),
        youtube_link varchar(255),
        primary key (id)
    ) engine=MyISAM;

    create table organisation (
       id bigint not null,
        description longtext,
        logo longblob,
        name varchar(255),
        website varchar(255),
        primary key (id)
    ) engine=MyISAM;

    create table organisation_interviews (
       interview_id bigint not null,
        organisation_id bigint not null,
        primary key (interview_id, organisation_id)
    ) engine=MyISAM;

    create table organisation_project (
       project_id bigint not null,
        organisation_id bigint not null,
        primary key (project_id, organisation_id)
    ) engine=MyISAM;
    
    create table project (
       id bigint not null,
        description longtext,
        project_from date not null,
        image_url varchar(255),
        latitude double precision not null,
        longitude double precision not null,
        street varchar(255),
        street_number varchar(255),
        zip_code varchar(255),
        name varchar(255),
        project_link varchar(255),
        project_to date,
        primary key (id)
    ) engine=MyISAM;

    create table tag (
       id bigint not null,
        text varchar(255),
        primary key (id)
    ) engine=MyISAM;

    create table tag_organisation (
       tag_id bigint not null,
        organisation_id bigint not null,
        primary key (tag_id, organisation_id)
    ) engine=MyISAM;

    create table tag_project (
       tag_id bigint not null,
        project_id bigint not null,
        primary key (tag_id, project_id)
    ) engine=MyISAM;

    alter table category_organisation 
       add constraint FKajb77tqd245cw8kmw5q22frcq 
       foreign key (organisation_id) 
       references organisation;

    alter table category_organisation 
       add constraint FKn01j766ewoulfn3794druq743 
       foreign key (category_id) 
       references category;

    alter table category_project 
       add constraint FK2ix002n9vlnfacdh6qnpg0sq8 
       foreign key (project_id) 
       references project;

    alter table category_project 
       add constraint FKcgbiuuuxpcjxyyt6malo8g0ik 
       foreign key (category_id) 
       references category;

    alter table organisation_interviews 
       add constraint FK8tbx2j6wmovtveaof3ont175y 
       foreign key (organisation_id) 
       references organisation;

    alter table organisation_interviews 
       add constraint FK3knlt6bhxt1lgxwnh8fnsl4ns 
       foreign key (interview_id) 
       references interview;

    alter table organisation_project 
       add constraint FKiei8swxu4lqd6ck558chg0e00 
       foreign key (organisation_id) 
       references organisation;

    alter table organisation_project 
       add constraint FK76shlrsd0g50pkt14eylxeagd 
       foreign key (project_id) 
       references project;

    alter table tag_organisation 
       add constraint FKopub2f756ksf7mx4pp2umhe5a 
       foreign key (organisation_id) 
       references organisation;

    alter table tag_organisation 
       add constraint FKk5ypjoo4vemss7mn3f6ey1kqw 
       foreign key (tag_id) 
       references tag;

    alter table tag_project 
       add constraint FKrdf44rhr3jvvjp83wd8wud9g5 
       foreign key (project_id) 
       references project;

    alter table tag_project 
       add constraint FKcha9voimm0ii1q43ixkjap0b4 
       foreign key (tag_id) 
       references tag;
