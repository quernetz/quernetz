package de.leipzig.htwk.deltasix.model.projections;

import de.leipzig.htwk.deltasix.DataGenerator;
import de.leipzig.htwk.deltasix.dataaccess.OrganisationRepository;
import de.leipzig.htwk.deltasix.dataaccess.ProjectRepository;
import de.leipzig.htwk.deltasix.dataaccess.TagRepository;
import de.leipzig.htwk.deltasix.model.Organisation;
import de.leipzig.htwk.deltasix.model.Project;
import de.leipzig.htwk.deltasix.model.Tag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@Transactional
public class TagsProjectionTest {

    public final String CONTENT_TYPE = "application/hal+json";
    public final String TAGS_URL = "http://localhost/api/tags/";

    @Autowired
    MockMvc mockMvc;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    OrganisationRepository organisationRepository;

    @Autowired
    ProjectRepository projectRepository;

    Tag testTag;
    Project testProject;
    Organisation testOrganisation;

    @BeforeEach
    void setUp(){
        testTag = Tag.builder()
                .text("blaahhhh")
                .organisations(new HashSet<>())
                .projects(new HashSet<>())
                .build();

        tagRepository.save(testTag);

        testProject = DataGenerator.getProject();
        testOrganisation = DataGenerator.getOrganisation();

        organisationRepository.save(testOrganisation);

        testProject.addOrganisation(testOrganisation.addProject(testProject).addTag(testTag.addOrganisation(testOrganisation))).addTag(testTag.addProject(testProject));

        projectRepository.save(testProject);
    }

    @Test
    public void shouldGetProjectionData() throws Exception{

        int testSize = 1;

        this.mockMvc.perform(get("/api/tags?projection=tags"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE))
                .andExpect(jsonPath("$._embedded.tags",hasSize(testSize)))
                .andExpect(jsonPath("$._embedded.tags[0].text").value(equalTo(testTag.getText())))
                .andExpect(jsonPath("$._embedded.tags[0]._links.organisations.href")
                        .value(equalTo(TAGS_URL+ testTag.getId().toString()+"/organisations{?projection}")))
                .andExpect(jsonPath("$._embedded.tags[0]._links.projects.href")
                        .value(equalTo(TAGS_URL+ testTag.getId().toString()+"/projects{?projection}")));
    }

    @Test
    public void shouldGetCorrectOrganisations() throws Exception{
        this.mockMvc.perform(get(TAGS_URL+testTag.getId().toString()+"/organisations"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE))
                .andExpect(jsonPath("$._embedded.organisations[0].name").value(equalTo(testOrganisation.getName())));
    }

    @Test
    public void shouldGetCorrectProjects() throws Exception{
        this.mockMvc.perform(get(TAGS_URL+testTag.getId().toString()+"/projects"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE))
                .andExpect(jsonPath("$._embedded.projects[0].name").value(equalTo(testProject.getName())));
    }
}
