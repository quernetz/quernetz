package de.leipzig.htwk.deltasix.documentation;

import com.fasterxml.jackson.databind.node.ObjectNode;
import de.leipzig.htwk.deltasix.DataGenerator;
import de.leipzig.htwk.deltasix.JSONHelper;
import de.leipzig.htwk.deltasix.dataaccess.CategoryRepository;
import de.leipzig.htwk.deltasix.dataaccess.OrganisationRepository;
import de.leipzig.htwk.deltasix.dataaccess.ProjectRepository;
import de.leipzig.htwk.deltasix.dataaccess.TagRepository;
import de.leipzig.htwk.deltasix.documentation.helpers.ConstrainedFieldHelper;
import de.leipzig.htwk.deltasix.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.hypermedia.LinkDescriptor;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.snippet.Attributes.key;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@SpringBootTest("spring.profiles.active=test")
@Transactional
public class
ProjectDocumentationTests {

    private final static String API_URL = "/api/projects/";

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    OrganisationRepository organisationRepository;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    CategoryRepository categoryRepository;

    private MockMvc mockMvc;

    private Project project;

    private Organisation organisation;

    private final String jwt = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJSYWxmIiwiZXhwIjoxNTkyNDEzOTY5LCJpYXQiOjE1OTIzOTU5Njl9.HB3iy1BkGoY3sdIQ1As0H_5QyzsASsMKUdKXGT0XO4Q";

    @BeforeEach
    private void setUp(WebApplicationContext context, RestDocumentationContextProvider documentation){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(documentationConfiguration(documentation)).build();

        this.project        = DataGenerator.getProject();
        this.organisation   = DataGenerator.getOrganisation();

        this.organisationRepository.save(organisation);

        this.project
                .addOrganisation(this.organisation
                        .addProject(this.project));

        this.projectRepository.save(this.project);
    }

    @Test
    public void projectGet() throws Exception{
        List<FieldDescriptor> fields = getConstrainedProjectFields();
        fields.addAll(getConstrainedLocationFields());
        this.mockMvc
            .perform(get(API_URL+"{project-id}",project.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andDo(document("projects/get-by-id",
                    pathParameters(parameterWithName("project-id").description("ID des Projekts")),
                    links(halLinks(),getProjectLinkDescriptors()),
                    responseFields(fields)));
    }

    @Test
    public void projectGetProjection() throws  Exception{

        List<FieldDescriptor> fields = getConstrainedProjectFields();
        fields.addAll(getConstrainedLocationFields());

        fields.add(subsectionWithPath("categories").description("Kategorien in die das Projekt fällt"));
        fields.add(subsectionWithPath("tags").description("Tags die zum Projekt passen"));
        fields.add(subsectionWithPath("organisations").description("Organisationen die mit dem Projekt verbunden sind"));

        this.mockMvc.perform(get(API_URL + "{project-id}"+"?projection=withTagsCategoriesOrganisationsAndInterviews",this.project.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("projects/get-by-id-projection",
                        pathParameters(parameterWithName("project-id").description("ID des Projekts")),
                        links(halLinks(), getProjectLinkDescriptors()),
                        responseFields(fields)));
    }
    @Test
    void projectsGet() throws Exception{
        this.mockMvc.perform(get(API_URL).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("projects/projects-get", responseFields(
                        subsectionWithPath("_embedded.projects").description("Array von Projekten").type(JsonFieldType.ARRAY),
                        subsectionWithPath("_links").ignored(),
                        subsectionWithPath("page").ignored()
                )));
    }

    @Test
    void projectDelete() throws Exception{
        this.mockMvc.perform(delete(API_URL+"{project-id}",this.project.getId()).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("projects/delete-project", pathParameters(
                        parameterWithName("project-id").description("ID des zu löschenden Projekts")
                )));
    }

    @Test
    void createProject() throws Exception{

        Tag helloWorld  = Tag.builder().text("helloWorld").build();
        Tag customWorld = Tag.builder().text("customWorld").build();

        Category umwelt = Category.builder().categoryName("umwelt").build();
        Category müll = Category.builder().categoryName("müll").build();

        this.categoryRepository.save(müll);
        this.categoryRepository.save(umwelt);
        this.tagRepository.save(helloWorld);
        this.tagRepository.save(customWorld);

        Project newProject = DataGenerator.getProject();

        List<FieldDescriptor> fields = getConstrainedProjectFields();
        fields.addAll(getConstrainedLocationFields());
        fields.addAll(Arrays.asList(
                fieldWithPath("categories").attributes(key("constraints").value("")).description("Kategorien in die das Projekt fällt (als Array von Strings mit jew. diesem Format: `'categories/{id}'`)"),
                fieldWithPath("tags").attributes(key("constraints").value("")).description("Tags die zum Projekt gehören (als Array von Strings mit jew. diesem Format: `'tags/{id}'`)"),
                fieldWithPath("organisations").attributes(key("constraints").value("")).description("Organisationen die beim Projekt beteiligt sind (als Array von Strings mit jew. diesem Format: `'organisations/{id}'`)")
        ));
        fields.removeIf(f -> f.getPath().equals("_links"));

        ObjectNode newProjectNode = JSONHelper.toObjectNode(newProject);

        LocalDate from  = newProject.getFrom();
        LocalDate to    = newProject.getTo();

        newProjectNode.put("from", from.toString());
        newProjectNode.put("to", to.toString());
        newProjectNode.remove("id");
        newProjectNode.putArray("organisations")
                .addAll(JSONHelper.getHyperlinkArray("organisations",project.getOrganisations().stream().map(o -> o.getId()).collect(Collectors.toList())));
        newProjectNode.putArray("categories").addAll(JSONHelper.getHyperlinkArray("categories", List.of(umwelt.getId(), müll.getId())));
        newProjectNode.putArray("tags").addAll(JSONHelper.getHyperlinkArray("tags", List.of(helloWorld.getId(), customWorld.getId())));

        this.mockMvc.perform(post(API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(JSONHelper.toJSON(newProjectNode))
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().isCreated())
                .andDo(document("projects/post",
                        requestFields(fields)));
    }
    @Test
    void modifyProject() throws Exception{
        String requestContent = this.mockMvc.perform(get(API_URL+this.project.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ObjectNode modifiedProjectNode = JSONHelper.parseJsonStringToObjectNode(requestContent);
        modifiedProjectNode.remove("Id");
        modifiedProjectNode.remove("_links");
        modifiedProjectNode.put("description", "New description");

        List<FieldDescriptor> fields = getConstrainedProjectFields();

        fields.removeIf(f -> f.getPath().equals("_links"));
        fields.addAll(getConstrainedLocationFields());

        this.mockMvc.perform(patch(API_URL+"{project-id}",project.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(JSONHelper.toJSON(modifiedProjectNode))
                .header("Authorization","Bearer "+jwt))
            .andExpect(status().is2xxSuccessful())
            .andDo(document("projects/modify-projects",
                    links(halLinks(),getProjectLinkDescriptors()),
                    pathParameters(parameterWithName("project-id").description("ID des Projektes, das verändert werden soll")),
                    requestFields(fields))
            );
    }

    private List<LinkDescriptor> getProjectLinkDescriptors(){
        return new ArrayList<>(Arrays.asList(
                linkWithRel("categories").description("Verknüpfung zu den assoziierten Kategorien"),
                linkWithRel("tags").description("Verknüpfung zu den assoziierten Tags"),
                linkWithRel("organisations").description("Verknüpfung zu den beteiligten Organisationen"),
                linkWithRel("project").description("Verweis auf dieses Projekt (Anmerkung: `?projection=withTagsAndCategories` enthält die Texte zu Kategorien und Tags"),
                linkWithRel("self").ignored()
        ));
    }

    private List<FieldDescriptor> getConstrainedLocationFields(){
        ConstrainedFieldHelper.ConstrainedFields fields =  ConstrainedFieldHelper.getConstrainedFields(Location.class);
        return new ArrayList<FieldDescriptor>(Arrays.asList(
            fields.withPath("location.longitude").attributes(key("constraints").value("Must be between -180.0 and 180.0. (GPS Mode)")).description("Längengrad des Projektstandorts"),
            fields.withPath("location.latitude").attributes(key("constraints").value("Must be between -90.0 and 90.0 (GPS Mode)")).description("Breitengrad des Projektstandorts"),
            fields.withPath("location.street").attributes(key("constraints").value("Must not be blank ( on Address Mode)")).description("Straßenname des Projektstandortes"),
            fields.withPath("location.streetNumber").attributes(key("constraints").value("Must not be blank ( on Address Mode)")).description("Straßennummer des Projektstandortes"),
            fields.withPath("location.zipCode").attributes(key("constraints").value("Must not be blank ( on Address Mode)")).description("Postleitzahl des Projektstandortes")
        ));
    }

    private List<FieldDescriptor> getConstrainedProjectFields(){
        ConstrainedFieldHelper.ConstrainedFields fields = ConstrainedFieldHelper.getConstrainedFields(Project.class);
        return new ArrayList<>(Arrays.asList(
            fields.withPath("name").description("Name des Projektes"),
            fields.withPath("description").description("Beschreibung zum Projekt"),
            fields.withPath("projectLink").description("HTTP(S)-Link zur Website des Projektes"),
            fields.withPath("from").description("Startdatum des Projektes"),
            fields.withPath("to").description("Enddatum des Projektes"),
            fields.withPath("imageUrl").description("URL zu Bild, dass zu diesem Projekt gehört"),
            subsectionWithPath("_links").ignored()
        ));
    }
}
