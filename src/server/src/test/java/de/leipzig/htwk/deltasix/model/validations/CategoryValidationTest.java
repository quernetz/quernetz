package de.leipzig.htwk.deltasix.model.validations;


import de.leipzig.htwk.deltasix.DataGenerator;
import de.leipzig.htwk.deltasix.dataaccess.CategoryRepository;
import de.leipzig.htwk.deltasix.dataaccess.OrganisationRepository;
import de.leipzig.htwk.deltasix.dataaccess.ProjectRepository;
import de.leipzig.htwk.deltasix.model.Category;
import de.leipzig.htwk.deltasix.model.Organisation;
import de.leipzig.htwk.deltasix.model.Project;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.validation.ConstraintViolationException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class CategoryValidationTest {

    @Autowired
    EntityManager entityManager;

    Category testCategory;
    Project testProject;
    Organisation testOrganisation;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    OrganisationRepository organisationRepository;

    @Autowired
    ProjectRepository projectRepository;

    @BeforeEach
    public void setUp(){
        testOrganisation = DataGenerator.getOrganisation();
        organisationRepository.save(testOrganisation);

        testProject = DataGenerator.getProject();
        testProject.addOrganisation(testOrganisation);
        projectRepository.save(testProject);

    }

    @Test
    public void shouldBeValid(){
        testCategory = Category.builder()
                .categoryName("Sick Category Name!")
                .projects(new HashSet<>())
                .organisations(new HashSet<>())
                .build();
        categoryRepository.save(testCategory);
        assertTrue(entityManager.contains(testCategory));
    }

    @Test
    public void shouldNotBeValid_blankName (){
        testCategory = Category.builder()
                .categoryName("")
                .organisations(Set.of(testOrganisation))
                .projects(Set.of(testProject))
                .build();
        categoryRepository.save(testCategory);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }
}
