package de.leipzig.htwk.deltasix;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Anton Rhein
 *
 * a static helper class for converting objects to JSON and manipulating them
 */
public class JSONHelper {
    private JSONHelper(){};

    private static ObjectMapper mapper = new ObjectMapper().configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    private static ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();

    /**
     * Converts an Object into a mutable object node
     * @param o Object that will be converted
     * @return converted Object
     */
    public static ObjectNode toObjectNode(Object o){
        return mapper.valueToTree(o);
    }

    /**
     * Converts any object (also @ref ObjectNode) to JSON string
     * @param object
     * @return JSON string
     * @throws JsonProcessingException
     */
    public static String toJSON(Object object) throws JsonProcessingException {
        return writer.writeValueAsString(object);
    }

    /**
     * Converts a JSON formatted string into a mutable object node
     * @param jsonString
     * @return an ObjectNode object
     * @throws JsonProcessingException
     * @throws JsonMappingException
     */
    public static ObjectNode parseJsonStringToObjectNode(String jsonString) throws JsonProcessingException, JsonMappingException {
        return toObjectNode(mapper.readTree(jsonString));
    }

    /**
     * Converts a collection of ids into an ArrayNode of hypermedia links
     * This is used for converting relational objects into HATEOAS conform hypermedia links
     * @param resourceLocation
     * @param ids
     * @return
     * @throws Exception
     */
    public static ArrayNode getHyperlinkArray(String resourceLocation, Collection<Long> ids) throws Exception{
        List<String> links = new ArrayList<>();
        for(Long id : ids){
            links.add(String.format("%s/%d", resourceLocation, id));
        }
        return ((ArrayNode) mapper.valueToTree(links));
    }
}
