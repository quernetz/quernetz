package de.leipzig.htwk.deltasix.model.validations;


import de.leipzig.htwk.deltasix.dataaccess.TagRepository;
import de.leipzig.htwk.deltasix.model.Tag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.validation.ConstraintViolationException;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class TagsValidationTest {

    @Autowired
    EntityManager entityManager;

    @Autowired
    TagRepository tagRepository;

    Tag testTag;

    @BeforeEach
    public void setUp(){
        testTag = Tag.builder()
                .text("#InsaneText")
                .projects(new HashSet<>())
                .organisations(new HashSet<>())
                .build();
    }

    @Test
    public void shouldBeValid(){
        tagRepository.save(testTag);
        assertTrue(entityManager.contains(testTag));
    }

    @Test
    public void shouldNotBeValid_blankText(){
        testTag = Tag.builder()
                .text("")
                .organisations(new HashSet<>())
                .projects(new HashSet<>())
                .build();
        tagRepository.save(testTag);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

}
