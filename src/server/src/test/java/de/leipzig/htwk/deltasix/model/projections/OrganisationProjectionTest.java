package de.leipzig.htwk.deltasix.model.projections;

import de.leipzig.htwk.deltasix.DataGenerator;
import de.leipzig.htwk.deltasix.dataaccess.*;
import de.leipzig.htwk.deltasix.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@Transactional
public class OrganisationProjectionTest {


    public static final String ORGANISATION_URL = "http://localhost/api/organisations/";
    @Autowired
    MockMvc mockMvc;

    @Autowired
    InterviewRepository interviewRepository;

    @Autowired
    OrganisationRepository testRepo;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    ProjectRepository projectRepository;

    final String MEDIA_TYPE = "application/hal+json";
    Organisation organisation;
    Interview testInterview;
    Project testProject;
    Tag testTag;
    Category testCategory;

    @BeforeEach
    void setUp() {
        testTag = Tag.builder()
                .text("legendary text")
                .build();

        tagRepository.save(testTag);

        testCategory = Category.builder()
                .categoryName("legendary name")
                .build();

        categoryRepository.save(testCategory);

        testInterview = DataGenerator.getInterview();
        testProject = DataGenerator.getProject();
        organisation = DataGenerator.getOrganisation();

        organisation.addTag(testTag.addOrganisation(organisation)).addCategory(testCategory.addOrganisation(organisation));

        testRepo.save(organisation);

        organisation.addInterview(testInterview);

        interviewRepository.save(testInterview);

        organisation.addProject(testProject.addOrganisation(organisation));

        projectRepository.save(testProject);
    }

    @Test
    public void shouldGetProjectionData() throws Exception {
        int testSize = 1;

        this.mockMvc.perform(get("/api/organisations?projection=withTagsCategoriesProjectsAndInterviews"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MEDIA_TYPE))
                .andExpect(jsonPath("$._embedded.organisations",hasSize(testSize)))
                .andExpect(jsonPath("$._embedded.organisations[0].name").value(equalTo(organisation.getName())))
                .andExpect(jsonPath("$._embedded.organisations[0]._links.tags.href")
                        .value(equalTo(ORGANISATION_URL+organisation.getId().toString()+"/tags")))
                .andExpect(jsonPath("$._embedded.organisations[0]._links.categories.href")
                       .value(equalTo(ORGANISATION_URL+organisation.getId().toString()+"/categories")))
                .andExpect(jsonPath("$._embedded.organisations[0]._links.projects.href")
                        .value(equalTo(ORGANISATION_URL +organisation.getId().toString()+"/projects{?projection}")))
                .andExpect(jsonPath("$._embedded.organisations[0]._links.interviews.href")
                        .value(equalTo(ORGANISATION_URL+organisation.getId()+"/interviews{?projection}")))
                .andExpect(jsonPath("$._embedded.organisations[0].tags[0].text")
                        .value(equalTo(testTag.getText())))
                .andExpect(jsonPath("$._embedded.organisations[0].categories[0].categoryName")
                        .value(equalTo(testCategory.getCategoryName())));
    }

    @Test
    public void shouldGetCorrectTag() throws Exception{
        this.mockMvc.perform(get(ORGANISATION_URL+organisation.getId().toString()+"/tags"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MEDIA_TYPE))
                .andExpect(jsonPath("$._embedded.tags[0].text")
                        .value(equalTo(testTag.getText())));
    }

    @Test
    public void shouldGetCorrectCategory() throws Exception{
        this.mockMvc.perform(get(ORGANISATION_URL+organisation.getId().toString()+"/categories"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MEDIA_TYPE))
                .andExpect(jsonPath("$._embedded.categories[0].categoryName")
                        .value(equalTo(testCategory.getCategoryName())));
    }

    @Test
    public void shouldGetCorrectProject() throws Exception{
        this.mockMvc.perform(get(ORGANISATION_URL+organisation.getId().toString()+"/projects"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MEDIA_TYPE))
                .andExpect(jsonPath("$._embedded.projects[0].name")
                        .value(equalTo(testProject.getName())));
    }

    @Test
    public void shouldGetCorrectInterviews() throws Exception{
        this.mockMvc.perform(get(ORGANISATION_URL+organisation.getId().toString()+"/interviews"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MEDIA_TYPE))
                .andExpect(jsonPath("$._embedded.interviews[0].name")
                        .value(equalTo(testInterview.getName())));
    }
}
