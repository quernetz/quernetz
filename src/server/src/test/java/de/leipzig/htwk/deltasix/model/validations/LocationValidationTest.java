package de.leipzig.htwk.deltasix.model.validations;

import de.leipzig.htwk.deltasix.model.Location;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class LocationValidationTest {


    Location testLocation;
    Location testDoubleLocation;
    ValidLocationValidator validator;

    ConstraintValidatorContext context;
    ConstraintValidatorContext.ConstraintViolationBuilder builder;
    @BeforeEach
    public void setUp(){
        validator = new ValidLocationValidator();
        testLocation = new Location("Bergstraße","12","04187");
        testDoubleLocation = new Location(51.335115, 12.402636);
        context = Mockito.mock(ConstraintValidatorContext.class);
        builder = Mockito.mock(ConstraintValidatorContext.ConstraintViolationBuilder.class);
        when(builder.addConstraintViolation()).thenReturn(context);
        when(context.buildConstraintViolationWithTemplate(anyString())).thenReturn(builder);
    }

    @Test
    public void shouldBeValid(){
        boolean result = validator.isValid(testLocation, context);
        boolean result2 = validator.isValid(testDoubleLocation,context);
        assertTrue(result2);
        assertTrue(result);
    }

    @Test
    public void shouldBeValidAtNegativeLimitLat(){
        testDoubleLocation.setLatitude(-90);
        boolean result = validator.isValid(testDoubleLocation,context);
        assertTrue(result);
    }

    @Test
    public void shouldBeValidAtLimitLat(){
        testDoubleLocation.setLatitude(90);
        boolean result = validator.isValid(testDoubleLocation,context);
        assertTrue(result);
    }

    @Test
    public void shouldBeValidAtLimitLong(){
        testDoubleLocation.setLongitude(180);
        boolean result = validator.isValid(testDoubleLocation,context);
        assertTrue(result);
    }

    @Test
    public void shouldBeValidAtNegativeLimitLong(){
        testDoubleLocation.setLongitude(-180);
        boolean result = validator.isValid(testDoubleLocation,context);
        assertTrue(result);
    }

    @Test
    public void shouldBeValidWithNull(){
        boolean result = validator.isValid(null,context);
        assertTrue(result);
    }

    @Test
    public void shouldNotBeValid_blankStreet(){
        testLocation.setStreet("");
        boolean result = validator.isValid(testLocation,context);
        assertFalse(result);
    }

    @Test
    public void shouldNotBeValid_blankStreetNumber(){
        testLocation.setStreetNumber("");
        boolean result = validator.isValid(testLocation,context);
        assertFalse(result);
    }

    @Test
    public void shouldNotBeValid_blankZipCode(){
        testLocation.setZipCode("");
        boolean result = validator.isValid(testLocation,context);
        assertFalse(result);
    }

    @Test
    public void shouldNotBeValid_LatUnderNegLimit(){
        testDoubleLocation.setLatitude(-90.10);
        boolean result = validator.isValid(testDoubleLocation,context);
        assertFalse(result);
    }

    @Test
    public void shouldNotBeValid_LatOverLimit(){
        testDoubleLocation.setLatitude(90.10);
        boolean result = validator.isValid(testDoubleLocation,context);
        assertFalse(result);
    }

    @Test
    public void shouldNotBeValid_LatFarOverLimit(){
        testDoubleLocation.setLatitude(190.10);
        boolean result = validator.isValid(testDoubleLocation,context);
        assertFalse(result);
    }

    @Test
    public void shouldNotBeValid_LatFarUnderNegLimit(){
        testDoubleLocation.setLatitude(-190.10);
        boolean result = validator.isValid(testDoubleLocation,context);
        assertFalse(result);
    }

    @Test
    public void shouldNotBeValid_LongUnderNegLimit(){
        testDoubleLocation.setLongitude(-180.10);
        boolean result = validator.isValid(testDoubleLocation,context);
        assertFalse(result);
    }

    @Test
    public void shouldNotBeValid_LongOverLimit(){
        testDoubleLocation.setLongitude(180.10);
        boolean result = validator.isValid(testDoubleLocation,context);
        assertFalse(result);
    }

    @Test
    public void shouldNotBeValid_LongFarUnderNegLimit(){
        testDoubleLocation.setLongitude(-280.10);
        boolean result = validator.isValid(testDoubleLocation,context);
        assertFalse(result);
    }

    @Test
    public void shouldNotBeValid_LongFarOverLimit(){
        testDoubleLocation.setLongitude(280.10);
        boolean result = validator.isValid(testDoubleLocation,context);
        assertFalse(result);
    }

    @Test
    public void shouldNotBeValid_LatAndLongNull(){
        testDoubleLocation.setLongitude(0);
        testDoubleLocation.setLatitude(0);
        boolean result = validator.isValid(testDoubleLocation,context);
        assertFalse(result);
    }
}
