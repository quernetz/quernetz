package de.leipzig.htwk.deltasix.model;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public class ProjectTests<model> {

    private final static String DESCRIPTION         = "My Awesome Project Description!";
    private static final String IMAGE_URL          = "https://i.ytimg.com/vi/aEsTwTkub9k/maxresdefault.jpg";
    private static final Category CATEGORY          = Category.builder().categoryName("Bier trinken").build();
    private static final String PROJECT_NAME        = "Awesome Project";
    private static final Location LOCATION          = new Location("Hello","World","12345");
    private static final String PROJECT_LINK        = "https://i.ytimg.com/vi/aEsTwTkub9k/maxresdefault.jpg";
    private static final int PROJECT_YEAR           = 4221;

    private Set<Tag> TAGS;
    private Project testSubject;

    public ProjectTests()
    {
        TAGS = List.of("#coolTag","#nousSommesHansJürgen","#saubereStrassenReineHerzen")
                .parallelStream()
                .map(t ->
                        Tag.builder().text(t).build())
                .collect(Collectors.toSet());
    }

    @BeforeEach
    public void setUp(){
        testSubject = Project.builder()
                .description(DESCRIPTION)
                .from(LocalDate.of(2010,10,12))
                .to(LocalDate.of(2011,04,3))
                .imageUrl(IMAGE_URL)
                .location(LOCATION)
                .categories(new HashSet<>(Arrays.asList(CATEGORY)))
                .name(PROJECT_NAME)
                .projectLink(PROJECT_LINK)
                .tags(TAGS)
                .build();
    }

    @Test
    public void projectShouldReturnImmutableCollectionsOfChildren()
    {
        assertThrows(UnsupportedOperationException.class, () -> testSubject.getTags().add(Tag.builder().text("Huhu").build()));
        assertThrows(UnsupportedOperationException.class, () -> testSubject.getCategories().add(Category.builder().categoryName("Nun!").build()));
    }

    @Test
    public void dateSettersShouldThrowExceptionOnInvalidArguments()
    {
        assertThrows(Exception.class,() -> testSubject.setFrom(LocalDate.of(2010, 13, 20)));
        assertThrows(Exception.class,() -> testSubject.setFrom(LocalDate.of(2010, 10, 40)));
        assertThrows(Exception.class,() -> testSubject.setTo(LocalDate.of(2010,13,20)));
        assertThrows(Exception.class,() -> testSubject.setFrom(LocalDate.of(-1,10,40)));
        assertThrows(Exception.class,() -> testSubject.setFrom(LocalDate.of(2010,-1,40)));
        assertThrows(Exception.class,() -> testSubject.setFrom(LocalDate.of(2010,10,-1)));
        assertThrows(Exception.class,() -> testSubject.setTo(LocalDate.of(-1,10,40)));
        assertThrows(Exception.class,() -> testSubject.setTo(LocalDate.of(2010,-1,40)));
        assertThrows(Exception.class,() -> testSubject.setTo(LocalDate.of(2010,10,-1)));
    }

    @Test
    public void addSingleToCollectionsShouldWork()
    {
        Category newCategory = Category.builder().categoryName("new Sector").build();
        testSubject.addCategory(newCategory);
        assertEquals(Set.of(CATEGORY, newCategory),testSubject.getCategories());

        Set<Tag> testSubjectTags = testSubject.getTags();
        Tag newTag = Tag.builder().text("#hansJürgenForPresident").build();
        Set<Tag> expectedTags = new HashSet(testSubjectTags);
        expectedTags.add(newTag);
        testSubject.addTag(newTag);
        assertEquals(testSubject.getTags(),expectedTags);
    }

    @Test
    public void addMultipleToCollectionsShouldWork()
    {
        assertEquals(1, testSubject.getCategories().size());

        assertEquals(1, testSubject.getCategories().size());
        assertEquals(TAGS.size(), testSubject.getTags().size());

        Collection<Category> newCategories = List.of("New 1", "New 2", "New 3").parallelStream().map(c -> Category.builder().categoryName(c).build()).collect(Collectors.toList());
        testSubject.addCategories(newCategories);
        newCategories.add(CATEGORY);
        assertEquals(newCategories.size(), testSubject.getCategories().size());
        testSubject.getCategories().forEach(c -> assertTrue(newCategories.contains(c)));
        assertEquals(TAGS.size(), testSubject.getTags().size());

        Collection<Tag> newTags = List.of("CoolTag", "NextCoolTag", "Bier", "läuft").parallelStream().map(t -> Tag.builder().text(t).build()).collect(Collectors.toList());
        Collection<Tag> testSubjectTags =  testSubject.getTags();
        Set<Tag> expected = new HashSet<>(testSubjectTags);
        expected.addAll(newTags);
        testSubject.addTags(newTags);

        assertEquals(expected, testSubject.getTags());
        assertEquals(4, testSubject.getCategories().size());
    }

    @Test
    public void removeMultipleFromCollectionsShouldWork()
    {
        assertEquals(1, testSubject.getCategories().size());
        assertEquals(1, testSubject.getCategories().size());

        assertEquals(TAGS.size(), testSubject.getTags().size());

        // create test categories
        Collection<Category> categories = List.of("a", "b","c").parallelStream().map(n -> Category.builder().categoryName(n).build()).collect(Collectors.toList());
        // add them to testSubject (Project)
        categories.forEach(testSubject::addCategory);
        // remove "a"-category and "b"-category
        testSubject.removeCategories(categories.parallelStream().filter(c -> c.getCategoryName().equals("a") || c.getCategoryName().equals("b")).collect(Collectors.toList()));
        // check for remaining categories ('c' and 'Bier trinken')
        List<Category> expectedCategories = categories.parallelStream().filter(c -> c.getCategoryName().equals("c")).collect(Collectors.toList());
        expectedCategories.add(CATEGORY);

        assertEquals(expectedCategories.size(), testSubject.getCategories().size());
        testSubject.getCategories().forEach(c -> expectedCategories.contains(c));
        assertEquals(TAGS.size(), testSubject.getTags().size());

        testSubject.removeTags(TAGS.parallelStream().filter(t -> t.getText().equals("#saubereStrassenReineHerzen") || t.getText().equals("#coolTag")).collect(Collectors.toList()));
        List<Tag> expectedTags = TAGS.parallelStream().filter(t -> t.getText().equals("#nousSommesHansJürgen")).collect(Collectors.toList());
        assertEquals(expectedTags.size(), testSubject.getTags().size());
        testSubject.getTags().forEach(t -> assertTrue(expectedTags.contains(t)));
        assertEquals(2, testSubject.getCategories().size());
    }

    @Test
    public void removeSingleFromCollectionShouldWork()
    {
        assertEquals(1, testSubject.getCategories().size());
        assertEquals(TAGS.size(), testSubject.getTags().size());

        Category a = Category.builder().categoryName("a").build();
        Category b = Category.builder().categoryName("b").build();

        testSubject.addCategory(a)
                .addCategory(b)
                .removeCategory(a);

        List<Category> expectedCategories  = new ArrayList<Category>();
        expectedCategories.add(CATEGORY);
        expectedCategories.add(b);

        assertEquals(expectedCategories.size(), testSubject.getCategories().size());
        expectedCategories.forEach(c -> assertTrue(testSubject.getCategories().contains(c)));
        assertEquals(TAGS.size(), testSubject.getTags().size());

        Tag coolTag = TAGS.parallelStream().filter(t -> t.getText().equals("#coolTag")).findFirst().get();

        testSubject.removeTag(coolTag);
        List<Tag> expected = TAGS.parallelStream().filter( t -> t.getText().equals("#nousSommesHansJürgen") || t.getText().equals("#saubereStrassenReineHerzen")).collect(Collectors.toList());
        assertEquals(expected.size(), testSubject.getTags().size());
        testSubject.getTags().forEach(t -> assertTrue(expected.contains(t)));
        assertEquals(2, testSubject.getCategories().size());
    }
}
