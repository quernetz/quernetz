package de.leipzig.htwk.deltasix;

import de.leipzig.htwk.deltasix.model.Interview;
import de.leipzig.htwk.deltasix.model.Location;
import de.leipzig.htwk.deltasix.model.Organisation;
import de.leipzig.htwk.deltasix.model.Project;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;

public class DataGenerator {
    private DataGenerator(){}

    public static Organisation getOrganisation(){
        return Organisation.builder()
                .tags(new HashSet<>())
                .categories(new HashSet<>())
                .interviews(new HashSet<>())
                .projects(new HashSet<>())
                .logo("".getBytes())
                .website("https://www.myawesomeorganisation.sh")
                .description("Test data organisation")
                .name("Black Squad")
                .build();
    }

    public static Project getProject(){
        return Project.builder()
                .organisations(new HashSet<>())
                .tags(new HashSet<>())
                .categories(new HashSet<>())
                .imageUrl("https://www.serpentineproject.class")
                .name("Project Serpentine")
                .projectLink("https://www.serpentineproject.class")
                .description("Test data project")
                .from(LocalDate.now().minusDays(1))
                .to(LocalDate.now().plusDays(1))
                .location(new Location("Teststreet","24a","12345"))
                .build();
    }

    public static Interview getInterview(){
        return Interview.builder()
                .organisations(new HashSet<>())
                .link("https://the-interview.mov")
                .description("The interview description")
                .name("The interview name")
                .youtubeLink("https://youtube.com/you-shall-not-pass")
                .date(LocalDate.now())
                .build();
    }
}
