package de.leipzig.htwk.deltasix.dataaccess;

import de.leipzig.htwk.deltasix.model.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
@DataJpaTest(properties = "spring.profiles.active=test")
public class DataAccessIntegrationTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TagRepository tagRepo;

    @Autowired
    private CategoryRepository catRepo;

    @Autowired
    private OrganisationRepository orgRepo;

    @Autowired
    private ProjectRepository projectRepo;

    @Autowired
    private InterviewRepository intRepository;

    @Test
    public void shouldSaveNewProjectAndNewAssociatedTags(){

        Project p = setupProject();

        Tag tag = Tag.builder().text("#corona").build();
        Tag newTag = Tag.builder().text("#weSurvivedCorona!").build();

        tagRepo.saveAll(List.of(tag,newTag));

        p.addTag(tag);
        p.addTag(newTag);

        projectRepo.save(p);

        Project newDbProject = projectRepo.findById(p.getId()).get();
        assertTrue(newDbProject.getTags().parallelStream().anyMatch(t -> t.getText().equals(tag.getText())));
        assertTrue(newDbProject.getTags().parallelStream().anyMatch(t -> t.getText().equals(newTag.getText())));
        assertTrue(tag.getId() > 0);
        assertTrue(newTag.getId() > 0);
        assertTrue(p.getId() > 0);
    }

    @Test
    public void shouldUpdateTagsToPersistedProject(){
        Project persistedProject = setupProject();
        Organisation organisation = setupOrganisation();
        orgRepo.save(organisation);
        persistedProject.addOrganisation(organisation.addProject(persistedProject));
        projectRepo.save(persistedProject);

        entityManager.flush();
        entityManager.clear();

        Long pId = persistedProject.getId();
        assertTrue(pId > 0, "Project was not persisted!");
        Project dbProject = projectRepo.findById(pId).get();

        Tag t1 = Tag.builder().text("#iLike").build();
        Tag t2 = Tag.builder().text("#iHate").build();

        tagRepo.saveAll(List.of(t1,t2));

        dbProject.addTags(Arrays.asList(t1,t2));
        t1.addProject(dbProject);
        t2.addProject(dbProject);
        projectRepo.save(dbProject);

        entityManager.flush();
        entityManager.clear();

        Collection<Tag> dbTags = tagRepo.findAll();

        assertEquals(2,dbTags.size());
        assertTrue(dbTags.parallelStream().allMatch(t -> t.getId() > 0), "Not all tags were persisted!" );
        assertTrue(dbTags.parallelStream().anyMatch(t -> t.getText().equals("#iLike")), "One tag is missing!");
        assertTrue(dbTags.parallelStream().anyMatch(t -> t.getText().equals("#iHate")),"One tag is missing!");

        //test if relationship is properly persisted:
        Set<Project> assignedDbProjects = new HashSet<>();
        //get projects that are assigned to tags in db:
        dbTags.forEach(t -> assignedDbProjects.addAll(t.getProjects()));
        assertEquals(1,assignedDbProjects.size());
        assertTrue(assignedDbProjects.parallelStream().allMatch(p -> p.getId()==pId));

        //test if same relationship is accessible from other direction:

        List<Long> dbProjectTagIds = projectRepo.findById(dbProject.getId()).get().getTags().parallelStream().map(t -> t.getId()).collect(Collectors.toList());
        assertEquals(2,dbProjectTagIds.size());
        assertTrue(dbTags.parallelStream().allMatch(t -> dbProjectTagIds.contains(t.getId())));
    }

    @Test
    public void shouldAssignPersistedTagsToProject(){
        Tag tag = Tag.builder().text("#anoroc").build();
        tagRepo.save(tag);

        assertTrue(tag.getId() > 0, "Tag was not persisted!");

        Project project = setupProject();
        project.addTag(tag);
        projectRepo.save(project);

        assertTrue(project.getId() > 0, "Project was not persisted !");

        Project dbProject = projectRepo.findById(project.getId()).get();

        assertEquals(1,dbProject.getTags().size());
        assertTrue(dbProject.getTags().parallelStream().allMatch(t -> t.getText().equals("#anoroc")));
    }

    @Test
    public void shouldAssignPersistedTagsToPersistedProjects(){
        Project project = setupProject();

        Tag t1 = Tag.builder().text("T1").build();
        Tag t2 = Tag.builder().text("T2").build();

        Organisation organisation = setupOrganisation();
        orgRepo.save(organisation);

        project.addOrganisation(organisation);

        projectRepo.save(project);
        tagRepo.saveAll(Arrays.asList(t1,t2));

        entityManager.flush();
        entityManager.clear();

        project = projectRepo.findById(project.getId()).get();

        project.addTags(List.of(t1,t2));
        t1.addProject(project);
        t2.addProject(project);
        tagRepo.saveAll(List.of(t1,t2));

        entityManager.flush();
        entityManager.clear();

        Project dbProject = projectRepo.findById(project.getId()).get();

        assertEquals(2,dbProject.getTags().size());
        assertTrue(dbProject.getTags().parallelStream().anyMatch(t -> t.getText().equals("T1")), "T1 was not assigned to Project");
        assertTrue(dbProject.getTags().parallelStream().anyMatch(t -> t.getText().equals("T2")), "T2 was not assigned to Project");
    }

    @Test
    public void shouldPersistNewCategoriesForPersistedProject(){
        Project project = setupProject();
        Category c1 = Category.builder().categoryName("c1").build();
        Category c2 = Category.builder().categoryName("c2").build();
        Organisation organisation = setupOrganisation();
        orgRepo.save(organisation);
        project.addOrganisation(organisation);
        projectRepo.save(project);

        entityManager.flush();
        entityManager.clear();

        project.addCategories(List.of(c1,c2));
        c1.addProject(project);
        c2.addProject(project);

        catRepo.saveAll(List.of(c1,c2));

        entityManager.flush();
        entityManager.clear();

        List<Category> dbCategories = catRepo.findAll();

        assertTrue(dbCategories.parallelStream().allMatch(c -> c.getId() > 0),"Some categories were not persisted!");
        assertTrue(dbCategories.parallelStream().anyMatch(c -> c.getCategoryName().equals("c1")), "Could not retrieve Category c1 from database!");
        assertTrue(dbCategories.parallelStream().anyMatch(c-> c.getCategoryName().equals("c2")), "Could not retrieve Category c2 from database!");

        Project relationshipProject = projectRepo.findById(project.getId()).get();
        Set<Project> projectsFromPersistedCategories = new HashSet<>();
        dbCategories.forEach(c -> projectsFromPersistedCategories.addAll(c.getProjects()));
        assertEquals(2,relationshipProject.getCategories().size());
        assertEquals(1, projectsFromPersistedCategories.size());
    }

    @Test
    public void shouldPersistNewProjectWithPersistedCategories(){
        Category category = Category.builder().categoryName("Wuff").build();
        catRepo.save(category);

        assertTrue(category.getId() > 0, "Category could not be stored in database!");

        Project project = setupProject();
        Organisation organisation = setupOrganisation();

        orgRepo.save(organisation);

        project.addOrganisation(organisation.addProject(project));
        project.addCategory(category.addProject(project));
        projectRepo.save(project);

        assertTrue(project.getId() > 0, "Project could not be saved!");

        entityManager.flush();
        entityManager.clear();

        Project dbProject = projectRepo.findById(project.getId()).get();
        assertEquals(1, dbProject.getCategories().size());

        Category dbCategory = catRepo.findById(category.getId()).get();
        assertEquals(1, dbCategory.getProjects().size());
    }

    @Test
    public void shouldPersistCategoriesToPersistedProject(){
        Project project = setupProject();
        Organisation organisation = setupOrganisation();
        orgRepo.save(organisation);

        project.addOrganisation(organisation.addProject(project));
        projectRepo.save(project);

        entityManager.flush();
        entityManager.clear();

        Project dbProject = projectRepo.findById(project.getId()).get();
        Category category = Category.builder().categoryName("Guacorona").build();
        category.addProject(dbProject);
        dbProject.addCategory(category);

        catRepo.save(category);
        entityManager.flush();
        entityManager.clear();

        Project dbCatP = projectRepo.findById(dbProject.getId()).get();
        assertEquals(1,dbCatP.getCategories().size());
        List<Category> dbC = catRepo.findAll();
        assertEquals(1, dbC.size());
        assertEquals(1,dbC.get(0).getProjects().size());
    }

    @Test
    public void shouldAssignPersistedCategoriesToPersistedProjects(){
        Category c1 = Category.builder().categoryName("Help").build();
        Project project = setupProject();
        Organisation organisation = setupOrganisation();
        orgRepo.save(organisation);
        project.addOrganisation(organisation);
        catRepo.save(c1);
        projectRepo.save(project);

        entityManager.flush();
        entityManager.clear();

        Category dbCategory = catRepo.findById(c1.getId()).get();
        Project dbProject = projectRepo.findById(project.getId()).get();

        dbProject.addCategory(dbCategory);
        dbCategory.addProject(dbProject);
        projectRepo.save(dbProject);

        entityManager.flush();
        entityManager.clear();

        Project dbCatProject = projectRepo.findById(dbProject.getId()).get();
        assertEquals(1,dbCatProject.getCategories().size());

        Category dbProjectCat = catRepo.findById(c1.getId()).get();
        assertEquals(1,dbProjectCat.getProjects().size());
    }

    @Test
    public void shouldPersistNewOrganisationAndCategories(){
        Organisation organisation = setupOrganisation();
        Category c1 = Category.builder().categoryName("CoronaDropKick").build();
        Category c2 = Category.builder().categoryName("Overload").build();

        catRepo.saveAll(List.of(c1,c2));

        organisation.addCategories(List.of(c1,c2));

        orgRepo.save(organisation);

        assertTrue(organisation.getId() > 0, "Organisation was not persisted!");
        assertTrue(c1.getId() > 0, "Category1 was not persisted!");
        assertTrue(c2.getId() > 0, "Category2 was not persisted!");
    }

    @Test
    public void shouldPersistNewCategoriesForPersistedOrganisation(){
        Organisation organisation = setupOrganisation();
        Category c1 = Category.builder().categoryName("c1").build();
        Category c2 = Category.builder().categoryName("c2").build();
        orgRepo.save(organisation);

        entityManager.flush();
        entityManager.clear();

        organisation = orgRepo.findById(organisation.getId()).get();
        organisation.addCategory(c1.addOrganisation(organisation)).addCategory(c2.addOrganisation(organisation));

        catRepo.saveAll(List.of(c1,c2));

        entityManager.flush();
        entityManager.clear();

        List<Category> dbCategories = catRepo.findAll();

        assertTrue(dbCategories.parallelStream().allMatch(c -> c.getId() > 0),"Some categories were not persisted!");
        assertTrue(dbCategories.parallelStream().anyMatch(c -> c.getCategoryName().equals("c1")), "Could not retrieve Category c1 from database!");
        assertTrue(dbCategories.parallelStream().anyMatch(c-> c.getCategoryName().equals("c2")), "Could not retrieve Category c2 from database!");

        Organisation relationshipOrganisation = orgRepo.findById(organisation.getId()).get();
        Set<Organisation> organisationsFromPersistedCategories = new HashSet<>();
        dbCategories.forEach(c -> organisationsFromPersistedCategories.addAll(c.getOrganisations()));
        assertEquals(2,relationshipOrganisation.getCategories().size());
        assertEquals(1, organisationsFromPersistedCategories.size());
    }

    @Test
    public void shouldPersistNewOrganisationWithPersistedCategories(){
        Category category = Category.builder().categoryName("Wuff").build();
        catRepo.save(category);

        assertTrue(category.getId() > 0, "Category could not be stored in database!");

        Organisation organisation = setupOrganisation();
        organisation.addCategory(category);
        category.addOrganisation(organisation);
        orgRepo.save(organisation);

        assertTrue(organisation.getId() > 0, "Organisation could not be saved!");

        entityManager.flush();
        entityManager.clear();

        Organisation dbOrganisation = orgRepo.findById(organisation.getId()).get();
        assertEquals(1, dbOrganisation.getCategories().size());

        Category dbCategory = catRepo.findById(category.getId()).get();
        assertEquals(1, dbCategory.getOrganisations().size());
    }

    @Test
    public void shouldPersistCategoriesToPersistedOrganisation(){
        Organisation organisation = setupOrganisation();
        orgRepo.save(organisation);

        entityManager.flush();
        entityManager.clear();

        Organisation dbOrganisation = orgRepo.findById(organisation.getId()).get();
        Category category = Category.builder().categoryName("Guacorona").build();
        dbOrganisation.addCategory(category);
        category.addOrganisation(dbOrganisation);

        catRepo.save(category);
        entityManager.flush();
        entityManager.clear();

        Organisation dbCatO = orgRepo.findById(dbOrganisation.getId()).get();
        assertEquals(1,dbCatO.getCategories().size());
        List<Category> dbC = catRepo.findAll();
        assertEquals(1, dbC.size());
        assertEquals(1,dbC.get(0).getOrganisations().size());
    }

    @Test
    public void shouldAssignPersistedCategoriesToPersistedOrganisations(){
        Category c1 = Category.builder().categoryName("Help").build();
        Organisation organisation = setupOrganisation();
        catRepo.save(c1);
        orgRepo.save(organisation);

        entityManager.flush();
        entityManager.clear();

        Category dbCategory = catRepo.findById(c1.getId()).get();
        Organisation dbOrganisation = orgRepo.findById(organisation.getId()).get();

        dbOrganisation.addCategory(dbCategory);
        dbCategory.addOrganisation(dbOrganisation);
        orgRepo.save(dbOrganisation);

        entityManager.flush();
        entityManager.clear();

        Organisation dbCatOrganisation = orgRepo.findById(dbOrganisation.getId()).get();
        assertEquals(1,dbCatOrganisation.getCategories().size());

        Category dbProjectCat = catRepo.findById(c1.getId()).get();
        assertEquals(1,dbProjectCat.getOrganisations().size());
    }

    @Test
    public void shouldUpdateTagsToPersistedOrganisation(){
        Organisation persistedOrganisation = setupOrganisation();
        orgRepo.save(persistedOrganisation);

        entityManager.flush();
        entityManager.clear();

        Long oId = persistedOrganisation.getId();
        assertTrue(oId > 0, "Organisation was not persisted!");
        Organisation dbOrganisation = orgRepo.findById(oId).get();

        Tag t1 = Tag.builder().text("#iLike").build();
        Tag t2 = Tag.builder().text("#iHate").build();

        tagRepo.saveAll(List.of(t1,t2));

        dbOrganisation.addTags(Arrays.asList(t1,t2));
        orgRepo.save(dbOrganisation);
        t1.addOrganisation(dbOrganisation);
        t2.addOrganisation(dbOrganisation);

        entityManager.flush();
        entityManager.clear();

        Collection<Tag> dbTags = tagRepo.findAll();

        assertEquals(2,dbTags.size());
        assertTrue(dbTags.parallelStream().allMatch(t -> t.getId() > 0), "Not all tags were persisted!" );
        assertTrue(dbTags.parallelStream().anyMatch(t -> t.getText().equals("#iLike")), "One tag is missing!");
        assertTrue(dbTags.parallelStream().anyMatch(t -> t.getText().equals("#iHate")),"One tag is missing!");

        //test if relationship is properly persisted:
        Set<Organisation> assignedDbOrganisations = new HashSet<>();
        //get projects that are assigned to tags in db:
        dbTags.forEach(t -> assignedDbOrganisations.addAll(t.getOrganisations()));
        assertEquals(1,assignedDbOrganisations.size());
        assertTrue(assignedDbOrganisations.parallelStream().allMatch(p -> p.getId() == oId));

        //test if same relationship is accessible from other direction:
        List<Long> dbOrganisationTagIds = orgRepo.findById(dbOrganisation.getId()).get().getTags().parallelStream().map(t -> t.getId()).collect(Collectors.toList());
        assertEquals(2,dbOrganisationTagIds.size());
        assertTrue(dbTags.parallelStream().allMatch(t -> dbOrganisationTagIds.contains(t.getId())));
    }

    @Test
    public void shouldAssignPersistedTagsToOrganisation(){
        Tag tag = Tag.builder().text("#anoroc").build();
        tagRepo.save(tag);

        assertTrue(tag.getId() > 0, "Tag was not persisted!");

        Organisation organisation = setupOrganisation();
        organisation.addTag(tag);
        orgRepo.save(organisation);

        assertTrue(organisation.getId() > 0, "Organisation was not persisted !");

        Organisation dbOrganisation = orgRepo.findById(organisation.getId()).get();

        assertEquals(1,dbOrganisation.getTags().size());
        assertTrue(dbOrganisation.getTags().parallelStream().allMatch(t -> t.getText().equals("#anoroc")));
    }

    @Test
    public void shouldAssignPersistedTagsToPersistedOrganisations(){
        Organisation organisation = setupOrganisation();
        Tag t1 = Tag.builder().text("T1").build();
        Tag t2 = Tag.builder().text("T2").build();

        orgRepo.save(organisation);
        tagRepo.saveAll(Arrays.asList(t1,t2));

        entityManager.flush();
        entityManager.clear();

        organisation = orgRepo.findById(organisation.getId()).get();
        t1 = tagRepo.findById(t1.getId()).get();
        t2 = tagRepo.findById(t2.getId()).get();
        organisation.addTag(t1.addOrganisation(organisation)).addTag(t2.addOrganisation(organisation));
        orgRepo.save(organisation);

        entityManager.flush();
        entityManager.clear();

        Organisation dbOrganisation = orgRepo.findById(organisation.getId()).get();

        assertEquals(2,dbOrganisation.getTags().size());
        assertTrue(dbOrganisation.getTags().parallelStream().anyMatch(t -> t.getText().equals("T1")), "T1 was not assigned to Project");
        assertTrue(dbOrganisation.getTags().parallelStream().anyMatch(t -> t.getText().equals("T2")), "T2 was not assigned to Project");
    }

    @Test
    public void shouldNotDeleteAssociatedTagsOrCategories(){
        Organisation organisation   = setupOrganisation();
        orgRepo.save(organisation);
        Project project             = setupProject();
        Tag tag                     = Tag.builder().text("#corONA").build();
        tagRepo.save(tag);
        Category category           = Category.builder().categoryName("Savings").build();
        catRepo.save(category);

        project.addCategory(category.addProject(project))
                .addOrganisation(organisation
                        .addProject(project)
                        .addCategory(category.addOrganisation(organisation)))
                .addTag(tag
                        .addProject(project)
                        .addOrganisation(organisation
                                .addTag(tag)));

        projectRepo.save(project);
        orgRepo.save(organisation);

        entityManager.flush();
        entityManager.clear();

        assertEquals(1, tagRepo.count());
        assertEquals(1, catRepo.count());

        projectRepo.delete(project);
        orgRepo.delete(organisation);

        entityManager.flush();
        entityManager.clear();

        assertEquals(1, tagRepo.count());
        assertEquals(1, catRepo.count());
    }

    @Test
    public void shouldUpdateProjectToPersistedOrganisation(){
        Organisation persistedOrganisation = setupOrganisation();
        orgRepo.save(persistedOrganisation);

        entityManager.flush();
        entityManager.clear();

        Organisation dbOrganisation = orgRepo.findById(persistedOrganisation.getId()).get();
        Project newProject = setupProject();

        newProject.addOrganisation(dbOrganisation);
        dbOrganisation.addProject(newProject);

        projectRepo.save(newProject);

        entityManager.flush();
        entityManager.clear();

        Collection<Project> dbProjects = projectRepo.findAll();

        assertEquals(1,dbProjects.size());
        assertTrue(dbProjects.parallelStream().allMatch(t -> t.getId() > 0), "Not all tags were persisted!" );

        //test if relationship is properly persisted:
        Set<Organisation> assignedDbOrganisations = new HashSet<>();

        //get projects that are assigned to tags in db:
        dbProjects.forEach(t -> assignedDbOrganisations.addAll(t.getOrganisations()));

        assertEquals(1,assignedDbOrganisations.size());
        assertTrue(assignedDbOrganisations.parallelStream().allMatch(o -> o.getId() == persistedOrganisation.getId()));

        //test if same relationship is accessible from other direction:
        List<Long> dbOrganisationProjectIds = orgRepo.findById(persistedOrganisation.getId()).get().getProjects().parallelStream().map(p -> p.getId()).collect(Collectors.toList());
        assertEquals(1,dbOrganisationProjectIds.size());
        assertTrue(dbProjects.parallelStream().allMatch(t -> dbOrganisationProjectIds.contains(t.getId())));
    }

    @Test
    public void shouldAssignPersistedProjectsToOrganisation(){
        Project project = setupProject();
        projectRepo.save(project);

        Organisation organisation = setupOrganisation();
        organisation.addProject(project);
        project.addOrganisation(organisation);
        orgRepo.save(organisation);

        Organisation dbOrganisation = orgRepo.findById(organisation.getId()).get();
        assertEquals(1,dbOrganisation.getProjects().size());

        Project dbProject = projectRepo.findById(project.getId()).get();
        assertEquals(1,dbProject.getOrganisations().size());
    }

    @Test
    public void ShouldNotDeleteOrganisationOnProjectDelete(){
        Project project = setupProject();
        Organisation organisation = setupOrganisation();
        orgRepo.save(organisation);

        project.addOrganisation(organisation);
        organisation.addProject(project);
        projectRepo.save(project);

        entityManager.flush();
        entityManager.clear();

        projectRepo.deleteById(project.getId());
        assertEquals(1, orgRepo.findAll().size());
    }

    private Organisation setupOrganisation(){
        return setupOrganisation("WHO");
    }

    private Organisation setupOrganisation(String name){
        return Organisation.builder().name(name).description("World Health Organisation").logo("https://www.web.de".getBytes()).website("http://www.who.int/").build();
    }

    private Project setupProject(String name){
        return Project.builder()
            .description("#freedomForCorona")
            .from(LocalDate.now())
            .to(LocalDate.now().plusDays(1))
            .imageUrl("http://www.google.de/a")
            .location(new Location("Strasse","1","12345"))
            .name(name)
            .projectLink("http://www.google.de")
            .build();
    }

    private Project setupProject(){
        return setupProject("#FreeCorona");
    }
}
