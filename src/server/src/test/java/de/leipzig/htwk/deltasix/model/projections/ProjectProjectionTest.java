package de.leipzig.htwk.deltasix.model.projections;

import de.leipzig.htwk.deltasix.DataGenerator;
import de.leipzig.htwk.deltasix.dataaccess.CategoryRepository;
import de.leipzig.htwk.deltasix.dataaccess.OrganisationRepository;
import de.leipzig.htwk.deltasix.dataaccess.ProjectRepository;
import de.leipzig.htwk.deltasix.dataaccess.TagRepository;
import de.leipzig.htwk.deltasix.model.Category;
import de.leipzig.htwk.deltasix.model.Organisation;
import de.leipzig.htwk.deltasix.model.Project;
import de.leipzig.htwk.deltasix.model.Tag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@Transactional
public class ProjectProjectionTest {

    public final String PROJECTS_URL = "http://localhost/api/projects/";
    public final String CONTENT_TYPE = "application/hal+json";

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ProjectRepository testRepo;

    @Autowired
    OrganisationRepository orgRepo;

    @Autowired
    TagRepository tagRepo;

    @Autowired
    CategoryRepository categoryRepository;

    Project testProject;
    Organisation testOrganisation;
    Tag testTag;
    Category testCategory;

    @BeforeEach
    void setUp(){
        testProject = DataGenerator.getProject();
        testOrganisation = DataGenerator.getOrganisation();

        orgRepo.save(testOrganisation);

        testTag = Tag.builder()
                .text("legendary text")
                .build();

        tagRepo.save(testTag);

        testCategory = Category.builder()
                .categoryName("legendary name")
                .build();

        categoryRepository.save(testCategory);

        testProject
                .addOrganisation(testOrganisation.addProject(testProject))
                .addCategory(testCategory.addProject(testProject))
                .addTag(testTag.addProject(testProject));

        testRepo.save(testProject);
    }

    @Test
    public void shouldGetProjectionData() throws Exception {

        int testSize = 1;

        this.mockMvc.perform(get("/api/projects?projection=withTagsCategoriesOrganisationsAndInterviews"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE))
                .andExpect(jsonPath("$._embedded.projects",hasSize(testSize)))
                .andExpect(jsonPath("$._embedded.projects[0].name").value(equalTo(testProject.getName())))
                .andExpect(jsonPath("$._embedded.projects[0]._links.tags.href")
                        .value(equalTo(PROJECTS_URL+testProject.getId().toString()+"/tags")))
                .andExpect(jsonPath("$._embedded.projects[0]._links.categories.href")
                        .value(equalTo(PROJECTS_URL+testProject.getId().toString()+"/categories")))
                .andExpect(jsonPath("$._embedded.projects[0]._links.organisations.href")
                        .value(equalTo(PROJECTS_URL+testProject.getId().toString()+"/organisations{?projection}")))
                .andExpect(jsonPath("$._embedded.projects[0].tags[0].text")
                        .value(equalTo(testTag.getText())))
                .andExpect(jsonPath("$._embedded.projects[0].categories[0].categoryName")
                        .value(equalTo(testCategory.getCategoryName())));
    }

    @Test
    public void shouldGetCorrectTags () throws Exception{
        this.mockMvc.perform(get(PROJECTS_URL+testProject.getId().toString()+"/tags"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE))
                .andExpect(jsonPath("$._embedded.tags[0].text").value(equalTo(testTag.getText())));
    }

    @Test
    public void shouldGetCorrectCategories() throws Exception{
        this.mockMvc.perform(get(PROJECTS_URL+testProject.getId()+"/categories"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE))
                .andExpect(jsonPath("$._embedded.categories[0].categoryName").value(equalTo(testCategory.getCategoryName())));
    }

    @Test
    public void shouldGetCorrectOrganisations() throws Exception{
        this.mockMvc.perform(get(PROJECTS_URL+testProject.getId().toString()+"/organisations"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE))
                .andExpect(jsonPath("$._embedded.organisations[0].name").value(equalTo(testOrganisation.getName())));
    }
}
