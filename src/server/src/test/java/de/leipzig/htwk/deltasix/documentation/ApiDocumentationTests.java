package de.leipzig.htwk.deltasix.documentation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@SpringBootTest("spring.profiles.active=test")
@Transactional
public class ApiDocumentationTests {

    private final static String API_URL = "/api/";
    private MockMvc mockMvc;

    @BeforeEach()
    private void setUp(WebApplicationContext context, RestDocumentationContextProvider documentation){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(documentationConfiguration(documentation)).build();
    }

    @Test
    void apiRoot() throws Exception{
        this.mockMvc.perform(get(API_URL).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("api/get", links(halLinks(),
                        linkWithRel("projects").description("Link zu Projekten. Bitte beim GET mit ?projection=withTagsAndCategories versehen"),
                        linkWithRel("organisations").description("Link zu Organisationen. Bitte beim GET mit ?projection=withTagsAndCategories versehen"),
                        linkWithRel("profile").ignored(),
                        linkWithRel("categories").description("Link zu Kategorien"),
                        linkWithRel("interviews").description("Link zu Interviews"),
                        linkWithRel("tags").description("Link zu Tags")
                ), responseFields(
                        subsectionWithPath("_links").ignored()
                )));
    }
}
