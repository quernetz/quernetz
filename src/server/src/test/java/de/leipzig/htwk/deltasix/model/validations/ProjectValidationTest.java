package de.leipzig.htwk.deltasix.model.validations;

import de.leipzig.htwk.deltasix.DataGenerator;
import de.leipzig.htwk.deltasix.dataaccess.CategoryRepository;
import de.leipzig.htwk.deltasix.dataaccess.OrganisationRepository;
import de.leipzig.htwk.deltasix.dataaccess.ProjectRepository;
import de.leipzig.htwk.deltasix.dataaccess.TagRepository;
import de.leipzig.htwk.deltasix.model.Category;
import de.leipzig.htwk.deltasix.model.Organisation;
import de.leipzig.htwk.deltasix.model.Project;
import de.leipzig.htwk.deltasix.model.Tag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.validation.ConstraintViolationException;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class ProjectValidationTest {

    @Autowired
    OrganisationRepository organisationRepository;

    @Autowired
    EntityManager entityManager;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    CategoryRepository categoryRepository;

    Category testCategory;
    Tag testTag;
    Organisation testOrganisation;
    Project testProject;

    @BeforeEach
    public void setUp(){
        testTag = Tag.builder().organisations(new HashSet<>()).projects(new HashSet<>()).text("#GeilerText").build();
        testCategory = Category.builder().categoryName("Geiler Name").organisations(new HashSet<>()).projects(new HashSet<>()).build();
        testOrganisation = DataGenerator.getOrganisation();
        testProject = DataGenerator.getProject();

        categoryRepository.save(testCategory);
        organisationRepository.save(testOrganisation);
        tagRepository.save(testTag);

    }

    @Test
    public void shouldBeValid(){
        projectRepository.save(testProject);
        assertTrue(entityManager.contains(testProject));
    }

    @Test
    public void shouldNotBeValid_blankName(){
          testProject.setName("");
          projectRepository.save(testProject);
          assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    public void shouldNotBeValid_nullLocation(){
        testProject.setLocation(null);
        projectRepository.save(testProject);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }
    
    @Test
    public void shouldNotBeValid_linkNoURL(){
        testProject.setProjectLink("keine URL");
        projectRepository.save(testProject);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    public void shouldNotBeValid_nullToDate(){
        testProject.setTo(null);
        projectRepository.save(testProject);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }
}
