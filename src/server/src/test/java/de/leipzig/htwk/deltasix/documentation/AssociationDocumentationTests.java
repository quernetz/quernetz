package de.leipzig.htwk.deltasix.documentation;

import de.leipzig.htwk.deltasix.DataGenerator;
import de.leipzig.htwk.deltasix.dataaccess.CategoryRepository;
import de.leipzig.htwk.deltasix.dataaccess.OrganisationRepository;
import de.leipzig.htwk.deltasix.dataaccess.ProjectRepository;
import de.leipzig.htwk.deltasix.dataaccess.TagRepository;
import de.leipzig.htwk.deltasix.model.Category;
import de.leipzig.htwk.deltasix.model.Organisation;
import de.leipzig.htwk.deltasix.model.Project;
import de.leipzig.htwk.deltasix.model.Tag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.persistence.EntityManager;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@SpringBootTest("spring.profiles.active=test")
@Transactional
public class AssociationDocumentationTests {

    @Autowired
    TagRepository tagRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    OrganisationRepository organisationRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    EntityManager entityManager;

    private MockMvc mockMvc;

    private final String jwt = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJSYWxmIiwiZXhwIjoxNTkyNDEzOTY5LCJpYXQiOjE1OTIzOTU5Njl9.HB3iy1BkGoY3sdIQ1As0H_5QyzsASsMKUdKXGT0XO4Q";

    @BeforeEach
    void setup(WebApplicationContext context, RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(documentationConfiguration(restDocumentation)).build();
    }

    @Test
    void deleteProjectTagAssociation() throws Exception {
        Tag tag = Tag.builder().text("#hansJürgen").build();
        Project project = DataGenerator.getProject();
        Organisation organisation = DataGenerator.getOrganisation();

        tagRepository.save(tag);
        organisationRepository.save(organisation);

        project.addOrganisation(organisation.addProject(project)).addTag(tag.addProject(project));

        projectRepository.save(project);

        this.mockMvc.perform(
                delete("/api/projects/{project-id}/tags/{tag-id}", project.getId(), tag.getId())
                        .header("Authorization","Bearer "+jwt)
                ).andExpect(status().is2xxSuccessful())
                .andDo(document("associations/delete-project-tag",
                        pathParameters(
                                parameterWithName("project-id").description("Projekt-ID der zu löschenden Relation"),
                                parameterWithName("tag-id").description("Tag-ID der zu löschenden Relation")
                        )));
        Project dbProject = projectRepository.findById(project.getId()).get();
        assertTrue(dbProject.getTags().isEmpty());
    }

    @Test
    void addTagToProject() throws Exception {
        Tag tag = Tag.builder().text("#hansJürgenNow").build();
        Project project = DataGenerator.getProject();
        Organisation organisation = DataGenerator.getOrganisation();
        project.addOrganisation(organisation);
        organisation.addProject(project);
        projectRepository.save(project);
        tagRepository.save(tag);

        this.mockMvc.perform(patch("/api/projects/{project-id}/tags", project.getId(), tag.getId())
                .content("tags/" + tag.getId()).contentType("text/uri-list")
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/add-tag-to-project",
                        pathParameters(parameterWithName("project-id").description("ID des Projektes, dem Tags hinzugefügt werden sollen"))));
        Project dbProject = projectRepository.findById(project.getId()).get();
        assertEquals(1, dbProject.getTags().size());
    }

    @Test
    void overwriteProjectTagAssociation() throws Exception {
        Tag tag = Tag.builder().text("#heinrich").build();
        Tag tagNew = Tag.builder().text("#warMalHeinrich").build();
        Project project = DataGenerator.getProject().addTag(tag);
        Organisation organisation = DataGenerator.getOrganisation().addProject(project);
        project.addOrganisation(organisation);

        projectRepository.save(project);
        tagRepository.save(tagNew);

        this.mockMvc.perform(put("/api/projects/{project-id}/tags", project.getId())
                .contentType("text/uri-list")
                .content("tags/" + tagNew.getId())
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/overwrite-tags-to-project",
                        pathParameters(
                                parameterWithName("project-id").description("ID des Projekts, dessen verknüpfte Tags überschrieben werden sollen"))));
        Project dbProject = projectRepository.findById(project.getId()).get();
        assertTrue(dbProject.getTags().contains(tagNew));
    }

    @Test
    void addProjectToTag() throws Exception {
        Tag tag = Tag.builder().text("#mampf").build();

        Project project = DataGenerator.getProject();

        Organisation organisation = DataGenerator.getOrganisation();
        organisationRepository.save(organisation);

        project.addOrganisation(organisation.addProject(project));

        projectRepository.save(project);
        tagRepository.save(tag);

        this.mockMvc.perform(put("/api/tags/{tag-id}/projects", tag.getId())
                .content("projects/" + project.getId())
                .contentType("text/uri-list")
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/add-project-to-tag",
                        pathParameters(
                                parameterWithName("tag-id").description("ID des Tags, dem Projekte hinzugefügt werden sollen")
                        )));
        entityManager.flush();
        entityManager.clear();
        Project dbProject = projectRepository.findById(project.getId()).get();
        assertTrue(dbProject.getTags().size() == 1);
    }

    @Test
    void deleteProjectCategoryAssociation() throws Exception {
        Category category = Category.builder().categoryName("Umwelt").build();
        Project project = DataGenerator.getProject();
        Organisation organisation = DataGenerator.getOrganisation();

        organisationRepository.save(organisation);
        categoryRepository.save(category);

        project.addOrganisation(organisation.addProject(project)).addCategory(category.addProject(project));

        projectRepository.save(project);

        this.mockMvc.perform(delete("/api/projects/{project-id}/categories/{category-id}", project.getId(), category.getId())
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/delete-project-category-association",
                        pathParameters(
                                parameterWithName("project-id").description("ID des Projektes, das an der zu löschenden Assoziation teilnimmt"),
                                parameterWithName("category-id").description("ID der Kategorie die an der Assoziation teilnimmt")
                        )));
        Project dbProject = projectRepository.findById(project.getId()).get();
        assertTrue(dbProject.getCategories().isEmpty());
    }

    @Test
    void addCategoryToProject() throws Exception {
        Category category = Category.builder().categoryName("x").build();
        Project project = DataGenerator.getProject();
        Organisation organisation = DataGenerator.getOrganisation();
        project.addOrganisation(organisation);
        organisation.addProject(project);

        projectRepository.save(project);
        categoryRepository.save(category);

        this.mockMvc.perform(patch("/api/projects/{project-id}/categories", project.getId())
                .contentType("text/uri-list").content("categories/" + category.getId())
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/add-category-to-project",
                        pathParameters(
                                parameterWithName("project-id").description("ID des Projektes, dem Kategorien hinzugefügt werden sollen")
                        )));
        Project dbProject = projectRepository.findById(project.getId()).get();
        assertEquals(1, dbProject.getCategories().size());
    }

    @Test
    void addProjectToCategory() throws Exception {
        Category category = Category.builder().categoryName("Hello").build();
        Project project = DataGenerator.getProject();
        Organisation organisation = DataGenerator.getOrganisation();
        organisationRepository.save(organisation);
        project.addOrganisation(organisation.addProject(project));

        projectRepository.save(project);
        categoryRepository.save(category);

        this.mockMvc.perform(put("/api/categories/{category-id}/projects", category.getId())
                .contentType("text/uri-list")
                .content("projects/" + project.getId())
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/add-project-to-category",
                        pathParameters(parameterWithName("category-id").description("ID der Kategorie zu der die Projekte hinzugefügt werden sollen"))));
        Project dbProject = projectRepository.findById(project.getId()).get();
        entityManager.flush();
        entityManager.clear();
        assertEquals(1, dbProject.getCategories().size());
    }

    @Test
    void overwriteProjectCategoryAssociation() throws Exception {
        Category oldCategory = Category.builder().categoryName("hello").build();
        Category newCategory = Category.builder().categoryName("world").build();
        Project project = DataGenerator.getProject();
        Organisation organisation = DataGenerator.getOrganisation();

        project.addOrganisation(organisation.addProject(project)).addCategory(oldCategory.addProject(project));

        projectRepository.save(project);
        categoryRepository.save(newCategory);

        this.mockMvc.perform(put("/api/projects/{project-id}/categories", project.getId())
                .contentType("text/uri-list")
                .content("categories/" + newCategory.getId())
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/overwrite-project-category-association",
                        pathParameters(parameterWithName("project-id").description("ID des Projekts, dessen Kategorie-Zuordnungen überschrieben werden sollen"))
                ));

        Project dbProject = projectRepository.findById(project.getId()).get();
        assertEquals(1, dbProject.getCategories().size());
        assertTrue(dbProject.getCategories().contains(newCategory));
    }

    @Test
    void overwriteOrganisationTagAssociation() throws Exception {
        Tag t1 = Tag.builder().text("haJü").build();
        Tag t2 = Tag.builder().text("#eminentSwag").build();
        Tag t3 = Tag.builder().text("#newTag").build();

        Organisation organisation = DataGenerator.getOrganisation();
        organisation.addTag(t1.addOrganisation(organisation)).addTag(t2.addOrganisation(organisation));
        organisationRepository.save(organisation);
        tagRepository.save(t3);

        this.mockMvc.perform(put("/api/organisations/{organisation-id}/tags", organisation.getId())
                .contentType("text/uri-list")
                .content("tags/" + t3.getId())
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/overwrite-organisation-tag-association",
                        pathParameters(parameterWithName("organisation-id").description("ID der Organisation, deren Tag-Zuordnung überschrieben werden soll"))));

        Organisation dbOrganisation = organisationRepository.findById(organisation.getId()).get();
        assertEquals(1, dbOrganisation.getTags().size());
        assertTrue(dbOrganisation.getTags().contains(t3));
    }

    @Test
    void deleteOrganisationTagAssociation() throws Exception {
        Tag x = Tag.builder().text("x").build();
        tagRepository.save(x);
        Organisation organisation = DataGenerator.getOrganisation();
        organisationRepository.save(organisation.addTag(x.addOrganisation(organisation)));

        this.mockMvc.perform(delete("/api/organisations/{organisation-id}/tags/{tag-id}", organisation.getId(), x.getId())
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/delete-organisation-tag-association", pathParameters(
                        parameterWithName("organisation-id").description("Organisations-ID der zu löschenden Assoziation"),
                        parameterWithName("tag-id").description("Tag-ID der zu löschenden Assoziation")
                )));
        Organisation dbOrganisation = organisationRepository.findById(organisation.getId()).get();
        assertTrue(dbOrganisation.getTags().isEmpty());
    }

    @Test
    void addTagsToOrganisation() throws Exception {
        Tag tag = Tag.builder().text("#yolo").build();
        Organisation organisation = DataGenerator.getOrganisation();
        tagRepository.save(tag);
        organisationRepository.save(organisation);

        this.mockMvc.perform(patch("/api/organisations/{organisation-id}/tags", organisation.getId())
                .contentType("text/uri-list")
                .content("tags/" + tag.getId())
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/add-tag-to-organisation",
                        pathParameters(parameterWithName("organisation-id").description("ID der Organisation zu der die Tags hizugefügt werden sollen"))));

        Organisation dbOrganisation = organisationRepository.findById(organisation.getId()).get();
        assertEquals(1, dbOrganisation.getTags().size());
    }

    @Test
    void addOrganisationsToTag() throws Exception {
        Organisation organisation = DataGenerator.getOrganisation();
        Tag tag = Tag.builder().text("#lol").build();
        tagRepository.save(tag);
        organisationRepository.save(organisation);

        this.mockMvc.perform(put("/api/tags/{tag-id}/organisations", tag.getId())
                .contentType("text/uri-list")
                .content("organisations/" + organisation.getId())
                .header("Authorization","Bearer "+jwt))
                .andDo(document("associations/add-organisations-to-tags",
                        pathParameters(parameterWithName("tag-id").description("ID des Tags zu dem die Organisation(en) hinzugefügt werden sollen"))));

        entityManager.flush();
        entityManager.clear();
        Organisation dbOrganisation = organisationRepository.findById(organisation.getId()).get();
        assertEquals(1, dbOrganisation.getTags().size());
        assertTrue(dbOrganisation.getTags().stream().allMatch(t -> t.getText().equals("#lol")));
    }

    @Test
    void addCategoriesToOrganisation() throws Exception {
        Organisation organisation = DataGenerator.getOrganisation();
        Category category = Category.builder().categoryName("ulf").build();
        organisationRepository.save(organisation);
        categoryRepository.save(category);

        this.mockMvc.perform(patch("/api/organisations/{organisation-id}/categories", organisation.getId())
                .contentType("text/uri-list").content("categories/" + category.getId())
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/add-categories-to-organisation",
                        pathParameters(parameterWithName("organisation-id")
                                .description("ID der Organisation zu der die Kategorien hinzugefügt werden soll"))));
        Organisation dbOrg = organisationRepository.findById(organisation.getId()).get();
        assertTrue(dbOrg.getCategories().contains(category));
    }

    @Test
    void addOrganisationsToCategories() throws Exception {
        Organisation organisation = DataGenerator.getOrganisation();
        Category category = Category.builder().categoryName("Hello World").build();

        organisationRepository.save(organisation);
        categoryRepository.save(category);

        this.mockMvc.perform(put("/api/categories/{category-id}/organisations", category.getId())
                .content("organisations/" + organisation.getId())
                .contentType("text/uri-list")
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/add-organisation-to-category", pathParameters(
                        parameterWithName("category-id").description("ID der Kategorie, zu der Organisationen hinzugefügt werden sollen")
                )));
        entityManager.flush();
        entityManager.clear();
        Organisation dbOrganisation = organisationRepository.findById(organisation.getId()).get();
        assertEquals(1, dbOrganisation.getCategories().size());
        assertTrue(dbOrganisation.getCategories().stream().allMatch(c -> c.getCategoryName().equals("Hello World")));
    }

    @Test
    void deleteCategoryOrganisationAssociation() throws Exception {
        Category category = Category.builder().categoryName("Hello").build();
        categoryRepository.save(category);
        Organisation organisation = DataGenerator.getOrganisation();
        organisationRepository.save(organisation.addCategory(category.addOrganisation(organisation)));

        this.mockMvc.perform(delete("/api/categories/{category-id}/organisations/{organisation-id}/", category.getId(), organisation.getId()))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/delete-organisation-category-association",
                        pathParameters(
                                parameterWithName("organisation-id").description("ID der Organisation, die an der zu löschenden Relation teilnimmt"),
                                parameterWithName("category-id").description("ID der Kategorie, die an der zu löschenden Relation teilnimmt")
                        )));

        entityManager.flush();
        entityManager.clear();

        Organisation dbOrganisation = organisationRepository.findById(organisation.getId()).get();
        assertTrue(dbOrganisation.getCategories().isEmpty());
    }

    @Test
    void overwriteOrganisationCategoryAssociation() throws Exception {
        Category old = Category.builder().categoryName("old").build();
        Category newC = Category.builder().categoryName("new").build();
        Organisation organisation = DataGenerator.getOrganisation();
        organisationRepository.save(organisation.addCategory(old.addOrganisation(organisation)));
        categoryRepository.save(newC);

        this.mockMvc.perform(put("/api/organisations/{organisation-id}/categories", organisation.getId())
                .contentType("text/uri-list")
                .content("category/" + newC.getId())
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/overwrite-organisation-category-association", pathParameters(
                        parameterWithName("organisation-id").description("ID der Organisation, deren verknüpfte Kategorien überschrieben werden sollen")
                )));
        Organisation dbOrganisation = organisationRepository.findById(organisation.getId()).get();
        assertTrue(dbOrganisation.getCategories().contains(newC));
        assertEquals(1, dbOrganisation.getCategories().size());
    }

    @Test
    void addProjectToOrganisation() throws Exception{
        Project p        = DataGenerator.getProject();
        Organisation a   = DataGenerator.getOrganisation();
        Organisation b   = DataGenerator.getOrganisation();

        organisationRepository.save(a);
        organisationRepository.save(b);

        projectRepository.save(p.addOrganisation(a.addProject(p)));

        this.mockMvc.perform(patch("/api/projects/{project-id}/organisations", p.getId())
                .contentType("text/uri-list")
                .content("organisations/"+b.getId())
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/add-project-to-organisation",
                        pathParameters(parameterWithName("project-id").description("ID des Projektes zu dem die Organisation hinzugefügt werden soll."))));
        entityManager.flush();
        entityManager.clear();

        Project dbProject = projectRepository.findById(p.getId()).get();
        Organisation dbOrg = organisationRepository.findById(b.getId()).get();
        assertEquals(2, dbProject.getOrganisations().size());
        assertEquals(1, dbOrg.getProjects().size());
    }

    @Test
    void addOrganisationsToProject() throws Exception {
        Project project = DataGenerator.getProject();
        Organisation organisation = DataGenerator.getOrganisation();
        projectRepository.save(project);
        organisationRepository.save(organisation);

        this.mockMvc.perform(put("/api/projects/{project-id}/organisations", project.getId())
                .contentType("text/uri-list")
                .content("organisations/" + organisation.getId())
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/add-organisation-to-project",
                        pathParameters(parameterWithName("project-id").description("ID des Projektes, zu dem die Organisation(en) hinzugefügt werden sollen"))));
        Project dbProject = projectRepository.findById(project.getId()).get();
        assertEquals(1, dbProject.getOrganisations().size());
    }

    @Test
    void overwriteAssociatedOrganisations() throws Exception {
        Project project = DataGenerator.getProject();
        Organisation oldOrganisation = DataGenerator.getOrganisation();
        Organisation newOrganisation = DataGenerator.getOrganisation();

        organisationRepository.saveAll(List.of(oldOrganisation, newOrganisation));

        projectRepository.save(project.addOrganisation(oldOrganisation.addProject(project)));

        this.mockMvc.perform(put("/api/projects/{project-id}/organisations", project.getId())
                .contentType("text/uri-list")
                .content("organisations/" + newOrganisation.getId().toString())
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/overwrite-project-organisation-association", pathParameters(
                        parameterWithName("project-id").description("ID der Organisation, deren verknüpften Projekte überschrieben werden sollen")
                )));

        entityManager.flush();
        entityManager.clear();

        Organisation dbOrganisation = organisationRepository.findById(newOrganisation.getId()).get();
        assertEquals(1, dbOrganisation.getProjects().size());
        assertTrue(dbOrganisation.getProjects().contains(project));
    }

    @Test
    void deleteProjectOrganisationAssociation() throws Exception {
        Project project = DataGenerator.getProject();
        Organisation organisation = DataGenerator.getOrganisation();
        Organisation organisation2 = DataGenerator.getOrganisation();
        organisationRepository.saveAll(List.of(organisation,organisation2));

        this.projectRepository.save(project.addOrganisation(organisation.addProject(project)).addOrganisation(organisation2.addProject(project)));

        this.mockMvc.perform(delete("/api/projects/{project-id}/organisations/{organisation-id}", project.getId(), organisation.getId())
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("associations/delete-project-organisation-association", pathParameters(
                        parameterWithName("project-id").description("ID des Projektes, dessen Verknüpfung zur entsprechenden Organisation gelöscht werden soll"),
                        parameterWithName("organisation-id").description("ID der Organisation, deren Verknüpfung zum entsprechenden Projekt gelöscht werden soll")
                )));
        entityManager.flush();
        entityManager.clear();
        Project dbProject = this.projectRepository.findById(project.getId()).get();
        assertTrue(dbProject.getOrganisations().size()==1);
    }
}
