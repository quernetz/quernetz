package de.leipzig.htwk.deltasix.service;

import de.leipzig.htwk.deltasix.dataaccess.OrganisationRepository;
import de.leipzig.htwk.deltasix.model.Organisation;
import javassist.NotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class OrganisationLogoStorageServiceTest {

    @Test
    public void saveImagePutTest() throws NotFoundException, IOException {
        Organisation organisation = Mockito.mock(Organisation.class);
        MultipartFile image = Mockito.mock(MultipartFile.class);
        OrganisationRepository organisationRepository = Mockito.mock(OrganisationRepository.class);
        Mockito.when(organisationRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(organisation));
        OrganisationLogoStorageService organisationLogoStorageService = new OrganisationLogoStorageService(organisationRepository, null);

        organisationLogoStorageService.saveImage(image,"42");

        Assertions.assertEquals(image.getBytes(), organisation.getLogo());
    }

    @Test
    public void saveImageDeleteTest() throws NotFoundException, IOException {
        Organisation organisation = Mockito.mock(Organisation.class);
        OrganisationRepository organisationRepository = Mockito.mock(OrganisationRepository.class);
        Mockito.when(organisationRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(organisation));
        OrganisationLogoStorageService organisationLogoStorageService = new OrganisationLogoStorageService(organisationRepository, null);

        organisationLogoStorageService.saveImage(null,"42");

        Assertions.assertNull(organisation.getLogo());
    }

    @Test
    public void saveImageOrganisationNotFoundTest() {
        OrganisationRepository organisationRepository = Mockito.mock(OrganisationRepository.class);
        Mockito.when(organisationRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
        OrganisationLogoStorageService organisationLogoStorageService = new OrganisationLogoStorageService(organisationRepository, null);

        Assertions.assertThrows(NotFoundException.class, () -> organisationLogoStorageService.saveImage(null,"42"));
    }

    @Test
    public void loadImageSuccsessTest() throws IOException, NotFoundException {
        byte[] bytes = {1,2,3};
        Organisation organisation = Mockito.mock(Organisation.class);
        Mockito.when(organisation.getLogo()).thenReturn(bytes);
        OrganisationRepository organisationRepository = Mockito.mock(OrganisationRepository.class);
        Mockito.when(organisationRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(organisation));
        File file = Mockito.mock(File.class);
        LogoWriter logoWriter = Mockito.mock(LogoWriter.class);
        Mockito.when(logoWriter.writeLogo(bytes)).thenReturn(file);
        OrganisationLogoStorageService organisationLogoStorageService = new OrganisationLogoStorageService(organisationRepository, logoWriter);

        File loadedImage = organisationLogoStorageService.loadImage("42");

        Assertions.assertEquals(file, loadedImage);
    }

    @Test
    public void loadImageOrganisationNotFoundTest(){
        OrganisationRepository organisationRepository = Mockito.mock(OrganisationRepository.class);
        Mockito.when(organisationRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
        OrganisationLogoStorageService organisationLogoStorageService = new OrganisationLogoStorageService(organisationRepository, null);

        Assertions.assertThrows(NotFoundException.class, () -> organisationLogoStorageService.loadImage("42"));
    }

    @Test
    public void loadImageLogoLengthZeroTest(){
        byte[] bytes = {};
        Organisation organisation = Mockito.mock(Organisation.class);
        Mockito.when(organisation.getLogo()).thenReturn(bytes);
        OrganisationRepository organisationRepository = Mockito.mock(OrganisationRepository.class);
        Mockito.when(organisationRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(organisation));
        OrganisationLogoStorageService organisationLogoStorageService = new OrganisationLogoStorageService(organisationRepository, null);

        Assertions.assertThrows(NotFoundException.class, () -> organisationLogoStorageService.loadImage("42"));
    }
    @Test
    public void loadImageLogoNullTest(){
        Organisation organisation = Mockito.mock(Organisation.class);
        Mockito.when(organisation.getLogo()).thenReturn(null);
        OrganisationRepository organisationRepository = Mockito.mock(OrganisationRepository.class);
        Mockito.when(organisationRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(organisation));
        OrganisationLogoStorageService organisationLogoStorageService = new OrganisationLogoStorageService(organisationRepository, null);

        Assertions.assertThrows(NotFoundException.class, () -> organisationLogoStorageService.loadImage("42"));
    }
}
