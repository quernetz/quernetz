package de.leipzig.htwk.deltasix.model.validations;

import de.leipzig.htwk.deltasix.DataGenerator;
import de.leipzig.htwk.deltasix.dataaccess.InterviewRepository;
import de.leipzig.htwk.deltasix.dataaccess.OrganisationRepository;
import de.leipzig.htwk.deltasix.model.Interview;
import de.leipzig.htwk.deltasix.model.Organisation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.validation.ConstraintViolationException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class InterviewValidationTest {

    @Autowired
    EntityManager entityManager;

    @Autowired
    InterviewRepository interviewRepository;

    @Autowired
    OrganisationRepository organisationRepository;

    Organisation testOrganisation;
    Interview testInterview;

    @BeforeEach
    public void setUp(){
        testOrganisation = DataGenerator.getOrganisation();
        organisationRepository.save(testOrganisation);

        testInterview = DataGenerator.getInterview();
        testOrganisation = DataGenerator.getOrganisation();
    }

    @Test
    public void shouldBeValid() {
        interviewRepository.save(testInterview);
        assertTrue(entityManager.contains(testInterview));
    }

    @Test
    public void shouldNotBeValid_blankName(){
        testInterview.setName("");
        interviewRepository.save(testInterview);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    public void shouldNotBeValid_blankDescription(){
        testInterview.setDescription("");
        interviewRepository.save(testInterview);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    public void shouldNotBeValid_linkNotAURL(){
        testInterview.setLink("keine URL");
        interviewRepository.save(testInterview);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    public void shouldNotBeValid_youtubeLinkNotAURL(){
        testInterview.setYoutubeLink("keine URL");
        interviewRepository.save(testInterview);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }
}
