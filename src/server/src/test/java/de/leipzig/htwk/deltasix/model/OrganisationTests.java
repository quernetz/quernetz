package de.leipzig.htwk.deltasix.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class OrganisationTests {

    private Organisation testObject;
    private String testName         = "Die Müllfreunde";
    private List<String> testFields = new ArrayList<String>(List.of("Abfall", "Umwelt"));

    @BeforeEach
    public void createTestObject() {
        testObject = Organisation.builder()
                .name(testName)
                .description("Hello")
                .logo("Test".getBytes())
                .website("https://welcome.to.corona.jungle")
                .build();
    }

    @Test
    public void setsShouldBeUnmodifiable(){
        assertThrows(UnsupportedOperationException.class, () -> testObject.getTags().add(null));
        assertThrows(UnsupportedOperationException.class, () -> testObject.getCategories().add(null));
        assertThrows(UnsupportedOperationException.class, () -> testObject.getInterviews().add(null));
        assertThrows(UnsupportedOperationException.class, () -> testObject.getProjects().add(null));
    }

    @ParameterizedTest
    @ValueSource(booleans = {true,false})
    public void addsAndRemovesShouldBehaveCorrectly(boolean isSingle){
        Tag tag             = Tag.builder().text("#hansJürgen").build();
        Interview interview = Interview.builder().description("Hupps").name("#Test").link(":sss").build();
        Project project     = Project
                .builder()
                .projectLink("...").description("...")
                .from(LocalDate.now()).to(LocalDate.now().plusDays(1))
                .name("Project").build();
        Category category = Category.builder().categoryName("Catg 1").build();

        if(isSingle){
            testObject.addTag(tag);
            testObject.addProject(project);
            testObject.addCategory(category);
            testObject.addInterview(interview);

            assertEquals(1, testObject.getTags().size());
            assertEquals(1, testObject.getProjects().size());
            assertEquals(1, testObject.getCategories().size());
            assertEquals(1, testObject.getInterviews().size());

            testObject.removeTag(tag);
            testObject.removeProject(project);
            testObject.removeCategory(category);
            testObject.removeInterview(interview);

            assertEquals(0, testObject.getTags().size());
            assertEquals(0, testObject.getProjects().size());
            assertEquals(0, testObject.getCategories().size());
            assertEquals(0, testObject.getInterviews().size());
        } else {
            testObject.addTags(List.of(tag));
            testObject.addProjects(List.of(project));
            testObject.addCategories(List.of(category));
            testObject.addInterviews(List.of(interview));

            assertEquals(1, testObject.getTags().size());
            assertEquals(1, testObject.getProjects().size());
            assertEquals(1, testObject.getCategories().size());
            assertEquals(1, testObject.getInterviews().size());

            testObject.removeTags(List.of(tag));
            testObject.removeProjects(List.of(project));
            testObject.removeCategories(List.of(category));
            testObject.removeInterviews(List.of(interview));

            assertEquals(0, testObject.getTags().size());
            assertEquals(0, testObject.getProjects().size());
            assertEquals(0, testObject.getCategories().size());
            assertEquals(0, testObject.getInterviews().size());
        }
    }
}