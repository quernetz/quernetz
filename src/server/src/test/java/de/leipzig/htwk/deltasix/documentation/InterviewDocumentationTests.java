package de.leipzig.htwk.deltasix.documentation;

import com.fasterxml.jackson.databind.node.ObjectNode;
import de.leipzig.htwk.deltasix.DataGenerator;
import de.leipzig.htwk.deltasix.JSONHelper;
import de.leipzig.htwk.deltasix.dataaccess.InterviewRepository;
import de.leipzig.htwk.deltasix.dataaccess.OrganisationRepository;
import de.leipzig.htwk.deltasix.dataaccess.ProjectRepository;
import de.leipzig.htwk.deltasix.documentation.helpers.ConstrainedFieldHelper;
import de.leipzig.htwk.deltasix.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.hypermedia.LinkDescriptor;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.snippet.Attributes.key;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@SpringBootTest("spring.profiles.active=test")
@Transactional
public class InterviewDocumentationTests {

    private final static String API_URL = "/api/interviews/";

    @Autowired
    private InterviewRepository interviewRepository;

    @Autowired
    private OrganisationRepository organisationRepository;

    @Autowired
    private ProjectRepository projectRepository;

    private Organisation organisation;

    private Interview interview;

    private MockMvc mockMvc;

    private final String jwt = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJSYWxmIiwiZXhwIjoxNTkyNDEzOTY5LCJpYXQiOjE1OTIzOTU5Njl9.HB3iy1BkGoY3sdIQ1As0H_5QyzsASsMKUdKXGT0XO4Q";

    @BeforeEach
    void setup(WebApplicationContext context, RestDocumentationContextProvider documentation) {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(documentationConfiguration(documentation))
                .build();
        this.interview = DataGenerator.getInterview();
        this.organisation = DataGenerator.getOrganisation();

        this.organisation.addInterview(this.interview);

        this.interviewRepository.save(this.interview);
    }

    @Test
    void getAllInterviews() throws Exception {
        this.mockMvc.perform(get(API_URL).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("interviews/interviews-get",
                        responseFields(
                                subsectionWithPath("_embedded.interviews").description("Array mit allen gespeicherten Interviews"),
                                subsectionWithPath("page").ignored(),
                                subsectionWithPath("_links").ignored()
                        )
                ));
    }

    @Test
    public void interviewGet() throws Exception {
        List<FieldDescriptor> fields = getConstrainedInterviewFields();

        this.mockMvc
                .perform(get(API_URL + "{interview-id}", interview.getId())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("interviews/get-by-id",
                        responseFields(fields)));
    }

    @Test
    void createInterview() throws Exception {
        Interview newInterview = DataGenerator.getInterview();

        List<FieldDescriptor> fields = getConstrainedInterviewFields();
        fields.addAll(Arrays.asList(
                fieldWithPath("organisations").attributes(key("constraints").value("")).description("Organisationen die beim Interview beteiligt sind (als Array von Strings mit jew. diesem Format: `'organisations/{id}'`)")
        ));

        fields.removeIf(f -> f.getPath().equals("_links"));

        ObjectNode newInterviewNode = JSONHelper.toObjectNode(newInterview);

        LocalDate date = newInterview.getDate();

        newInterviewNode.put("date", date.toString());
        newInterviewNode.remove("id");
        newInterviewNode.putArray("organisations")
                .addAll(JSONHelper.getHyperlinkArray("organisations", interview.getOrganisations().stream().map(o -> o.getId()).collect(Collectors.toList())));

        this.mockMvc.perform(post(API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(JSONHelper.toJSON(newInterviewNode))
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + jwt))
                .andExpect(status().isCreated())
                .andDo(document("interview/post",
                        requestFields(fields)));
    }

    @Test
    void modifyInterview() throws Exception {
        String requestContent = this.mockMvc.perform(get(API_URL + this.interview.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ObjectNode modifiedInterview = JSONHelper.parseJsonStringToObjectNode(requestContent);
        modifiedInterview.remove("Id");
        modifiedInterview.remove("_links");
        modifiedInterview.put("description", "New description");

        List<FieldDescriptor> fields = getConstrainedInterviewFields();

        fields.removeIf(f -> f.getPath().equals("_links"));

        this.mockMvc.perform(patch(API_URL + "{interview-id}", interview.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(JSONHelper.toJSON(modifiedInterview))
                .header("Authorization", "Bearer " + jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("interview/modify-interviews",
                        links(halLinks(), getInterviewLinkDescriptors()),
                        pathParameters(parameterWithName("interview-id").description("ID des Interviews, das verändert werden soll")),
                        requestFields(fields))
                );
    }


    @Test
    void deletingInterviews() throws Exception {
        this.mockMvc.perform(delete(API_URL + "{interview-id}", this.interview.getId())
                .header("Authorization", "Bearer " + jwt)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("interviews/delete-interview",
                        pathParameters(parameterWithName("interview-id").description("ID des zu löschenden Interviews"))
                ));
    }

    private List<FieldDescriptor> getConstrainedInterviewFields() {
        ConstrainedFieldHelper.ConstrainedFields fields = ConstrainedFieldHelper.getConstrainedFields(Interview.class);
        return new ArrayList<FieldDescriptor>(Arrays.asList(
                fields.withPath("name").description("Name / Titel des Interviews"),
                fields.withPath("link").description("Link zu einer externen Website"),
                fields.withPath("youtubeLink").description("Link zum Youtube-Video des Interviews"),
                fields.withPath("date").description("Datum des Interviews. Das Datumsformat ist den Beispielen zu entnehmen"),
                fields.withPath("description").description("Beschreibung des Interviews"),
                subsectionWithPath("_links").ignored()
        ));
    }

    private List<LinkDescriptor> getInterviewLinkDescriptors() {
        return new ArrayList<>(Arrays.asList(
                linkWithRel("organisations").description("Verknüpfung zu den beteiligten Organisationen"),
                linkWithRel("interview").description("Verweis auf dieses Interview"),
                linkWithRel("self").ignored()
        ));
    }
}
