package de.leipzig.htwk.deltasix.documentation;

import com.fasterxml.jackson.databind.node.ObjectNode;
import de.leipzig.htwk.deltasix.JSONHelper;
import de.leipzig.htwk.deltasix.dataaccess.CategoryRepository;
import de.leipzig.htwk.deltasix.documentation.helpers.ConstrainedFieldHelper;
import de.leipzig.htwk.deltasix.model.Category;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.hypermedia.LinkDescriptor;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@SpringBootTest("spring.profiles.active=test")
@Transactional
public class CategoryDocumentationTests {

    private final static String API_URL = "/api/categories/";

    @Autowired
    CategoryRepository repo;

    private Category category;

    private MockMvc mockMvc;

    private final String jwt = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJSYWxmIiwiZXhwIjoxNTkyNDEzOTY5LCJpYXQiOjE1OTIzOTU5Njl9.HB3iy1BkGoY3sdIQ1As0H_5QyzsASsMKUdKXGT0XO4Q";

    @BeforeEach
    void setup(WebApplicationContext context, RestDocumentationContextProvider restDocumentation){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(documentationConfiguration(restDocumentation))
                .build();
        category = Category.builder().categoryName("dummyCat").build();
        repo.save(category);
    }

    @Test
    void getAllCategories() throws Exception {
        this.mockMvc.perform(get(API_URL).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("categories/get",
                        links(halLinks(),
                                linkWithRel("self").ignored(),
                                linkWithRel("profile").ignored()),
                        responseFields(
                                subsectionWithPath("_embedded.categories").description("Array aller Kategorien"),
                                subsectionWithPath("_links").ignored(),
                                subsectionWithPath("page").ignored())));
    }

    @Test
    void getCategoryById() throws Exception{
        this.mockMvc.perform(get(API_URL+"{category-id}",category.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("categories/get-by-id",
                        links(halLinks(), getCategoryLinks()),
                        responseFields(getConstrainedCategoryFields()),
                        pathParameters(parameterWithName("category-id").description("ID der zu ladenden Kategorie"))
                ));
    }

    @Test
    void deleteCategory() throws Exception{
        this.mockMvc.perform(delete(API_URL + "{category-id}", category.getId()).accept(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("categories/delete",
                        pathParameters(parameterWithName("category-id").description("ID der zu löschenden Kategorie"))));
    }

    @Test
    void createCategory() throws Exception{
        Category c = Category.builder().categoryName("Kategorie C").build();

        ObjectNode categoryNode = JSONHelper.toObjectNode(c);

        categoryNode.remove("id");
        categoryNode.remove("organisations");
        categoryNode.remove("projects");

        List<FieldDescriptor> requestFields = getConstrainedCategoryFields();
        requestFields.removeIf(f -> f.getPath().equals("_links"));
        this.mockMvc.perform(post(API_URL).accept(MediaType.APPLICATION_JSON)
                    .content(JSONHelper.toJSON(categoryNode))
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("categories/create",
                        links(halLinks(), getCategoryLinks()),
                        responseFields(getConstrainedCategoryFields()),
                        requestFields(requestFields)
                ));
    }

    @Test
    void modifyCategory() throws Exception{
        Category c = Category.builder().categoryName("Kategorie C war ein doofer Name!").build();
        ObjectNode categoryNode = JSONHelper.toObjectNode(c);
        categoryNode.remove("id");
        categoryNode.remove("projects");
        categoryNode.remove("organisations");
        List<FieldDescriptor> requestFields = getConstrainedCategoryFields();
        requestFields.removeIf(f -> f.getPath().equals("_links"));
        this.mockMvc.perform(patch(API_URL+"{category-id}",category.getId())
                .accept(MediaType.APPLICATION_JSON)
                .content(JSONHelper.toJSON(categoryNode))
                .header("Authorization","Bearer "+jwt))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("categories/modify",
                        pathParameters(parameterWithName("category-id").description("ID der zu verändernden Kategorie")),
                        responseFields(getConstrainedCategoryFields()),
                        requestFields(requestFields)
                ));
    }

    private List<FieldDescriptor> getConstrainedCategoryFields(){
        ConstrainedFieldHelper.ConstrainedFields fields = ConstrainedFieldHelper.getConstrainedFields(Category.class);
        return new ArrayList<FieldDescriptor>(Arrays.asList(
            fields.withPath("categoryName").description("Name der Kategorie"),
            subsectionWithPath("_links").ignored()
        ));
    }
    private List<LinkDescriptor> getCategoryLinks(){
        return new ArrayList<>(Arrays.asList(
            linkWithRel("projects").description("Alle Projekte zu dieser Kategorie"),
            linkWithRel("organisations").description("Alle Organisationen zu dieser Kategorie"),
            linkWithRel("category").ignored(),
            linkWithRel("self").ignored()
        ));
    }
}
