package de.leipzig.htwk.deltasix.model.projections;

import de.leipzig.htwk.deltasix.DataGenerator;
import de.leipzig.htwk.deltasix.dataaccess.CategoryRepository;
import de.leipzig.htwk.deltasix.dataaccess.OrganisationRepository;
import de.leipzig.htwk.deltasix.dataaccess.ProjectRepository;
import de.leipzig.htwk.deltasix.model.Category;
import de.leipzig.htwk.deltasix.model.Organisation;
import de.leipzig.htwk.deltasix.model.Project;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@Transactional
public class CategoryProjectionTest {

    public final String CONTENT_TYPE = "application/hal+json";
    public final String CATEGORY_URL = "http://localhost/api/categories/";

    @Autowired
    MockMvc mockMvc;

    @Autowired
    CategoryRepository testRepo;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    OrganisationRepository organisationRepository;

    Organisation testOrganisation;
    Project testProject;
    Category category;

    @BeforeEach
    void setUp(){
        category = Category.builder()
                .categoryName("Blub")
                .projects(new HashSet<>())
                .organisations(new HashSet<>())
                .build();

        testRepo.save(category);


        testOrganisation = DataGenerator.getOrganisation();

        organisationRepository.save(testOrganisation);

        testProject = DataGenerator.getProject();

        testProject
                .addOrganisation(testOrganisation
                        .addProject(testProject)
                        .addCategory(category
                                .addOrganisation(testOrganisation)
                                .addProject(testProject)));
        projectRepository.save(testProject);
    }

    @Test
    public void shouldGetProjectionData() throws Exception {

        int testSize = 1;

        this.mockMvc.perform(get("/api/categories?projection=categories"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE))
                .andExpect(jsonPath("$._embedded.categories",hasSize(testSize)))
                .andExpect(jsonPath("$._embedded.categories[0].categoryName").value(equalTo(category.getCategoryName())))
                .andExpect(jsonPath("$._embedded.categories[0]._links.organisations.href")
                        .value(equalTo(CATEGORY_URL+category.getId().toString()+"/organisations{?projection}")))
                .andExpect(jsonPath("$._embedded.categories[0]._links.projects.href")
                        .value(equalTo(CATEGORY_URL+category.getId().toString()+"/projects{?projection}")));
    }

    @Test
    public void shouldGetCorrectOrganisations() throws Exception{
        this.mockMvc.perform(get(CATEGORY_URL+category.getId().toString()+"/organisations"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE))
                .andExpect(jsonPath("$._embedded.organisations[0].name").value(equalTo(testOrganisation.getName())));
    }

    @Test
    public void shouldGetCorrectProjects() throws Exception{
        this.mockMvc.perform(get(CATEGORY_URL+category.getId().toString()+"/projects"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE))
                .andExpect(jsonPath("$._embedded.projects[0].name").value(equalTo(testProject.getName())));
    }
}

