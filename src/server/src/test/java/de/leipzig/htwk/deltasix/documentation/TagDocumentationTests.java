package de.leipzig.htwk.deltasix.documentation;

import com.fasterxml.jackson.databind.node.ObjectNode;
import de.leipzig.htwk.deltasix.JSONHelper;
import de.leipzig.htwk.deltasix.dataaccess.TagRepository;
import de.leipzig.htwk.deltasix.documentation.helpers.ConstrainedFieldHelper;
import de.leipzig.htwk.deltasix.model.Tag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.hypermedia.LinkDescriptor;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.snippet.Attributes.key;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith({SpringExtension.class, RestDocumentationExtension.class})
@SpringBootTest("spring.profiles.active=test")
@Transactional
public class TagDocumentationTests {

    private final static String API_URL = "/api/tags/";

    @Autowired
    TagRepository repo;

    private Tag tag;

    private MockMvc mockMvc;
    
    private final String jwt = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJSYWxmIiwiZXhwIjoxNTkyNDEzOTY5LCJpYXQiOjE1OTIzOTU5Njl9.HB3iy1BkGoY3sdIQ1As0H_5QyzsASsMKUdKXGT0XO4Q";

    @BeforeEach
    void setup(WebApplicationContext context, RestDocumentationContextProvider restDocumentation){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(documentationConfiguration(restDocumentation))
                .build();
        this.tag = Tag.builder().text("#helloHansJürgen").build();
        this.repo.save(tag);
    }

    @Test
    void getAllTags() throws  Exception{
        this.mockMvc.perform(get(API_URL).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("tags/get-all",
                    responseFields(
                        subsectionWithPath("_embedded.tags").description("Array mit allen vorhandenen Tags"),
                        subsectionWithPath("_links").ignored(),
                        subsectionWithPath("page").ignored()
                    )));
    }

    @Test
    void getTagById() throws Exception {
        this.mockMvc.perform(get(API_URL+"{tag-id}",tag.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("tags/get-by-id",
                        links(halLinks(),getTagLinks()),
                        responseFields(getConstrainedTagFields()),
                        pathParameters(parameterWithName("tag-id").description("ID des zu ladenden Tags"))
                ));
    }

    @Test
    void deleteTag() throws Exception {
        this.mockMvc.perform(get(API_URL+"{tag-id}",tag.getId()).accept(MediaType.APPLICATION_JSON)
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("tags/delete",
                        pathParameters(parameterWithName("tag-id").description("ID des zu löschenden Tags"))));
    }

    @Test
    void createTag () throws Exception{

        List<FieldDescriptor> responseFields = getConstrainedTagFields();
        List<FieldDescriptor> requestFields = getConstrainedTagFields();

        Tag tag = Tag.builder().text("#yesWeBeerCan").build();
        ObjectNode tagNode = JSONHelper.toObjectNode(tag);
        tagNode.remove("id");

        requestFields.removeIf(f -> f.getPath().equals("_links"));
        requestFields.add(fieldWithPath("organisations").ignored());
        requestFields.add(fieldWithPath("projects").ignored());

        this.mockMvc.perform(post(API_URL)
                .accept(MediaType.APPLICATION_JSON)
                .content(JSONHelper.toJSON(tagNode))
                .header("Authorization","Bearer "+jwt))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("tags/post",
                        requestFields(requestFields),
                        responseFields(responseFields)
                ));
    }

    @Test
    void modifyTag() throws Exception{

        Tag patchedTag = Tag.builder().text("#nousSommesUnis").build();
        List<FieldDescriptor> responseFields = getConstrainedTagFields();
        List<FieldDescriptor> requestFields = getConstrainedTagFields();

        requestFields.add(fieldWithPath("organisations").description("Array von Organisationen").optional().attributes(key("constraints").value("")));
        requestFields.add(fieldWithPath("projects").description("Array von Projekten").optional().attributes(key("constraints").value("")));

        requestFields.removeIf(f -> f.getPath().equals("_links"));

        ObjectNode patchedTagNode = JSONHelper.toObjectNode(patchedTag);
        patchedTagNode.remove("id");

        this.mockMvc.perform(patch(API_URL+"{tag-id}",this.tag.getId())
                .accept(MediaType.APPLICATION_JSON)
                .content(JSONHelper.toJSON(patchedTagNode))
                .header("Authorization","Bearer "+jwt))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("tags/modify",
                        links(halLinks(),getTagLinks()),
                        responseFields(responseFields),
                        requestFields(requestFields),
                        pathParameters(parameterWithName("tag-id").description("ID des zu ändernden Tags"))
                ));
    }

    private List<FieldDescriptor> getConstrainedTagFields(){
        ConstrainedFieldHelper.ConstrainedFields fields = ConstrainedFieldHelper.getConstrainedFields(Tag.class);
        return new ArrayList<FieldDescriptor>(
                Arrays.asList(
                    fields.withPath("text").description("Tag-Wert / Text"),
                    subsectionWithPath("_links").ignored()
        ));
    }

    private List<LinkDescriptor> getTagLinks(){
        return new ArrayList<>(Arrays.asList(
                linkWithRel("organisations").description("Alle zum Tag assozierten Organisationen"),
                linkWithRel("projects").description("Alle zum Tag assozierten Projekte"),
                linkWithRel("self").ignored(),
                linkWithRel("tag").ignored()
        ));
    }
}
