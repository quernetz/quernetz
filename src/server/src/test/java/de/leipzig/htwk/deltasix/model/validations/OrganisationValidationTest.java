package de.leipzig.htwk.deltasix.model.validations;

import de.leipzig.htwk.deltasix.DataGenerator;
import de.leipzig.htwk.deltasix.dataaccess.*;
import de.leipzig.htwk.deltasix.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.validation.ConstraintViolationException;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class OrganisationValidationTest {

    @Autowired
    EntityManager entityManager;

    @Autowired
    OrganisationRepository organisationRepository;

    @Autowired
    InterviewRepository interviewRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    CategoryRepository categoryRepository;

    Category testCategory;
    Tag testTag;
    Organisation testOrganisation;
    Project testProject;
    Interview testInterview;


    @BeforeEach
    public void setUp(){
        testTag = Tag.builder()
                .organisations(new HashSet<>())
                .projects(new HashSet<>())
                .text("#GeilerText")
                .build();
        testCategory = Category.builder()
                .categoryName("Geiler Name")
                .organisations(new HashSet<>())
                .projects(new HashSet<>())
                .build();
        testInterview = DataGenerator.getInterview();
        testProject = DataGenerator.getProject();

        tagRepository.save(testTag);
        categoryRepository.save(testCategory);
        interviewRepository.save(testInterview);
        projectRepository.save(testProject);

        testOrganisation = DataGenerator.getOrganisation();
        testOrganisation.addProject(testProject);
        testOrganisation.addInterview(testInterview);
        testOrganisation.addTag(testTag);
        testOrganisation.addCategory(testCategory);

    }

    @Test
    public void shouldBeValid(){
        organisationRepository.save(testOrganisation);
        assertTrue(entityManager.contains(testOrganisation));
    }

    @Test
    public void shouldNotBeValid_blankName() {
        testOrganisation.setName("");
        organisationRepository.save(testOrganisation);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    public void shouldNotBeValid_NullDescription() {
        testOrganisation.setDescription(null);
        organisationRepository.save(testOrganisation);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    public void shouldNotBeValid_LinkNoURL(){
        testOrganisation.setWebsite("keine URL");
        organisationRepository.save(testOrganisation);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }
    
}
