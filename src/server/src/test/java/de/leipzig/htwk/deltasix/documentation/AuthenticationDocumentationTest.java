package de.leipzig.htwk.deltasix.documentation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.leipzig.htwk.deltasix.DataGenerator;
import de.leipzig.htwk.deltasix.model.Organisation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;


import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@SpringBootTest({"spring.profiles.active=dev", "deltasix.auth.user=testName", "deltasix.auth.password=testPassword"})
@Transactional
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
public class AuthenticationDocumentationTest {

    private final String authenticationUrl = "/authenticate";
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    ObjectNode userNode;
    Organisation organisation;
    JsonNode organisationNode;

    @BeforeEach
    public void setUp(WebApplicationContext context, RestDocumentationContextProvider documentation){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(documentationConfiguration(documentation)).build();
        userNode = objectMapper.createObjectNode();
        userNode.put("username","testName");
        userNode.put("password","testPassword");
        organisation = DataGenerator.getOrganisation();
        organisationNode = objectMapper.valueToTree(organisation);
    }

    @Test
    void authEndpoint() throws Exception{
        mockMvc.perform(post(authenticationUrl).contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(userNode)).accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andDo(document("authentication"));
        //jwtNode = (ObjectNode)objectMapper.readTree(result.getResponse().getContentAsString());

        /*mockMvc.perform(post("http://localhost/api/organisations/").content(String.valueOf(organisationNode)).header("Authorization",jwtNode.get("jwt"))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());*/
    }
}
