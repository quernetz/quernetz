package de.leipzig.htwk.deltasix.documentation;

import com.fasterxml.jackson.databind.node.ObjectNode;
import de.leipzig.htwk.deltasix.DataGenerator;
import de.leipzig.htwk.deltasix.JSONHelper;
import de.leipzig.htwk.deltasix.dataaccess.CategoryRepository;
import de.leipzig.htwk.deltasix.dataaccess.OrganisationRepository;
import de.leipzig.htwk.deltasix.dataaccess.TagRepository;
import de.leipzig.htwk.deltasix.documentation.helpers.ConstrainedFieldHelper;
import de.leipzig.htwk.deltasix.model.Category;
import de.leipzig.htwk.deltasix.model.Organisation;
import de.leipzig.htwk.deltasix.model.Tag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.hypermedia.LinkDescriptor;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.snippet.Attributes.key;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@SpringBootTest("spring.profiles.active=test")
@Transactional
public class OrganisationDocumentationTest {

    private final static String API_URL = "/api/organisations/";

    @Autowired
    private OrganisationRepository organisationRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    private Organisation organisation;

    private MockMvc mockMvc;

    private final String jwt = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJSYWxmIiwiZXhwIjoxNTkyNDEzOTY5LCJpYXQiOjE1OTIzOTU5Njl9.HB3iy1BkGoY3sdIQ1As0H_5QyzsASsMKUdKXGT0XO4Q";

    @BeforeEach
    private void setUp(WebApplicationContext context, RestDocumentationContextProvider documentation) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(documentationConfiguration(documentation)).build();

        this.organisation = DataGenerator.getOrganisation();

        this.organisationRepository.save(organisation);
    }

    @Test
    void getAllOrganisations() throws Exception {
        this.mockMvc.perform(get(API_URL).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("organisations/organisations-get", responseFields(
                        subsectionWithPath("_embedded.organisations").description("Array von Organisationen"),
                        subsectionWithPath("_links").ignored(),
                        subsectionWithPath("page").ignored())));
    }

    @Test
    void getOrganisationById() throws Exception {

        List<FieldDescriptor> fields = getConstrainedOrganisationFields();

       // fields.add(subsectionWithPath("projects").description("Array von Projekten, die mit dieser Organisation in Beziehung stehen"));
      //  fields.add(subsectionWithPath("interviews").description("Array von Interviews, die mit dieser Organisation in Beziehung stehen"));

        this.mockMvc.perform(get(API_URL + "{organisation-id}", this.organisation.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("organisations/get-by-id",
                        pathParameters(parameterWithName("organisation-id").description("ID der zu ladenden Organisation")),
                        links(halLinks(), getOrganisationLinkDescriptors()),
                        responseFields(fields)));
    }

    @Test
    void getOrganisationByIdWithTagsAndCategories() throws Exception {

        Tag t1 = Tag.builder().text("#suppJogi").build();
        Tag t2 = Tag.builder().text("#mahlzeitHeinrich").build();

        tagRepository.saveAll(List.of(t1, t2));

        Category c1 = Category.builder().categoryName("YoloJürgen").build();

        categoryRepository.save(c1);

        t1.addOrganisation(this.organisation
                .addTag(t1)
                .addTag(t2.addOrganisation(this.organisation))
                .addCategory(c1.addOrganisation(this.organisation)));

        this.organisationRepository.save(this.organisation);

        List<FieldDescriptor> fields = getConstrainedOrganisationFields();
        fields.add(subsectionWithPath("categories").description("Array von Kategorien, in die die Organisation fällt"));
        fields.add(subsectionWithPath("tags").description("Array von Tags, die der Organisation zugeordnet werden"));
        fields.add(subsectionWithPath("projects").description("Array von Projekten, die mit dieser Organisation in Beziehung stehen"));
        fields.add(subsectionWithPath("interviews").description("Array von Interviews, die mit dieser Organisation in Beziehung stehen"));
        fields.add(subsectionWithPath("logo").ignored());
        this.mockMvc.perform(get(API_URL + "{organisation-id}?projection=withTagsCategoriesProjectsAndInterviews", this.organisation.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("organisations/get-by-id-projection",
                        links(halLinks(), getOrganisationLinkDescriptors()),
                        responseFields(fields),
                        pathParameters(parameterWithName("organisation-id").description("ID der zu ladenden Organisation"))));
    }

    @Test
    void deleteOrganisation() throws Exception {

        this.mockMvc.perform(delete(API_URL + "{organisation-id}", this.organisation.getId()).accept(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + jwt))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("organisations/delete-organisation", pathParameters(
                        parameterWithName("organisation-id").description("ID der zu löschenden Organisation")
                )));
    }

    @Test
    void createOrganisation() throws Exception {

        Organisation newOrganisation = DataGenerator.getOrganisation();

        Tag hansJürgenFTW = Tag.builder().text("#hansJürgenForPresident").build();
        Tag heinrichToTheMax = Tag.builder().text("#maximumHeinrich").build();

        tagRepository.save(hansJürgenFTW);
        tagRepository.save(heinrichToTheMax);

        Category htwkTestsWin = Category.builder().categoryName("whenTestdataWritesHistory").build();

        categoryRepository.save(htwkTestsWin);

        List<FieldDescriptor> fields = getConstrainedOrganisationFields();
        fields.removeIf(f -> f.getPath().equals("_links"));
        fields.addAll(Arrays.asList(
                fieldWithPath("tags").description("Array von übergebenen Tags (im Format `'tags/{id}'`)").attributes(key("constraints").value("")),
                fieldWithPath("projects").description("Array von übergebenen Projekten (im Format `'projects/{id}'`)").attributes(key("constraints").value("")),
                fieldWithPath("interviews").description("Array von übergebenen Interviews (im Format `'interviews/{id}'`)").attributes(key("constraints").value("")),
                fieldWithPath("categories").description("Array von übergebenen Kategorien (im Format `'categories/{id}'`)").attributes(key("constraints").value(""))
        ));

        ObjectNode newOrganisationNode = JSONHelper.toObjectNode(newOrganisation);
        newOrganisationNode.remove("id");
        newOrganisationNode.putArray("tags").addAll(JSONHelper.getHyperlinkArray("tags", List.of(hansJürgenFTW.getId(), heinrichToTheMax.getId())));
        newOrganisationNode.putArray("categories").addAll(JSONHelper.getHyperlinkArray("categories", List.of(htwkTestsWin.getId())));

        this.mockMvc.perform(post(API_URL).content(JSONHelper.toJSON(newOrganisationNode)).accept(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + jwt))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("organisations/post",
                        requestFields(fields)));

    }

    @Test
    void updateOrganisation() throws Exception {
        Organisation moddedOrganisation = DataGenerator.getOrganisation();

        ObjectNode organisationNode = JSONHelper.toObjectNode(moddedOrganisation);
        organisationNode.remove("id");

        List<FieldDescriptor> fields = getConstrainedOrganisationFields();
        fields.removeIf(f -> f.getPath().equals("_links"));
        fields.addAll(Arrays.asList(
                fieldWithPath("tags").description("Array von Tags, die der Organisation mitgegeben werden").optional().attributes(key("constraints").value("")),
                fieldWithPath("projects").description("Array von Projekten, die der Organisation mitgegeben werden").optional().attributes(key("constraints").value("")),
                fieldWithPath("interviews").description("Array von Interviews, die der Organisation mitgegeben werden").optional().attributes(key("constraints").value("")),
                fieldWithPath("categories").description("Array von Kategorien, die der Organisation mitgegeben werden").optional().attributes(key("constraints").value(""))
        ));

        this.mockMvc.perform(patch(API_URL + "{organisation-id}", this.organisation.getId())
                .accept(MediaType.APPLICATION_JSON)
                .content(JSONHelper.toJSON(organisationNode))
                .header("Authorization", "Bearer " + jwt))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andDo(document("organisations/modify-organisation",
                        pathParameters(parameterWithName("organisation-id").description("ID der zu ändernden Organisation")),
                        requestFields(fields)
                ));
    }

    private List<FieldDescriptor> getConstrainedOrganisationFields() {
        ConstrainedFieldHelper.ConstrainedFields fields = ConstrainedFieldHelper.getConstrainedFields(Organisation.class);
        return new ArrayList<FieldDescriptor>(Arrays.asList(
                fields.withPath("name").description("Name der Organisation"),
                fields.withPath("website").description("Hyperlink zur Seite der Organisation"),
                fields.withPath("description").description("Beschreibung der Organisation"),
               // fields.withPath("projects").description("Array von Projekten, die mit dieser Organisation in Beziehung stehen"),
               // fields.withPath("interviews").description("Array von Interviews, die mit dieser Organisation in Beziehung stehen"),
                subsectionWithPath("_links").ignored()
        ));
    }

    private List<LinkDescriptor> getOrganisationLinkDescriptors() {
        return new ArrayList<LinkDescriptor>(Arrays.asList(
                linkWithRel("projects").description("Die zur Organisation gehörenden Projekte"),
                linkWithRel("categories").description("Die Kategorien zu denen die Organisation gehört"),
                linkWithRel("tags").description("Die Tags, die zur Organisation verknüpft sind"),
                linkWithRel("interviews").description("Die Interviews, bei denen die Organisation mitgewirkt hat"),
                linkWithRel("self").ignored(),
                linkWithRel("organisation").ignored()
        ));
    }
}
