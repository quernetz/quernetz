= Quernetzer-API Backend-Dokumentation
Team Delta-SIX
0.0.1, 09.04.2020
:toc: left
:sectnums:

== Vorabinformationen

=== RESTfull-HTTP (hier verwendete Methoden)

|===
|HTTP-Methode|Semantik in REST

|`GET`      |Lade das Objekt zum angegebenen Pfad
|`POST`     |Speichere die übergebenen Daten als neues Objekt unter diesem Pfad (IDs werden durch Server vergeben)
|`DELETE`   |Lösche das Objekt zum angegebenen Pfad
|`PATCH`    |Ersetze die jeweils mitgelieferten Felder des im Pfad adressierten Objektes (unter Beibehaltung der Identität)
|`PUT`      |Überschreibe die adressierte Ressource mit den Feldern im Body (keine Beibehaltung der Identität, alle nicht mitgelieferten Felder werden entfernt)
|===

==== Assoziationen (Beziehungen)

- um Beziehungen / Referenzen auf andere Entitäten in REST auszudrücken,
gibt man die entsprechende Ressource als `local-URI` an.
- `local-URI`: kein Hostname, kein `api`-Präfix

_Beispiel 1_ (One-To-One, oder Many-To-One):

```json
{
    "description": "Ich verweise auf eine Parent-Entity",
    "parent" : "/parent/1" <1>
}
```
<1> hier wird (sozusagen) der Entitätstyp angegeben, und dann spezifiziert, welches Element gemeint ist (das mit der ID=1)

_Beispiel 2_ (One-To-Many / Many-To-Many)

```json
{
    "description": "Ich habe einige Verweise auf Kind-Elemente"
    "childs": [
        "/child/1",
        "/child/2",
        "/child/3"
    ]
}
```

==== `POST`

* Ein erfolgreicher POST lässt sich anhand des Status-Codes `201` (IS-CREATED) sowie einem
entsprechenden Objekt in der Response prüfen
* Trat ein Fehler bei der Validierung der Daten auf wird `400` (BAD_REQUEST) zurückgegeben.

Grundsätzlich gilt bei der Verwendung der Quernetzer-API:

* ein neues Objekt wird ohne `ID` (o.ä. Variationen der Zeichenkette) an den Server geschickt
* Im Response-Body wird das vom Server erzeugte Objekt zurückgegeben

==== `GET`

* erfolgreiche GETs liefern `200` als Status-Code
* `404` bedeutet, dass das Objekt nicht gefunden wurde
* `500` sollte gemeldet werden (i. S. des Projektes)

* zu einem Entitätstyp können entweder alle Elemente geladen werden, oder nur eins anhand der ID
`/typ[?/id]` (Beispiele im Folgenden vorhanden)

==== `PATCH`

Bei einem PATCH werden die Eigenschaften, des bereits auf dem Server liegenden Objektes durch die
im Body gelieferten Eigenschaften (und Werte) ersetzt.

Im `PATCH` - Body sollen keine IDs (es sei denn man möchte Beziehungen patchen) stehen.
Das entsprechend zu ändernde Objekt wird über die URL identifiziert.
Es ist darauf zu achten, das NUR ZU ÄNDERNDE WERTE im Request-Body übermittelt werden.

==== `PUT`

Bei `PUT` müssen sowohl die geänderten als auch die gleichbleibenden Daten übertragen werden.
Jedes Feld, dass nicht im Request-Body angegeben wird, wird auf dem Server gelöscht. Ferner überschreibt `PUT` die Identität
(Andernfalls würden alle nicht gelieferten Felder / Daten entfernt werden).

Lediglich bei der Manipulation von Beziehungen zwischen den Entitäten wird PUT (wie unten beschrieben)
verwendet / kann verwendet werden.

==== `DELETE`

Lösche die adressierte Ressource.
Wird `404` zurückgegeben, dann existierte die Ressource vor dem Löschen nicht.
`204` wird sowohl zurückgegeben, wenn die Ressource gelöscht wurde, als auch wenn sie
auf Grund von Integritätsverletzungen nicht gelöscht werden konnte. Demnach sollte der Erfolg mit einem weiteren
Request geprüft werden.

```
/[entitätstyp]/[id]
```

include::validation.adoc[]

include::authentication.adoc[]

include::associations.adoc[]

include::api.adoc[]

include::projects.adoc[]

include::interviews.adoc[]

include::organisations.adoc[]

include::categories.adoc[]

include::tags.adoc[]