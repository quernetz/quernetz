== API-Index

=== API - Endpunkte

Stellt Informationen über die verwendete API zu Verfügung:
Art der verfügbaren Entitäten, sowie die verfügbaren Projections.
Zu Projekten und Organisationen existiert (jew.) die Projection `withTagsAndCategories`.
Diese erlaubt es, in nur einem `GET`-Request sowohl die Texte zu Kategorien und Tags mit zu laden,
ohne diese über einen zusätzlichen Request laden zu müssen.

==== Request
include::{snippets}/api/get/http-request.adoc[]
==== Response
include::{snippets}/api/get/http-response.adoc[]
===== Links
include::{snippets}/api/get/links.adoc[]