== Projekt-Endpunkt - Backend-Dokumentation

=== GPS- / Adressen-Modus beim verwenden der Locations

Beim Arbeiten mit Standorten sollte erwähnt sein, dass auf Grund
momentan unzureichender Spezifikation, Standorte sowohl als GPS-Koordinaten
als auch als Adress-Tupel gespeichert werden können.

Dabei ist jedoch zu beachten, dass man sich beim Speichern für einen der beiden Modi entscheiden muss:

. Entweder GPS-Mode (`longitude` und `latitude` müssen zwischen Werten von [-90.0, +90], bzw. [-180.0,180.0] gespeichert werden)
    * `street`, `streetNumber` und `zipCode` werden dabei nicht validiert
    * Um den GPS-Modus auszudrück, müssen `longitude` UND `latitude` von 0.0 verschieden sein

. Oder Adress-Modus:
    * `longitude` und `latitude` müssen auf 0.0 gesetzt sein
    * die Adressfelder (`street`, `streetNumber` und `zipCode`) dürfen nicht leer sein / nicht nur aus Leerzeichen bestehen!

=== Alle Projekte laden
==== Request
include::{snippets}/projects/projects-get/http-request.adoc[]
==== Response
include::{snippets}/projects/projects-get/http-response.adoc[]
===== Response fields
include::{snippets}/projects/projects-get/response-fields.adoc[]

=== Projekt anhand der ID laden
==== Request
include::{snippets}/projects/get-by-id/http-request.adoc[]
===== Pfadparameter
include::{snippets}/projects/get-by-id/path-parameters.adoc[]
==== Response
include::{snippets}/projects/get-by-id/http-response.adoc[]
===== Response fields
include::{snippets}/projects/get-by-id/response-fields.adoc[]
===== Verknüpfungen
include::{snippets}/projects/get-by-id/links.adoc[]

=== Projekte mit Tags und Kategorien laden
==== Request
include::{snippets}/projects/get-by-id-projection/http-request.adoc[]
===== Pfadparameter
include::{snippets}/projects/get-by-id-projection/path-parameters.adoc[]
==== Response
include::{snippets}/projects/get-by-id-projection/http-response.adoc[]
===== Response fields
include::{snippets}/projects/get-by-id-projection/response-fields.adoc[]
===== Verknüpfungen
include::{snippets}/projects/get-by-id-projection/links.adoc[]

=== Projekte laden
==== Request
include::{snippets}/projects/delete-project/http-request.adoc[]
===== Pfadparameter
include::{snippets}/projects/delete-project/path-parameters.adoc[]
==== Response
include::{snippets}/projects/delete-project/http-response.adoc[]

=== Projekte erstellen
==== Request
include::{snippets}/projects/post/http-request.adoc[]
===== Request fields
include::{snippets}/projects/post/request-fields.adoc[]
==== Response
include::{snippets}/projects/post/response-body.adoc[]

=== Projekte verändern
Anmerkung: Unter <<Projekte erstellen>> ist das Format der Referenzen auf andere Entitäten beschrieben.
==== Request
include::{snippets}/projects/modify-projects/http-request.adoc[]
===== Request fields
include::{snippets}/projects/modify-projects/request-fields.adoc[]
===== Pfadparameter
include::{snippets}/projects/modify-projects/path-parameters.adoc[]
==== Response
include::{snippets}/projects/modify-projects/http-response.adoc[]
===== Verknüpfungen
include::{snippets}/projects/modify-projects/links.adoc[]

=== Projekte löschen
==== Request
include::{snippets}/projects/delete-project/http-request.adoc[]
===== Pfadparameter
include::{snippets}/projects/delete-project/path-parameters.adoc[]
==== Response
include::{snippets}/projects/delete-project/http-response.adoc[]
