package de.leipzig.htwk.deltasix.model.projections;

import de.leipzig.htwk.deltasix.model.Interview;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDate;
import java.util.List;

/**
 * Projection used that already includes organisation names to respective interviews
 * usage: [url]/interview?projection=withOrganisations
 *
 * @see de.leipzig.htwk.deltasix.model.Organisation;
 * @see de.leipzig.htwk.deltasix.dataaccess.OrganisationRepository
 */
@Projection(name = "withOrganisations", types={Interview.class})
public interface InterviewProjection {
    List<OrganisationNameProjection> getOrganisations();
    String getName();
    String getDescription();
    String getLink();
    String getYoutubeLink();
    LocalDate getDate();
}