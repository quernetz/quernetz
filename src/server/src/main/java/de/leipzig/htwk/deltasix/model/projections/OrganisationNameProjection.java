package de.leipzig.htwk.deltasix.model.projections;

public interface OrganisationNameProjection {
    String getName();
}
