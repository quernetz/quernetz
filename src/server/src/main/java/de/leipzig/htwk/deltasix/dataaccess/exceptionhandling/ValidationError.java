package de.leipzig.htwk.deltasix.dataaccess.exceptionhandling;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Anton Rhein
 * A simple java bean that abstracts an validation error
 * e.g. if an invalid resource is passed, it serves as a key,value map of fieldName and reason (why the field was invalid)
 */
public class ValidationError {

    @Getter
    private List<ViolationField> violations = new ArrayList<>();

    public ValidationError addViolation(String field, String message){
        this.violations.add(new ViolationField(field,message));
        return this;
    }
}
@Getter
class ViolationField{
    private String fieldName;
    private String reason;
    public ViolationField(String name, String reason){
        this.fieldName = name;
        this.reason = reason;
    }
}