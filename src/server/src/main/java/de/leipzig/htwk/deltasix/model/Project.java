/* Author: Anton Rhein
 * Date: 2019/12/05
 * Project entity
 */

package de.leipzig.htwk.deltasix.model;

import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.URL;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Entity
@Accessors(chain = true)
@EqualsAndHashCode(of = "id")
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Validated
public class Project {

    @Getter
    @Setter(AccessLevel.PRIVATE)
    @GeneratedValue
    @Id
    private Long id;

    @Getter
    @Setter
    @NotBlank
    private String name;

    @Getter
    @Setter
    @Lob
    private String description;

    @Embedded
    @Getter
    @Setter
    @NotNull
    @Valid
    private Location location;

    @Getter
    @Setter
    @URL
    private String projectLink;

    @Getter
    @Setter
    @URL
    private String imageUrl;

    @Getter
    @Setter
    @Column(name = "PROJECT_FROM")
    @NotNull
    private LocalDate from;

    @Getter
    @Setter
    @Column(name = "PROJECT_TO")
    private LocalDate to;

    @Builder.Default
    @Fetch(FetchMode.JOIN)
    @ManyToMany(mappedBy = "projects")
    private Set<@Valid Category> categories = new HashSet<>();

    @Builder.Default
    @Setter(AccessLevel.PRIVATE)
    @Fetch(FetchMode.JOIN)
    @ManyToMany(mappedBy = "projects")
    private Set<@Valid Tag> tags = new HashSet<>();

    @Builder.Default
    @Fetch(FetchMode.JOIN)
    @NotEmpty
    @ManyToMany
    @JoinTable(name = "ORGANISATION_PROJECT", joinColumns = @JoinColumn(name = "PROJECT_ID"), inverseJoinColumns = @JoinColumn(name = "ORGANISATION_ID"))
    private Set<@Valid Organisation> organisations = new HashSet<>();

    public Set<Category> getCategories() { return Collections.unmodifiableSet(this.categories); }

    public Set<Tag> getTags() {return Collections.unmodifiableSet(this.tags);}

    public Set<Organisation> getOrganisations() { return Collections.unmodifiableSet(this.organisations); }

    /**
     * @param year
     * @param month
     * @param day
     * @return this instance
     * @throws Exception when provided date parameters are invalid
     */
    public Project setFrom(@Range(min = 1980, max = 9999) int year, @Range(min = 1, max = 12) int month, @Range(min = 1, max = 31) int day){
        this.from = LocalDate.of(year, month, day);
        return this;
    }

    /**
     * @param year
     * @param month
     * @param day
     * @return this instance
     * @throws Exception when provided date parameters are invalid
     */
    public Project setTo(@Range(min = 1980, max = 9999)int year, @Range(min = 1, max = 12) int month, @Range(min = 1, max = 31) int day){
        this.to = LocalDate.of(year, month, day);
        return this;
    }

    /**
     * Adds a sector to this project
     *
     * @param category the category to add
     * @return this instance
     */
    public Project addCategory(@Valid @NotNull Category category) {
        this.categories.add(category);
        return this;
    }


    /**
     * Add a collections of sectors to this project
     *
     * @param categories the categories to add
     * @return this instance
     */
    public Project addCategories(@NotEmpty Collection<@Valid @NotNull Category> categories) {
        this.categories.addAll(categories);
        return this;
    }

    /**
     * Removes the passed sector from this project
     *
     * @param category the category to remove
     * @return this instance
     */
    public Project removeCategory(@Validated @NotNull Category category) {
        this.categories.remove(category);
        return this;
    }

    /**
     * remove a range of sectors from this project
     *
     * @param categories to remove
     * @return this instance
     */
    public Project removeCategories(@NotEmpty Collection<@Valid @NotNull Category> categories) {
        this.categories.removeAll(categories);
        return this;
    }

    public Project addTag( @NotNull @Validated Tag tag) {
        this.tags.add(tag);
        return this;
    }

    public Project removeTag(@NotNull @Validated Tag tag){
        this.tags.remove(tag);
        return this;
    }

    public Project addTags(@NotEmpty Collection<@Valid Tag> tags){
        this.tags.addAll(tags);
        return this;
    }

    public Project removeTags(@NotEmpty Collection< @Valid Tag> tags) {
        this.tags.removeAll(tags);
        return this;
    }

    public Project addOrganisation(@NotNull @Validated Organisation organisation) {
        this.organisations.add(organisation);
        return this;
    }

    public Project removeOrganisation(@NotNull @Validated Organisation organisation){
        this.organisations.remove(organisation);
        return this;
    }

    public Project addOrganisations(@NotEmpty Collection<@Valid Organisation> organisations){
        this.organisations.addAll(organisations);
        return this;
    }

    public Project removeOrganisations(@NotEmpty Collection<@Valid Organisation> organisations) {
        this.organisations.removeAll(organisations);
        return this;
    }

    @PreUpdate
    @PrePersist
    private void updateAssociations(){
        this.tags.forEach(t -> t.addProject(this));
        this.categories.forEach(c -> c.addProject(this));
        this.organisations.forEach(o -> o.addProject(this));
    }

    @PreRemove
    private void removeAssociations() {
        this.tags.forEach(t -> t.removeProject(this));
        this.categories.forEach(c -> c.removeProject(this));
    }
}
