package de.leipzig.htwk.deltasix.model.projections;

public interface ProjectNameProjection {
    String getName();
}
