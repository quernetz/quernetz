package de.leipzig.htwk.deltasix.dataaccess.exceptionhandling;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;

@ControllerAdvice
@Slf4j
/**
 * @author Anton Rhein
 * - a custom handler for ConstraintViolationExceptions
 * - it makes the server return 400 (Bad Request) when a constraint violation on the provided entity occurs
 * - it adds the noticed violations as reasons to the response
 * - without this advice, server would return a 500 with a cryptic stacktrace
 */
public class ValidationErrorControllerAdvice {

    @ExceptionHandler({TransactionSystemException.class})
    public ResponseEntity<String> handleConstraintViolationException(TransactionSystemException ex){
        Throwable cause = ex.getRootCause();
        if(cause instanceof ConstraintViolationException){
            ConstraintViolationException violation = (ConstraintViolationException) cause;
            try{
                String message = getConstraintViolationMessage(violation.getConstraintViolations());
                return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).body(message);
            } catch(JsonProcessingException e){
                log.error("ConstraintViolation occurred - Unable to convert to JSON");
                throw ex;
            }
        } else {
            throw ex;
        }
    }

    private String getConstraintViolationMessage(Set<ConstraintViolation<?>> violations) throws JsonProcessingException{
        ValidationError error = new ValidationError();
        violations.forEach(v -> error.addViolation(v.getPropertyPath().toString(), v.getMessage()));
        return convertToJSON(error);
    }

    private String convertToJSON(Object x) throws JsonProcessingException {
        ObjectWriter writer = new ObjectMapper().configure(SerializationFeature.WRAP_ROOT_VALUE, false).writer();
        return writer.writeValueAsString(x);
    }
}
