package de.leipzig.htwk.deltasix.model;

import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.URL;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Entity
@Builder
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Validated
public class Interview {

    @Getter
    @Setter(AccessLevel.PRIVATE)
    @GeneratedValue
    @Id
    private Long id;

    @Getter
    @Setter
    @NotBlank
    private String name;

    @Getter
    @Setter
    @URL
    private String link;

    @Getter
    @Setter
    @URL
    private String youtubeLink;

    @Getter
    @Setter
    private LocalDate date;

    @Getter
    @Setter
    @Lob
    @NotBlank
    private String description;

    @Builder.Default
    @Fetch(FetchMode.JOIN)
    @ManyToMany
    @JoinTable(name = "ORGANISATION_INTERVIEWS", joinColumns = @JoinColumn(name = "INTERVIEW_ID"), inverseJoinColumns = @JoinColumn(name = "ORGANISATION_ID"))
    private Set<@Valid Organisation> organisations = new HashSet<>();

    public Set<Organisation> getOrganisations() { return Collections.unmodifiableSet(this.organisations); }

    public Interview addOrganisation(@NotNull @Validated Organisation organisation) {
        this.organisations.add(organisation);
        return this;
    }

    public Interview removeOrganisation(@NotNull @Validated Organisation organisation){
        this.organisations.remove(organisation);
        return this;
    }

    @PreUpdate
    @PrePersist
    private void updateAssociations(){
        this.organisations.forEach(o -> o.addInterview(this));
    }

}
