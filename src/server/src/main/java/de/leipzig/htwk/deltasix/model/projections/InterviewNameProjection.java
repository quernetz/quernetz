package de.leipzig.htwk.deltasix.model.projections;

public interface InterviewNameProjection {
    String getName();
}
