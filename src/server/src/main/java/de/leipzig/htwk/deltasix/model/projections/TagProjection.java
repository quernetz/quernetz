package de.leipzig.htwk.deltasix.model.projections;

/**
 * @author Anton Rhein
 *
 * Projection only used when Projects and Organisations are queried by their projections
 * Allows to directly include tag text as well as links to self, projects and organisations
 *
 * @see de.leipzig.htwk.deltasix.model.Tag;
 * @see de.leipzig.htwk.deltasix.dataaccess.TagRepository
 */
public interface TagProjection {
    String getText();
}
