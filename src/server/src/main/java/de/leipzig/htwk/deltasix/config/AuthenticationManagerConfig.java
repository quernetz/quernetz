package de.leipzig.htwk.deltasix.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/* Wird benötigt da es nur für zwei bestimmte Profile eine Security Config vorhanden ist
 * und es somit für das starten ohne Profil (Default) nicht möglich ist den Application Context
 * hochzufahren, da der Authentication Manager fehlen würde.
 */

@Configuration
public class AuthenticationManagerConfig {
    @Bean
    public AuthenticationManager defaultImpl(){
        return new AuthenticationManager() {
            @Override
            public Authentication authenticate(Authentication authentication) throws AuthenticationException {
                return null;
            }
        };
    }

}
