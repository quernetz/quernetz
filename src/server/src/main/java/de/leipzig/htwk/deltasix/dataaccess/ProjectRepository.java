/*
 * Author: Anton Rhein
 * Date: 2019/05/12
 * interface: returns project objects
 */
package de.leipzig.htwk.deltasix.dataaccess;

import de.leipzig.htwk.deltasix.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RepositoryRestResource
public interface ProjectRepository extends JpaRepository<Project, Long> {
}
