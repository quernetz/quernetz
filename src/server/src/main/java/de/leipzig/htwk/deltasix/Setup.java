package de.leipzig.htwk.deltasix;

import de.leipzig.htwk.deltasix.dataaccess.*;
import de.leipzig.htwk.deltasix.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Slf4j
@Profile({"dev"})
public class Setup implements CommandLineRunner {

    public static final String ORGANISATION_DESCRIPTION = "Hier kommt noch mehr langer Text... Vielleicht. Keine Ahnung. Noch mehr Text!";
    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private InterviewRepository interviewRepository;

    @Autowired
    private OrganisationRepository organisationRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private EntityManager entityManager;

    @PreDestroy
    public void destroyed() {
        this.interviewRepository.deleteAll();
        this.projectRepository.deleteAll();
        this.organisationRepository.deleteAll();
        this.categoryRepository.deleteAll();
        this.tagRepository.deleteAll();
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {

        ArrayList<Category> categories = new ArrayList<>(List.of(
                Category.builder().categoryName("Umwelt").build(),
                Category.builder().categoryName("Müll").build(),
                Category.builder().categoryName("Bildung").build(),
                Category.builder().categoryName("Soziales").build(),
                Category.builder().categoryName("Politik").build()
        ));

        ArrayList<Tag> tags = new ArrayList<>(List.of(
                Tag.builder().text("#unitedWeStrand").build(),
                Tag.builder().text("#nousSommesHansJürgen").build(),
                Tag.builder().text("#yesWeBeer").build()
        ));

        tagRepository.saveAll(tags);
        HashSet<Tag> dbTags = new HashSet<>();
        dbTags.addAll(tagRepository.findAll());

        categoryRepository.saveAll(categories);
        HashSet<Category> dbCategories = new HashSet<>();
        dbCategories.addAll(categoryRepository.findAll());

        List<Organisation> organisationMockups = new ArrayList<>();

        Organisation.OrganisationBuilder oBuilder = Organisation.builder();

        Organisation stadtReinigung = oBuilder.tags(new HashSet<>())
                .name("Stadtreinigung")
                .logo("".getBytes())
                .website("https://www.stadtreinigung-leipzig.de")
                .categories(categories.parallelStream().filter(c -> c.getCategoryName().equals("Müll")).collect(Collectors.toSet()))
                .description("Die Stadtreinigung der Stadt Leipzig.")
                .tags(dbTags.parallelStream().filter(t -> t.getText().contains("Jürgen")).collect(Collectors.toSet()))
                .build();
        dbTags.parallelStream().filter(t -> t.getText().contains("Jürgen")).collect(Collectors.toSet()).forEach(t -> t.addOrganisation(stadtReinigung));
        categories.parallelStream().filter(c -> c.getCategoryName().equals("Müll")).collect(Collectors.toSet()).forEach(c -> c.addOrganisation(stadtReinigung));

        organisationRepository.save(stadtReinigung);

        Organisation engagementGlobal = oBuilder.tags(new HashSet<>())
                .name("ENGAGEMENT GLOBAL")
                .logo("".getBytes())
                .website("https://www.engagement-global.de")
                .categories(categories.parallelStream().filter(c -> c.getCategoryName().equals("Soziales")).collect(Collectors.toSet()))
                .description("Eine deutsche non-profit-Organisation mit einem Fokus auf Entwiklungsinitiativen.")
                .build();

        categories.parallelStream().filter(c -> c.getCategoryName().equals("Soziales")).collect(Collectors.toSet()).forEach(c -> c.addOrganisation(engagementGlobal));

        organisationRepository.save(engagementGlobal);

        Organisation einfachUnverpackt = oBuilder.tags(new HashSet<>())
                .name("Einfach unverpackt")
                .logo("".getBytes())
                .website("https://www.einfach-unverpackt.de")
                .categories(categories.parallelStream().filter(c -> c.getCategoryName().equals("Müll")).collect(Collectors.toSet()))
                .description("Ein verpackungsfreier Lader in der Leipizger Südvorstadt.")
                .build();

        categories.parallelStream().filter(c -> c.getCategoryName().equals("Müll")).collect(Collectors.toSet()).forEach(c -> c.addOrganisation(einfachUnverpackt));

        organisationRepository.save(einfachUnverpackt);

        Organisation krimzKrams = oBuilder.tags(new HashSet<>())
                .name("krimZkrams")
                .logo("".getBytes())
                .website("https://www.kunzstoffe.de")
                .categories(categories.parallelStream().filter(c -> c.getCategoryName().equals("Müll")).collect(Collectors.toSet()))
                .description("Eine Urbane Ideenwerkstatt, welche aus Müll neue Dinge herstellt.")
                .build();

        categories.parallelStream().filter(c -> c.getCategoryName().equals("Müll")).collect(Collectors.toSet()).forEach(c -> c.addOrganisation(krimzKrams));

        organisationRepository.save(krimzKrams);

        Organisation socialCooking = oBuilder.tags(new HashSet<>())
                .name("Social cooking")
                .logo("".getBytes())
                .website("https://www.facebook.com/SocialCookingPritsch/")
                .categories(categories.parallelStream().filter(c -> c.getCategoryName().equals("Soziales")).collect(Collectors.toSet()))
                .description("Ein Koch, welcher sich an sozialen Kochprojekten beteidigt.")
                .build();

        categories.parallelStream().filter(c -> c.getCategoryName().equals("Soziales")).collect(Collectors.toSet()).forEach(c -> c.addOrganisation(socialCooking));

        organisationRepository.save(socialCooking);

        Organisation timmiHelp = oBuilder.tags(new HashSet<>())
                .name("TiMMi ToHelp e.V.")
                .logo("".getBytes())
                .website("https://www.timmitohelp.de")
                .categories(categories.parallelStream().filter(c -> c.getCategoryName().equals("Soziales")).collect(Collectors.toSet()))
                .description("Ein Verein, welchen sozial schlechter gestellten Menschen hilft.")
                .build();

        categories.parallelStream().filter(c -> c.getCategoryName().equals("Soziales")).collect(Collectors.toSet()).forEach(c -> c.addOrganisation(timmiHelp));

        organisationRepository.save(timmiHelp);

        Organisation momochrome = oBuilder.tags(new HashSet<>())
                .name("Druckerei Momochrome")
                .logo("".getBytes())
                .website("https://www.momochrome.de")
                .categories(categories.parallelStream().filter(c -> c.getCategoryName().equals("Soziales")).collect(Collectors.toSet()))
                .description("Die Leipziger Druckerei Monochrome.")
                .build();

        categories.parallelStream().filter(c -> c.getCategoryName().equals("Soziales")).collect(Collectors.toSet()).forEach(c -> c.addOrganisation(momochrome));

        organisationRepository.save(momochrome);

        Organisation quelleEv = oBuilder.tags(new HashSet<>())
                .name("Wohnprojekt Quelle e.V.")
                .logo("".getBytes())
                .website("https://www.wohnungslosenhilfe-leipzig.de")
                .categories(categories.parallelStream().filter(c -> c.getCategoryName().equals("Soziales")).collect(Collectors.toSet()))
                .description("Ein leipziger Verein, welcher betreutes Wohnen anbietet.")
                .build();

        categories.parallelStream().filter(c -> c.getCategoryName().equals("Soziales")).collect(Collectors.toSet()).forEach(c -> c.addOrganisation(quelleEv));

        organisationRepository.save(quelleEv);

        Organisation vhs = oBuilder.tags(new HashSet<>())
                .name("Volkshochschule Leipzig")
                .logo("".getBytes())
                .website("https://www.vhs-leipzig.de")
                .categories(categories.parallelStream().filter(c -> c.getCategoryName().equals("Soziales")).collect(Collectors.toSet()))
                .description("Das kommunale Weiterbildungszentrum Leipzigs..")
                .build();
        categories.parallelStream().filter(c -> c.getCategoryName().equals("Soziales")).collect(Collectors.toSet()).forEach(c -> c.addOrganisation(vhs));

        organisationRepository.save(vhs);

        Organisation punkWerksKammer = oBuilder.tags(new HashSet<>())
                .name("Punkwerkskammer")
                .logo("".getBytes())
                .website("https://www.punkwerxxkammer.wixsite.com/pwkg3")
                .categories(categories.parallelStream().filter(c -> c.getCategoryName().equals("Soziales")).collect(Collectors.toSet()))
                .description("Ein Verein unter dem Motto 'von Obdachlosen für Obdachlose'.")
                .build();
        categories.parallelStream().filter(c -> c.getCategoryName().equals("Soziales")).collect(Collectors.toSet()).forEach(c -> c.addOrganisation(punkWerksKammer));

        organisationRepository.save(punkWerksKammer);

        Project.ProjectBuilder pBuilder = Project.builder();

        Project erstesProjekt = pBuilder.name("Erstes Projekt")
                .description("Das war unser erstes Projekt")
                .from(LocalDate.of(2019, 01, 01))
                .to(LocalDate.of(2019, 01, 02))
                .imageUrl("https://m.media-amazon.com/images/I/51AuCnI0-4L._SS500_.jpg")
                .location(new Location("Straße", "2", "12345"))
                .projectLink("https://www.duckduckgo.com")
                .build()
                .addCategories(dbCategories)
                .addTags(tags);
        tags.forEach(t -> t.addProject(erstesProjekt));
        dbCategories.forEach(c -> c.addProject(erstesProjekt));

        Project neuesProjekt = pBuilder.name("Neues Projekt")
                .description("Das ist unser neues Projekt")
                .from(LocalDate.of(2018, 03, 14))
                .to(LocalDate.of(2020, 01, 02))
                .imageUrl("https://m.media-amazon.com/images/I/51AuCnI0-4L._SS500_.jpg")
                .location(new Location("Andere Straße", "3", "16645"))
                .projectLink("https://www.wikipedia.de")
                .build();

        Project drittesProjekt = pBuilder.name("Drittes Projekt")
                .description("Das war unser drittes Projekt")
                .from(LocalDate.of(2010, 02, 10))
                .to(LocalDate.of(2012, 02, 01))
                .imageUrl("https://m.media-amazon.com/images/I/51AuCnI0-4L._SS500_.jpg")
                .location(new Location(10.05647649284, 10.322030045))
                .projectLink("https://www.okillydokilly.de")
                .build().addCategories(dbCategories);
        dbCategories.forEach(c -> c.addProject(drittesProjekt));

        Project viertesProjekt = pBuilder.name("Viertes Projekt")
                .description("Das war unser viertes Projekt")
                .from(LocalDate.of(2015, 10, 15))
                .to(LocalDate.of(2016, 9, 13))
                .imageUrl("https://m.media-amazon.com/images/I/51AuCnI0-4L._SS500_.jpg")
                .location(new Location("Brettweg", "2", "12345"))
                .projectLink("https://www.www.de")
                .build()
                .addTags(dbTags);
        dbTags.forEach(tag -> tag.addProject(viertesProjekt));

        Project naechstesProjekt = pBuilder.name("Nächstes Projekt")
                .description("Projekt im Mai")
                .from(LocalDate.of(2020, 12, 10))
                .to(LocalDate.of(2021, 10, 02))
                .imageUrl("https://m.media-amazon.com/images/I/51AuCnI0-4L._SS500_.jpg")
                .location(new Location("Feldweg", "3", "55555"))
                .projectLink("https://www.wikiped.de")
                .build();

        drittesProjekt.addOrganisation(stadtReinigung);
        erstesProjekt.addOrganisation(stadtReinigung);


        engagementGlobal.addProject(erstesProjekt);
        engagementGlobal.addProject(neuesProjekt);

        erstesProjekt.addOrganisation(engagementGlobal);
        neuesProjekt.addOrganisation(engagementGlobal);

        organisationMockups.add(engagementGlobal);

        einfachUnverpackt.addProject(naechstesProjekt).addProject(drittesProjekt);

        naechstesProjekt.addOrganisation(einfachUnverpackt);
        drittesProjekt.addOrganisation(einfachUnverpackt);

        organisationMockups.add(einfachUnverpackt);

        krimzKrams.addProject(erstesProjekt);
        erstesProjekt.addOrganisation(krimzKrams);

        organisationMockups.add(krimzKrams);

        socialCooking.addProject(naechstesProjekt);
        naechstesProjekt.addOrganisation(socialCooking);
        organisationMockups.add(socialCooking);

        timmiHelp.addProject(drittesProjekt);
        drittesProjekt.addOrganisation(timmiHelp);

        organisationMockups.add(timmiHelp);

        momochrome.addProject(viertesProjekt);
        viertesProjekt.addOrganisation(momochrome);
        organisationMockups.add(momochrome);

        organisationMockups.add(quelleEv);

        organisationMockups.add(punkWerksKammer);

        projectRepository.saveAll(List.of(
                erstesProjekt,
                neuesProjekt,
                drittesProjekt,
                viertesProjekt,
                naechstesProjekt
        ));

        HashSet<Organisation> organisations = new HashSet<>();
        organisations.addAll(organisationRepository.findAll());

        Interview quergefragt1 = Interview.builder().name("Quergefragt #1 - Patrice von Leipspeis")
                .description("Stolz präsentieren wir unsere neue Interview-Rubrik \nQuer-gefragt!.\nIn der ersten Folge verrät uns Patrice von Leipspeis & Ölmühle Leipzig, wie er lokale Lebensmittel produziert, was ihn antreibt und welchen Beitrag wir selber für eine nachhaltige Welt leisten können.")
                .youtubeLink("https://www.youtube.com/embed/2es-4oQgLTU")
                .link("https://leipspeis.de/")
                .date(LocalDate.now())
                .build();

        Interview quergefragt2 = Interview.builder().name("Quergefragt #2 - Lisa & Eberhard vom Café kaputt")
                .description("In der zweiten Folge verraten uns Lisa und Eberhard vom Café kaputt, warum sich \"selber reparieren\" lohnt, was sie antreibt und welchen Beitrag wir selber für eine nachhaltige Welt leisten können. ")
                .youtubeLink("https://www.youtube.com/embed/Ocrs0NJd3YA")
                .link("https://www.leipzig-leben.de/cafe-kaputt-leipzig/")
                .date(LocalDate.now())
                .build();

        Interview quergefragt3 = Interview.builder().name("Quergefragt #3 - Richard von ernte-mich")
                .description("In der dritten Folge Quergefragt sprechen wir mit Richard von Ernte-mich über mehr Transparenz und Aufklärung in der Bio-Landwirtschaft.\nAußerdem darüber, wie auch ihr euer eigenes Beet mieten könnt!")
                .youtubeLink("https://www.youtube.com/embed/6AiWQKklx6g")
                .link("https://www.erntemich.de/")
                .date(LocalDate.now())
                .build();

        Interview abfallToGo = Interview.builder().name("Abfall-to-go-Beutel")
                .description("Die Quernetzer möchten Dir helfen, deinen Park sauber zu halten. Dafür haben wir den selbsterklärenden Abfall-to-go-Beutel entwickelt.\nGefällt Dir unsere Arbeit, möchtest du mitmachen oder hast Fragen, dann schreibe uns unter die-quernetzer@gmx.de oder besuche unsere Homepage die-quernetzer.de.")
                .youtubeLink("https://www.youtube.com/embed/4t5HA69nZ1A&t=1s")
                .link("https://die-quernetzer.de/")
                .date(LocalDate.now())
                .build();

        Interview quergefragt4 = Interview.builder()
                .name("Quergefragt #4 mit Pierre von \"Einfach Unverpackt\"")
                .description("In der vierten Folge Quergefragt sprechen wir mit Pierre von \"Einfach Unverpackt\" über verpackungsfreies Einkaufen und was ihn dazu motiviert hat, ein eigenen Laden aufzumachen.\nAußerdem darüber,  was jeder von euch tun kann, um ein verpackungsfreies Leben zu führen!")
                .youtubeLink("https://www.einfach-unverpackt.de/")
                .build();

        quergefragt1.addOrganisation(punkWerksKammer);
        quergefragt2.addOrganisation(punkWerksKammer);
        quergefragt3.addOrganisation(socialCooking);
        quergefragt4.addOrganisation(socialCooking);
        abfallToGo.addOrganisation(socialCooking);

        interviewRepository.saveAll(List.of(quergefragt1, quergefragt2, quergefragt3, quergefragt4, abfallToGo));


        entityManager.flush();
        entityManager.clear();
    }
}
