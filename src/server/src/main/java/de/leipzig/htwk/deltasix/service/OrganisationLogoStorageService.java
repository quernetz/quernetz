package de.leipzig.htwk.deltasix.service;

import de.leipzig.htwk.deltasix.model.Organisation;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import de.leipzig.htwk.deltasix.dataaccess.OrganisationRepository;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrganisationLogoStorageService {

    private final OrganisationRepository organisationRepository;
    private final LogoWriter logoWriter;

    @Transactional
    public void saveImage(MultipartFile image, String id) throws NotFoundException, IOException {
        Optional<Organisation> organisationOptional = organisationRepository.findById(Long.parseLong(id));
        if (organisationOptional.isPresent()) {
            Organisation organisation = organisationOptional.get();
            if(image != null){
                organisation.setLogo(image.getBytes());
                organisationRepository.save(organisation);
            } else {
                organisation.setLogo(null);
            }
        } else {
            throw new NotFoundException("No organisation found for id " + id);
        }
    }

    @Transactional
    public File loadImage(String id) throws NotFoundException, IOException {
        Optional<Organisation> organisationOptional = organisationRepository.findById(Long.parseLong(id));
        if (organisationOptional.isPresent()) {
            Organisation organisation = organisationOptional.get();
            if(organisation.getLogo() != null){
                if (organisation.getLogo().length != 0) {
                    return logoWriter.writeLogo(organisation.getLogo());
                } else {
                    throw new NotFoundException("No Logo found for Organisation with id " + organisation.getId());
                }
            } else {
                throw new NotFoundException("No Logo found for Organisation with id " + organisation.getId());
            }
        } else {
            throw new NotFoundException("No organisation found for id " + id);
        }
    }
}
