package de.leipzig.htwk.deltasix.model.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {ValidLocationValidator.class})
@Documented
public @interface ValidLocation {
    String message() default "Standort ungültig!";
    Class<?>[]groups () default {};
    Class<? extends Payload>[] payload() default {};
}
