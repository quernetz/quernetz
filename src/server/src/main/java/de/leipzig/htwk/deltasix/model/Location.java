package de.leipzig.htwk.deltasix.model;

import de.leipzig.htwk.deltasix.model.validations.ValidLocation;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.validation.Valid;

@EqualsAndHashCode
@NoArgsConstructor
@Data
@Embeddable
@ValidLocation
public class Location {
    /*
     * Not sure about storing locations as coordinates or adresses
     *
     */
    private double longitude;

    /*
     * Validation of double-Values isn't supported
     * either switch them to BigDecimal and use @DecimalMin, @DecimalMax
     * or take the risk
     */
    private double latitude;

    private String street;

    private String streetNumber;

    private String zipCode;

    public Location(double latitude, double longitude) {
        this.longitude = longitude;
        this.latitude = latitude;

        street          = "";
        streetNumber    = "";
        zipCode         = "";
    }

    public Location(String street, String streetNumber, String zipCode) {
        this.street         = street;
        this.streetNumber   = streetNumber;
        this.zipCode        = zipCode;
    }

    //creates a copy of provided Location
    public Location(@Valid Location l) {
        this.streetNumber   = l.streetNumber;
        this.street         = l.street;
        this.zipCode        = l.zipCode;
        this.longitude      = l.longitude;
        this.latitude       = l.latitude;
    }
}
