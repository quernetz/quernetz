package de.leipzig.htwk.deltasix.model;

import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Entity
@Builder
@EqualsAndHashCode(exclude = {"projects","organisations"})
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Validated
public class Category {

    @Getter
    @Setter(AccessLevel.PRIVATE)
    @GeneratedValue
    @Id
    private Long id;

    @Getter
    @Setter(AccessLevel.PRIVATE)
    @NotBlank
    private String categoryName;

    @Builder.Default
    @Fetch(FetchMode.JOIN)
    @ManyToMany
    @JoinTable(name = "CATEGORY_PROJECT", joinColumns = @JoinColumn(name = "CATEGORY_ID"), inverseJoinColumns = @JoinColumn(name = "PROJECT_ID"))
    private Set<@Valid Project> projects = new HashSet<>();

    @Builder.Default
    @Fetch(FetchMode.JOIN)
    @ManyToMany
    @JoinTable(name = "CATEGORY_ORGANISATION", joinColumns = @JoinColumn(name = "CATEGORY_ID"), inverseJoinColumns = @JoinColumn(name = "ORGANISATION_ID"))
    private Set<@Valid Organisation> organisations = new HashSet<>();

    public Set<@Valid Project> getProjects() { return Collections.unmodifiableSet(this.projects); }

    public Set<@Valid Organisation> getOrganisations() { return Collections.unmodifiableSet(this.organisations); }

    public Category addOrganisation(@Validated Organisation o) {
        this.organisations.add(o);
        return this;
    }

    public Category addProject(@Validated Project p) {
        this.projects.add(p);
        return this;
    }

    public Category removeProject(Project p){
        this.projects.remove(p);
        return this;
    }

    public Category removeOrganisation(Organisation o){
        this.organisations.remove(o);
        return this;
    }

    @PrePersist
    @PreUpdate
    private void updateProjectAssociation() {
        this.projects.forEach(p -> p.addCategory(this));
        this.organisations.forEach(o -> o.addCategory(this));
    }


}

