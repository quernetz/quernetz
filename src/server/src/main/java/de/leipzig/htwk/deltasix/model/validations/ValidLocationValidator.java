package de.leipzig.htwk.deltasix.model.validations;

import de.leipzig.htwk.deltasix.model.Location;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author Anton Rhein
 * validates provided location
 * a location is valid when:
 *  - either longitutde and latitude are zero (0.0) and street, streetNumber and zipCode aren't blank
 *  - or longitude is in [-180.0,180.0] and latitude is in [-90.0,90.0]
 * @see de.leipzig.htwk.deltasix.model.Location;
 * @see de.leipzig.htwk.deltasix.model.Project
 */
public class ValidLocationValidator implements ConstraintValidator<ValidLocation, Location> {

    @Override
    public void initialize(ValidLocation constraintAnnotation) {
    }

    @Override
    public boolean isValid(Location location, ConstraintValidatorContext constraintValidatorContext) {
        if(location == null){
            return true;
        }
        double lat = location.getLatitude();
        double lon = location.getLongitude();

        if((lat < -90.0 || lat > 90.0 || lon < -180.0 || lon > 180.0 ) && (lat != 0.0 && lon !=0.0)){
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate("angegebene Koordinaten sind ungültig!").addConstraintViolation();
            return false;
        }

        if(lat == 0 && lon == 0){
            if(location.getStreet().isBlank() || location.getStreetNumber().isBlank() || location.getZipCode().isBlank()){
                constraintValidatorContext.disableDefaultConstraintViolation();
                constraintValidatorContext.buildConstraintViolationWithTemplate("angegebene Adresse ist ungültig!").addConstraintViolation();
                return false;
            }
        }
        return true;
    }
}
