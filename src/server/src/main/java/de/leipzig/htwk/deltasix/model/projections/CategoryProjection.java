package de.leipzig.htwk.deltasix.model.projections;
/**
 * @author Anton Rhein
 *
 * Projection only used when Projects and Organisations are queried by their projections
 * Allows to directly include category name as well as links to self, projects and organisations
 *
 * @see de.leipzig.htwk.deltasix.model.Category;
 * @see de.leipzig.htwk.deltasix.dataaccess.CategoryRepository
 */

public interface CategoryProjection {
    String getCategoryName();
}
