package de.leipzig.htwk.deltasix.model.projections;

import de.leipzig.htwk.deltasix.model.Location;
import de.leipzig.htwk.deltasix.model.Project;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Anton Rhein
 *
 * Projection used that already includes category names, tag texts and organisation name to respective projects
 * usage: [url]/projects?projection=withTagsCategoriesOrganisationsAndInterviews
 *
 * @see de.leipzig.htwk.deltasix.model.Project;
 * @see de.leipzig.htwk.deltasix.dataaccess.ProjectRepository
 */

@Projection(name = "withTagsCategoriesOrganisationsAndInterviews", types={Project.class})
public interface ProjectProjection {
    List<TagProjection> getTags();
    List<OrganisationNameProjection> getOrganisations();
    List<CategoryProjection> getCategories();
    String getName();
    String getDescription();
    String getImageUrl();
    Location getLocation();
    String getProjectLink();
    LocalDate getFrom();
    LocalDate getTo();
}