package de.leipzig.htwk.deltasix.model;

import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Entity
@Builder
@EqualsAndHashCode(exclude = {"organisations","projects"})
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Validated
public class Tag {

    @GeneratedValue
    @Id
    @Getter
    @Setter(AccessLevel.PRIVATE)
    private Long id;

    @Getter
    @Setter(AccessLevel.PRIVATE)
    @NotBlank
    private String text;

    @Builder.Default
    @Fetch(FetchMode.JOIN)
    @ManyToMany
    @JoinTable(name = "TAG_ORGANISATION", joinColumns = @JoinColumn(name = "TAG_ID"), inverseJoinColumns = @JoinColumn(name = "ORGANISATION_ID"))
    private Set<Organisation> organisations =  new HashSet<>();

    @Builder.Default
    @Setter
    @Fetch(FetchMode.JOIN)
    @ManyToMany
    @JoinTable(name = "TAG_PROJECT", joinColumns = @JoinColumn(name = "TAG_ID"), inverseJoinColumns = @JoinColumn(name = "PROJECT_ID"))
    private Set<Project> projects = new HashSet<>();

    public Set<Organisation> getOrganisations(){ return Collections.unmodifiableSet(this.organisations); }

    public Set<Project> getProjects(){ return Collections.unmodifiableSet(this.projects); }

    public Tag addOrganisation(@Validated Organisation o){
        this.organisations.add(o);
        return this;
    }

    public Tag addProject(@Validated Project p){
        this.projects.add(p);
        return this;
    }

    public Tag removeProject(Project p){
        this.projects.remove(p);
        return this;
    }

    public Tag removeOrganisation(Organisation o){
        this.organisations.remove(o);
        return this;
    }

    @PrePersist
    @PreUpdate
    void updateAssociations(){
        this.projects.forEach(p -> p.addTag(this));
        this.organisations.forEach(o -> o.addTag(this));
    }
}
