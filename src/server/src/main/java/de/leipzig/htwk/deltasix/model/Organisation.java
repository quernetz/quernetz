package de.leipzig.htwk.deltasix.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.URL;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Entity
@Builder
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Validated
public class Organisation {

    @Setter(AccessLevel.PRIVATE)
    @Getter
    @GeneratedValue
    @Id
    private Long id;

    @Getter
    @Setter
    @NotBlank
    private String name;

    @JsonIgnore
    @Lob
    @Getter
    @Setter
    private byte[] logo;

    @Getter
    @Setter
    @URL
    private String website;

    @Getter
    @Setter
    @Lob
    @NotBlank
    private String description;

    @Setter(AccessLevel.PRIVATE)
    @Builder.Default
    @ManyToMany(mappedBy = "organisations")
    private Set<@Valid Tag> tags = new HashSet<>();

    @Setter(AccessLevel.PRIVATE)
    @Builder.Default
    @ManyToMany(mappedBy = "organisations")
    private Set<@Valid Category> categories = new HashSet<>();

    @Setter(AccessLevel.PRIVATE)
    @Builder.Default
    @Fetch(FetchMode.JOIN)
    @ManyToMany(mappedBy = "organisations")
    private Set<@Valid Project> projects = new HashSet<>();

    @Setter(AccessLevel.PRIVATE)
    @Builder.Default
    @Fetch(FetchMode.JOIN)
    @ManyToMany(mappedBy = "organisations")
    private Set<@Valid Interview> interviews = new HashSet<>();

    public Set<Tag> getTags() {
        return Collections.unmodifiableSet(this.tags);
    }

    public Set<Category> getCategories() {
        return Collections.unmodifiableSet(this.categories);
    }

    public Set<Interview> getInterviews() {
        return Collections.unmodifiableSet(this.interviews);
    }

    public Set<Project> getProjects() {
        return Collections.unmodifiableSet(this.projects);
    }

    public Organisation addTag(@NotNull @Valid Tag tag) {
        this.tags.add(tag);
        return this;
    }

    public Organisation addTags(@NotEmpty Collection<@Valid Tag> tags) {
        this.tags.addAll(tags);
        return this;
    }

    public Organisation removeTag(@NotNull @Valid Tag tag) {
        this.tags.remove(tag);
        return this;
    }

    public Organisation removeTags(@NotEmpty Collection<@Valid Tag> tags) {
        this.tags.removeAll(tags);
        return this;
    }

    public Organisation addCategory(@NotNull @Valid Category category) {
        this.categories.add(category);
        return this;
    }

    public Organisation addCategories(@NotEmpty Collection<@Valid Category> categories) {
        this.categories.addAll(categories);
        return this;
    }

    public Organisation removeCategory(@NotNull @Valid Category category) {
        this.categories.remove(category);
        return this;
    }

    public Organisation removeCategories(@NotEmpty Collection<@Valid Category> categories) {
        this.categories.removeAll(categories);
        return this;
    }

    public Organisation addProject(@NotNull @Valid Project project) {
        this.projects.add(project);
        return this;
    }

    public Organisation addProjects(@NotEmpty Collection<@Valid Project> projects) {
        this.projects.addAll(projects);
        return this;
    }

    public Organisation removeProject(@NotNull @Valid Project project) {
        this.projects.remove(project);
        return this;
    }

    public Organisation removeProjects(@NotEmpty Collection<@Valid Project> projects) {
        this.projects.removeAll(projects);
        return this;
    }

    public Organisation addInterview(@NotNull @Valid Interview interview) {
        this.interviews.add(interview);
        return this;
    }

    public Organisation addInterviews(@NotEmpty Collection<@Valid Interview> interviews) {
        this.interviews.addAll(interviews);
        return this;
    }

    public Organisation removeInterview(@NotNull @Valid Interview interview) {
        this.interviews.remove(interview);
        return this;
    }

    public Organisation removeInterviews(@NotEmpty Collection<@Valid Interview> interviews) {
        this.interviews.removeAll(interviews);
        return this;
    }

    @PrePersist
    private void addAssociations() {
        this.tags.forEach(t -> t.addOrganisation(this));
        this.categories.forEach(c -> c.addOrganisation(this));
        this.projects.forEach(p -> p.addOrganisation(this));
    }

    @PreRemove
    private void clearAssociations() {
        this.tags.forEach(t -> t.removeOrganisation(this));
        this.categories.forEach(c -> c.removeOrganisation(this));
        this.projects.forEach(p -> {
            //check, if project has other organisations associated, if so: this organisation can be removed
            if (p.getOrganisations().size() > 1) {
                p.removeOrganisation(this);
            }
        });
    }
}