package de.leipzig.htwk.deltasix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeltasixApplication {
    
	public static void main(String[] args) {
		SpringApplication.run(DeltasixApplication.class, args);
	}
	
}
