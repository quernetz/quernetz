package de.leipzig.htwk.deltasix.authentification;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Value("${deltasix.auth.user:defaultuser}")
    private String username;

    @Value("${deltasix.auth.password:defaultpw}")
    private String password;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username.equals(this.username)) {
            return new User(this.username, new BCryptPasswordEncoder().encode(this.password), new ArrayList<>());
        }
        else{
            return null;
        }
    }
}