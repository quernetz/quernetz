package de.leipzig.htwk.deltasix.service;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
public class LogoWriter {
    public File writeLogo(byte[] logoAsBytes) throws IOException {
        File file = new File("pathname");
        FileUtils.writeByteArrayToFile(file, logoAsBytes);
        return file;
    }
}
