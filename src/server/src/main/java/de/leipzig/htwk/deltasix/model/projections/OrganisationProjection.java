package de.leipzig.htwk.deltasix.model.projections;
import de.leipzig.htwk.deltasix.model.Organisation;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

/**
 * @author Anton Rhein
 *
 * Projection used that already includes category names, tag texts, project names and interview names to respective organisations
 * usage: [url]/orgabisations?projection=withTagsCategoriesProjectsAndInterviews
 *
 * @see de.leipzig.htwk.deltasix.model.Organisation;
 * @see de.leipzig.htwk.deltasix.dataaccess.OrganisationRepository
 */
@Projection(name = "withTagsCategoriesProjectsAndInterviews", types={Organisation.class})
public interface OrganisationProjection {
    List<TagProjection> getTags();
    List<ProjectNameProjection> getProjects();
    List<InterviewNameProjection> getInterviews();
    List<CategoryProjection> getCategories();
    String getName();
    String getLogo();
    String getWebsite();
    String getDescription();
}