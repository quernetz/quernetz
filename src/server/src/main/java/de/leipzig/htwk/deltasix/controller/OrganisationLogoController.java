package de.leipzig.htwk.deltasix.controller;

import de.leipzig.htwk.deltasix.service.OrganisationLogoStorageService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@RequiredArgsConstructor
@RestController
public class OrganisationLogoController {

    private final OrganisationLogoStorageService organisationLogoStorageService;

    @GetMapping(value = "/api/organisations/{id}/logo",
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody byte[] downloadFile(@PathVariable String id) throws NotFoundException, IOException {
        File image = organisationLogoStorageService.loadImage(id);
        InputStream inputStream = new FileInputStream(image);
        return IOUtils.toByteArray(inputStream);
    }

    @PutMapping(value = "/api/organisations/{id}/logo",
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public @ResponseBody byte[] uploadFile(@RequestParam("file") MultipartFile file,
                                           @PathVariable("id") String id) throws NotFoundException, IOException {
        organisationLogoStorageService.saveImage(file, id);
        File image = organisationLogoStorageService.loadImage(id);
        InputStream inputStream = new FileInputStream(image);
        return IOUtils.toByteArray(inputStream);
    }

    @DeleteMapping(value = "/api/organisations/{id}/logo")
    public ResponseEntity deleteFile(@PathVariable("id") String id) throws NotFoundException, IOException {
        organisationLogoStorageService.saveImage(null, id);
        return ResponseEntity.noContent().build();
    }
}