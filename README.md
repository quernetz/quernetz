# Das Quernetz [![pipeline status](https://gitlab.com/quernetz/quernetz/badges/master/pipeline.svg)](https://gitlab.com/quernetz/quernetz/commits/master)

## Was ist das Quernetz?
Bei dem Quernetz handelt es sich um eine von uns für die Quernetzer entwickelte Webapplikation. Diese ermöglicht den Quernetzern auf ihrer [Website](https://die-quernetzer.de) die Daten zu Verbindungen zwischen verschiedenen gemeinnützigen Organisationen und mit diesen in Beziehung stehenden Projekten und Interviews zu pflegen und grafisch darzustellen.

[Hier](https://das-quernetz.de/) könnt ihr es gerne ausprobieren!

## Für wen wird das Quernetz entwickelt?
Das Quernetz wird für einen gemeinnützigen Verein aus Leipzig entwickelt, die Quernetzer. Sie bieten Menschen eine Plattform, die Ideen für eine nachhaltige und sozialere Gesellschaft haben, jedoch nicht wissen, an wen sie sich wenden sollen und helfen ihnen. Dafür "quernetzen" sie Projekte und Organisationen, die in gleichen Bereichen arbeiten, um diese zusammenzubringen. Ebenso ist ihr Ziel, zu informieren und aufzuklären, wie man schon heute die Welt besser machen kann.

## Aufbau der Applikation
Das Softwaresystem Quernetz besteht aus 3 verschiedenen Komponenten. 

Dazu gehören:
- das Public-UI, welches das Quernetz grafisch darstellt
- das Admin-UI, über welche nach erfolgreichem Login die Inhalte des Quernetzes gepflegt werden können
- und dem Backend-Server, welcher den Komponenten Admin-UI und Public-UI via REST-API Zugriff auf die gespeicherten Inhalte bietet

### Public-UI
Hier könnt ihr den öffentlich zugänglichen Teil unserer Applikation sehen, mit welcher das Quernetz dargestellt wird. 
Über einen Filter lassen sich durch die Angabe von Kategorien jene Knoten auf dem Netz darstellen, welche den gewählten Themenbereichen zugehörig sind.

![Das Public-UI](docs/images/README_pictures/public.PNG)

Fährt man mit dem Cursor über einen der Knoten, werden nur die Nachbarknoten auf dem Netz hervorgehoben. Unbeteiligte Knoten verblassen. 

![Ein Beispiel für die Hover Funktion](docs/images/README_pictures/public_hover.png)

Klickt man auf einen der Knoten, öffnet sich ein Pop-Up mit weiteren Informationen. Hier beispielhaft zu sehen für ein Projekt:

![Das Projekt-Pop-Up](docs/images/README_pictures/projekt.PNG)

Und eine Organisation:

![Das Organisations-Pop-Up](docs/images/README_pictures/orga.PNG)

Und ein Interview:

![Das Interview-Pop-Up](docs/images/README_pictures/interview.PNG)

### Admin-UI
Die Inhaltspflege des Quernetzes wird durch 5 miteinander in Beziehung stehenden Tabellen realisiert. Hier seht ihr beispielsweise die Tabelle für die auf dem Quernetz dargestellten Organisationen.

![Das Admin-UI mit der Organisations Tabelle](docs/images/README_pictures/admin_orga.PNG)

Soll einer Tabelle ein Datensatz hinzugefügt werden, dann geschieht dies über den Button oben rechts. Das zur Tabelle zugehörige Formular öffnet sich.

![Das Organisation-erstellen-Pop-Up](docs/images/README_pictures/orga_erstellen.PNG)

## Ein kurzer Abriss der Technik
Das Quernetz besteht aus 2 JavaScript-Frontends die mit [React](https://reactjs.org) entwickelt wurden, sowie einem auf Java und [Spring-Boot](https://spring.io/projects/spring-boot) basierenden Backend. Front- und Backend kommunizieren über eine [REST-API](https://de.wikipedia.org/wiki/Representational_State_Transfer).

## Wie ist das Quernetz entstanden?
Der Großteil der Entwicklungsarbeit ist an der [HTWK Leipzig](https://www.htwk-leipzig.de/startseite/) in 2 Semestern, während eines Softwareprojektes, von den Bachelorstudenten
- Balthasar Becker
- Maximilian Alexander Deichsel
- Anton Diettrich
- Philine Egri
- Maximilian Honig
- Maximilien Karehnke
- Romy Kircheis
- Hartmut Knaack
- Anton Rhein
- Bastian Springmann
- Ralf Stürzner

unter der Anleitung der Masterstudenten und Projektleiter
- Johannes Albrecht (Projektmanager & Scrum Master)
- Fabian Mittmann (Softwarearchitekt)
- Leonard Purschke (Product Owner)

geleistet worden.

Derzeit arbeiten
- Bastian Springmann
- Maximilian Alexander Deichsel
- Fabian Mittmann

daran, das Quernetz zu finalisieren.

## Links
- [Die Quernetzer](https://die-quernetzer.de)
- [Quernetz](https://das-quernetz.de/)
- [Inhaltspflege Quernetz](https://das-quernetz.de/admin/welcome)
- REST-API:
  -  [Organisationen](https://das-quernetz.de:8080/api/organisations)
  -  [Projekte](https://das-quernetz.de:8080/api/projects)
  -  [Interviews](https://das-quernetz.de:8080/api/interviews)
  -  [Kategorien](https://das-quernetz.de:8080/api/categories)
  -  [Tags](https://das-quernetz.de:8080/api/tags)
  